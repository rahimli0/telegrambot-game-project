import os

from celery import Celery, shared_task
from celery.schedules import crontab
import datetime

import requests
from dateutil.relativedelta import relativedelta
from decimal import Decimal
from django.db.models import Q

from django.utils import timezone

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tbgm.settings')

app = Celery('tbgm')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))

app.conf.timezone = 'Europe/Kiev'
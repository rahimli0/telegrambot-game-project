from __future__ import print_function
import os
from uuid import uuid4

from PIL import Image, ImageOps, ImageFont, ImageDraw



def make_game_avatar_total_images(file1,file2):
    result = Image.new("RGB", (626, 469), "white")

    path1 = os.path.expanduser(file1)
    # path2 = os.path.join(file2)
    # os.path.expanduser(file[1])
    img1 = Image.open(path1)
    # img2 = Image.open(path2)
    # img1.thumbnail((50, 50), Image.ANTIALIAS)
    # img2.thumbnail((350, 50), Image.ANTIALIAS)

    result.paste(img1, (50, 50, 50 + 160, 50 + 200))
    # result.paste(img2, (50, 50, 50 + 160, 50 + 200))
    # new_file = 'photos/games/all/{}'.format('{}.{}'.format(uuid4().hex, 'png'))
    new_file = './{}'.format('{}.{}'.format(uuid4().hex, 'png'))
    result.save(os.path.join(str(new_file)), quality=95)
    return os.path.join('/media/' ,str(new_file))

def make_av(file1,file2,file_b):
    import os
    from PIL import Image
    image1 = Image.open(file1, 'r').resize((280, 300)).convert("RGBA")
    image2 = Image.open(file2, 'r').resize((280, 300)).convert("RGBA")
    ironman = Image.open(file_b, 'r').convert("RGBA")
    text_img = Image.new('RGBA', (626, 509), (0, 0, 0, ))
    text_img.paste(image1, (28, 60), mask=image1)
    text_img.paste(image2, (315, 60), mask=image2)
    text_img.paste(ironman, (0, 0), mask=ironman)
    text_img.save("ball.png", format="png")


make_av('./1.png','./avatar_uol8tc6.png','./versus-back.png')

# def make_avatar_total_images(files):
#   column = 3
#   x_c = 150
#   y_c = 200
#   font_size = 20
#   file_count = len(files)
#   print(file_count % 3)
#   if len(files) < 5:
#     column = 2
#     row = 2
#     x_c = 225
#     y_c = 300
#     if len(files) <= 2:
#       row = 1
#     if len(files) <= 1:
#       column = 1
#   else:
#     extra_count = 1 if file_count % 3 else 0
#     row = file_count // column + extra_count
#
#   print(row)
#   result = Image.new("RGB", (x_c*column, y_c*row))
#
#   for index, file in enumerate(files):
#     index_i = index + 1
#     path = os.path.expanduser(file[1])
#     img = Image.open(path)
#     img_with_border = ImageOps.expand(img, border=1, fill='black')
#     img_with_border.thumbnail((x_c, y_c), Image.ANTIALIAS)
#     # fnt = ImageFont.truetype('/Library/Fonts/Arial.ttf', 15)
#     d = ImageDraw.Draw(img_with_border)
#     d.text((10, 10), file[0], fill=(0, 0, 0), )
#     x = index % column * x_c
#     y = index // column * y_c
#     print(index,index // column,y,)
#     # if index_i % column:
#     #   y -= y_c
#     w, h = img_with_border.size
#     # print(index,row)
#     print('pos {0},{1} size {2},{3}'.format(x, y, w, h))
#     result.paste(img_with_border, (x, y, x + w, y + h))
#
#   result.save(os.path.expanduser('./image.png'))


files = [
  ('Title 1','./11.jpg'),
  ('Title 2','./2.png'),
  ('Title 3','./3.jpg'),
  ('Title 4','./11.jpg'),
  ('Title 5','./3.jpg'),
  ('Title 6','./11.jpg'),
  # './11.jpg',
  # './2.png',
  # './3.jpg',
  # '~/Downloads/2.jpg',
  # '~/Downloads/3.jpg',
  # '~/Downloads/4.jpg'
]
# make_avatar_total_images(files)
# from PIL import Image, ImageDraw, ImageFilter
# def avatar_make(user_id):
#     im1 = Image.open('avatar.png')
#     im2 = Image.open('item.png')
#     back_im = im1.copy()
#     back_im.paste(im2, (395, 270), im2)
#     back_im.save('1.png', quality=95)
#
# avatar_make(1)

# #!/usr/bin/env python
# # -*- coding: utf-8 -*-
# # This program is dedicated to the public domain under the CC0 license.
#
# """
# First, a few callback functions are defined. Then, those functions are passed to
# the Dispatcher and registered at their respective places.
# Then, the bot is started and runs until we press Ctrl-C on the command line.
#
# Usage:
# Example of a bot-user conversation using ConversationHandler.
# Send /start to initiate the conversation.
# Press Ctrl-C on the command line or send a signal to the process to stop the
# bot.
# """
#
# import logging
#
# from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove)
# from telegram import (Updater, CommandHandler, MessageHandler, Filters,
#                           ConversationHandler)
#
# # Enable logging
# logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
#                     level=logging.INFO)
#
# logger = logging.getLogger(__name__)
#
# GENDER, PHOTO, LOCATION, BIO = range(4)
#
#
# def start(update, context):
#     reply_keyboard = [['Boy', 'Girl', 'Other']]
#
#     update.message.reply_text(
#         'Hi! My name is Professor Bot. I will hold a conversation with you. '
#         'Send /cancel to stop talking to me.\n\n'
#         'Are you a boy or a girl?',
#         reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
#
#     return GENDER
#
#
# def gender(update, context):
#     user = update.message.from_user
#     logger.info("Gender of %s: %s", user.first_name, update.message.text)
#     update.message.reply_text('I see! Please send me a photo of yourself, '
#                               'so I know what you look like, or send /skip if you don\'t want to.',
#                               reply_markup=ReplyKeyboardRemove())
#
#     return PHOTO
#
#
# def photo(update, context):
#     user = update.message.from_user
#     photo_file = update.message.photo[-1].get_file()
#     photo_file.download('user_photo.jpg')
#     logger.info("Photo of %s: %s", user.first_name, 'user_photo.jpg')
#     update.message.reply_text('Gorgeous! Now, send me your location please, '
#                               'or send /skip if you don\'t want to.')
#
#     return LOCATION
#
#
# def skip_photo(update, context):
#     user = update.message.from_user
#     logger.info("User %s did not send a photo.", user.first_name)
#     update.message.reply_text('I bet you look great! Now, send me your location please, '
#                               'or send /skip.')
#
#     return LOCATION
#
#
# def location(update, context):
#     user = update.message.from_user
#     user_location = update.message.location
#     logger.info("Location of %s: %f / %f", user.first_name, user_location.latitude,
#                 user_location.longitude)
#     update.message.reply_text('Maybe I can visit you sometime! '
#                               'At last, tell me something about yourself.')
#
#     return BIO
#
#
# def skip_location(update, context):
#     user = update.message.from_user
#     logger.info("User %s did not send a location.", user.first_name)
#     update.message.reply_text('You seem a bit paranoid! '
#                               'At last, tell me something about yourself.')
#
#     return BIO
#
#
# def bio(update, context):
#     user = update.message.from_user
#     logger.info("Bio of %s: %s", user.first_name, update.message.text)
#     update.message.reply_text('Thank you! I hope we can talk again some day.')
#
#     return ConversationHandler.END
#
#
# def cancel(update, context):
#     user = update.message.from_user
#     logger.info("User %s canceled the conversation.", user.first_name)
#     update.message.reply_text('Bye! I hope we can talk again some day.',
#                               reply_markup=ReplyKeyboardRemove())
#
#     return ConversationHandler.END
#
#
# def error(update, context):
#     """Log Errors caused by Updates."""
#     logger.warning('Update "%s" caused error "%s"', update, context.error)
#
#
# def main():
#     # Create the Updater and pass it your bot's token.
#     # Make sure to set use_context=True to use the new context based callbacks
#     # Post version 12 this will no longer be necessary
#     updater = Updater("TOKEN", use_context=True)
#
#     # Get the dispatcher to register handlers
#     dp = updater.dispatcher
#
#     # Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO
#     conv_handler = ConversationHandler(
#         entry_points=[CommandHandler('start', start)],
#
#         states={
#             GENDER: [MessageHandler(Filters.regex('^(Boy|Girl|Other)$'), gender)],
#
#             PHOTO: [MessageHandler(Filters.photo, photo),
#                     CommandHandler('skip', skip_photo)],
#
#             LOCATION: [MessageHandler(Filters.location, location),
#                        CommandHandler('skip', skip_location)],
#
#             BIO: [MessageHandler(Filters.text, bio)]
#         },
#
#         fallbacks=[CommandHandler('cancel', cancel)]
#     )
#
#     dp.add_handler(conv_handler)
#
#     # log all errors
#     dp.add_error_handler(error)
#
#     # Start the Bot
#     updater.start_polling()
#
#     # Run the bot until you press Ctrl-C or the process receives SIGINT,
#     # SIGTERM or SIGABRT. This should be used most of the time, since
#     # start_polling() is non-blocking and will stop the bot gracefully.
#     updater.idle()
#
#
# if __name__ == '__main__':
#     main()
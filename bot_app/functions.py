import os
from decimal import Decimal
from uuid import uuid4

from PIL import Image, ImageOps, ImageDraw
from django.conf import settings
from django.contrib.auth import get_user_model, logout
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse

# GUser = get_user_model()
bot_name = settings.TELEGRAM_BOT_NAME
bot_token = settings.TELEGRAM_BOT_TOKEN
redirect_url = settings.TELEGRAM_LOGIN_REDIRECT_URL

from django_telegram_login.authentication import verify_telegram_authentication
from django_telegram_login.errors import (
    NotTelegramDataError,
    TelegramDataIsOutdatedError,
)


# def check_user(m_id):
#
#     user = GUser.objects.filter(m_id=m_id).

def to_number(str_text):
    try:
        val = float(str_text)
        return [True,val]
    except ValueError:
        return [False,None]


def make_game_image(file1,file2,file_b):
    import os
    from PIL import Image
    background = Image.open(os.path.join(settings.MEDIA_ROOT ,file_b), 'r').convert("RGBA")
    text_img = Image.new('RGBA', (626, 509), (0, 0, 0, ))
    if file1:
        image1 = Image.open(os.path.join(settings.MEDIA_ROOT ,file1), 'r').resize((280, 300)).convert("RGBA")
        text_img.paste(image1, (28, 60), mask=image1)
    if file2:
        image2 = Image.open(os.path.join(settings.MEDIA_ROOT ,file2), 'r').resize((280, 300)).convert("RGBA")
        text_img.paste(image2, (315, 60), mask=image2)
    text_img.paste(background, (0, 0), mask=background)
    new_file = 'photos/games/all/{}'.format('{}.{}'.format(uuid4().hex, 'png'))
    text_img.save(os.path.join(settings.MEDIA_ROOT ,str(new_file)), format="png", quality=35)
    return new_file
    # return os.path.join('/media/' ,str(new_file))

def make_avatar_total_images(files):
    column = 3
    x_c = 150
    y_c = 200
    font_size = 20
    file_count = len(files)
    print(file_count % 3)
    if len(files) < 5:
        column = 2
        row = 2
        x_c = 225
        y_c = 300
        if len(files) <= 2:
            row = 1
        if len(files) <= 1:
            column = 1
    else:
        extra_count = 1 if file_count % 3 else 0
        row = file_count // column + extra_count

    print(row)
    result = Image.new("RGB", (x_c*column, y_c*row))

    for index, file in enumerate(files):
        path = os.path.join(settings.MEDIA_ROOT ,file[1])
        # os.path.expanduser(file[1])
        img = Image.open(path)
        img_with_border = ImageOps.expand(img, border=1, fill='black')
        img_with_border.thumbnail((x_c, y_c), Image.ANTIALIAS)
        # fnt = ImageFont.truetype('/Library/Fonts/Arial.ttf', 15)
        d = ImageDraw.Draw(img_with_border)
        d.text((10, 10), file[0], fill=(0, 0, 0), )
        x = index % column * x_c
        y = index // column * y_c
        print(index,index // column,y,)
        # if index_i % column:
        #   y -= y_c
        w, h = img_with_border.size
        # print(index,row)
        print('pos {0},{1} size {2},{3}'.format(x, y, w, h))
        result.paste(img_with_border, (x, y, x + w, y + h))

    new_file = 'photos/user-avatar/choose-all/{}'.format('{}.{}'.format(uuid4().hex, 'png'))
    result.save(os.path.join(settings.MEDIA_ROOT ,str(new_file)), quality=35)
    return os.path.join('/media/' ,str(new_file))

def make_avatar_total_images_name(files):
    column = 3
    x_c = 150
    y_c = 200
    font_size = 20
    file_count = len(files)
    print(file_count % 3)
    if len(files) < 5:
        column = 2
        row = 2
        x_c = 225
        y_c = 300
        if len(files) <= 2:
            row = 1
        if len(files) <= 1:
            column = 1
    else:
        extra_count = 1 if file_count % 3 else 0
        row = file_count // column + extra_count

    print(row)
    result = Image.new("RGB", (x_c*column, y_c*row))

    for index, file in enumerate(files):
        path = os.path.join(settings.MEDIA_ROOT ,file[1])
        # os.path.expanduser(file[1])
        img = Image.open(path)
        img_with_border = ImageOps.expand(img, border=1, fill='black')
        img_with_border.thumbnail((x_c, y_c), Image.ANTIALIAS)
        # fnt = ImageFont.truetype('/Library/Fonts/Arial.ttf', 15)
        d = ImageDraw.Draw(img_with_border)
        d.text((10, 10), file[0], fill=(0, 0, 0), )
        x = index % column * x_c
        y = index // column * y_c
        print(index,index // column,y,)
        # if index_i % column:
        #   y -= y_c
        w, h = img_with_border.size
        # print(index,row)
        print('pos {0},{1} size {2},{3}'.format(x, y, w, h))
        result.paste(img_with_border, (x, y, x + w, y + h))

    new_file = 'photos/user-avatar/choose-all/{}'.format('{}.{}'.format(uuid4().hex, 'png'))
    result.save(os.path.join(settings.MEDIA_ROOT ,str(new_file)), quality=35)
    return str(new_file)



def make_game_avatar_total_images(file1,file2):
    result = Image.new("RGB", (626, 469))

    path1 = os.path.join(settings.MEDIA_ROOT ,file1)
    path2 = os.path.join(settings.MEDIA_ROOT ,file2)
    # os.path.expanduser(file[1])
    img1 = Image.open(path1)
    img2 = Image.open(path2)
    img1.thumbnail((50, 50), Image.ANTIALIAS)
    img2.thumbnail((350, 50), Image.ANTIALIAS)

    result.paste(img1, (50, 50, 50 + 160, 50 + 200))
    result.paste(img2, (50, 50, 50 + 160, 50 + 200))
    new_file = 'photos/games/all/{}'.format('{}.{}'.format(uuid4().hex, 'png'))
    result.save(os.path.join(settings.MEDIA_ROOT ,str(new_file)), quality=35)
    return os.path.join('/media/' ,str(new_file))

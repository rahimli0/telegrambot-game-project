from django.conf.urls import url

from .views import *
from .api_views import *

app_name = 'bot-app'
urlpatterns = [
	url(r'^update/$', UpdateBot.as_view(), name='update'),

	url(r'^get-start/(?P<c_id>[0-9]+)/$', GetStarted.as_view(), name='get-start'),

	url(r'^user-main-message/(?P<c_id>[0-9]+)/$', UserMainMessage.as_view(), name='user-main-message'),

	url(r'^show-fight-menu/(?P<c_id>[0-9]+)/$', ShowFightMenu.as_view(), name='show-fight-menu'),
	url(r'^generate-new-game/(?P<c_id>[0-9]+)/(?P<type>[\w-]+)/$', GenerateGame.as_view(), name='generate-new-game'),
	url(r'^delete-all-games/(?P<c_id>[0-9]+)/$', DeleteAllGames.as_view(), name='delete-all-games'),
	url(r'^game-status/(?P<c_id>[0-9]+)/$', GameStatus.as_view(), name='game-status'),
	url(r'^game-action/(?P<c_id>[0-9]+)/$', GameActionApiView.as_view(), name='game-action'),
	url(r'^game-status-with-id/(?P<c_id>[0-9]+)/(?P<g_id>[0-9]+)/$', GameStatusWithId.as_view(), name='game-status-with-id'),
	# url(r'^game-status-with-id-general/(?P<c_id>[0-9]next-step-status+)/(?P<g_id>[0-9]+)/$', GameStatusWithId.as_view(), name='game-status-with-id-general'),
	url(r'^game-status-with-general/(?P<c_id>[0-9]+)/(?P<g_id>[0-9]+)/$', GameStatusWithGeneral.as_view(), name='game-status-with-general'),
	url(r'^game-status-with-general-only/(?P<g_id>[0-9]+)/$', GameStatusWithGeneralOnly.as_view(), name='game-status-with-general-only'),
	url(r'^game-status-witget-starth-general-for-start/(?P<g_id>[0-9]+)/$', GameStatusWithGeneralForStart.as_view(), name='game-status-with-general-for-start'),
	url(r'^check-private-game/(?P<c_id>[0-9]+)/$', CheckPrivateGame.as_view(), name='check-private-game'),
	url(r'^start-the-game/(?P<c_id>[0-9]+)/(?P<g_u_id>[\w-]+)/$', StartTheGame.as_view(), name='start-the-game'),
	url(r'^finish-the-game/(?P<g_id>[0-9]+)/$', FinishTheGame.as_view(), name='finish-the-game'),
	url(r'^finish-the-started-game/(?P<c_id>[0-9]+)/$', FinishStartedTheGame.as_view(), name='finish-the-started-game'),
	url(r'^game-status-for-finished-canceled/(?P<g_id>[0-9]+)/$', GameStatusForCancelFinished.as_view(), name='game-status-for-finished-canceled'),

	url(r'^next-step-status/(?P<m_id>[0-9]+)/$', NextStepStatus.as_view(), name='next-step-status'),
	url(r'^use-referal-api/(?P<c_id>[0-9]+)/$', UserReferalApiView.as_view(), name='use-referal-api'),

	url(r'^employees/$', GetList.as_view(), name='users'),
	url(r'^get-user-avatars/(?P<c_id>[0-9]+)/$', UserAvatarList.as_view(), name='user-avatars'),
	url(r'^get-free-avatars/$', GetFreeAvatarList.as_view(), name='free-avatars'),
	url(r'^avatar-list-with-image/$', AvatarListWithImage.as_view(), name='avatar-list-with-image'),
	url(r'^user-avatar-list-with-image/(?P<c_id>[0-9]+)/$', UserAvatarListWithImage.as_view(), name='user-avatar-list-with-image'),
	url(r'^free-avatars-title/$', GetFreeAvatarListTitle.as_view(), name='free-avatars-title'),

	url(r'^delete-messages/(?P<c_id>[0-9]+)/(?P<l_m_id>[0-9]+)/$', DeleteMessages.as_view(), name='delete-messages'),
	url(r'^only-delete-messages/(?P<c_id>[0-9]+)/(?P<l_m_id>[0-9]+)/$', OnlyDeleteMessages.as_view(), name='only-delete-messages'),

	url(r'^my-strategy-status/(?P<c_id>[0-9]+)/$', MyStrategyStatus.as_view(), name='my-strategy-status'),
	url(r'^my-strategy-change/(?P<c_id>[0-9]+)/$', ChangeMyStrategy.as_view(), name='my-strategy-change'),
	url(r'^check-create-or-edit-user/(?P<c_id>[0-9]+)/$', CheckCreateOrEditUser.as_view(), name='check-create-or-edit-user'),
	url(r'^my-strategy-change-data/(?P<c_id>[0-9]+)/$', ChangeMyStrategyData.as_view(), name='my-strategy-change-data'),

	url(r'^create-user/$', CreateUser.as_view(), name='create-user'),
	url(r'^set-user-language/(?P<m_id>[0-9]+)/$', SetUserLanguage.as_view(), name='set-user-language'),
	url(r'^get-menu/(?P<m_id>[0-9]+)/(?P<username>[\w-]+)/$', GetMenu.as_view(), name='get-menu'),
	url(r'^set-limit-point/(?P<m_id>[0-9]+)/(?P<username>[\w-]+)/$', SetLimitPoint.as_view(), name='set-limit-point'),

	url(r'^get-employees-text/(?P<m_id>[0-9]+)/(?P<username>[\w-]+)/$', GetEmployeeListText.as_view(), name='get-employees-text'),
	url(r'^get-employees-mark-text/(?P<m_id>[0-9]+)/(?P<username>[\w-]+)/$', GetEmployeeListMarkText.as_view(), name='get-employees-mark-text'),

	# url(r'^finished-task/(?P<id>[0-9]+)/(?P<username>[\w-]+)/$', FinishedTask.as_view(), name='finished-task'),
	# url(r'^task-list/(?P<username>[\w-]+)/$', GetMyTasksList.as_view(), name='task-list'),
	# url(r'^employees/$', GetList.as_view(), name='employees'),
	# url(r'^daily-check/(?P<m_id>[0-9]+)/$', daily_check, name='daily-check'),
	# url(r'^daily-check/(?P<m_id>[0-9]+)/$', DailyCheckVIEW.as_view(), name='daily-check'),
]


import _thread

import emoji
from rest_framework.response import Response
from rest_framework.views import APIView
import json

from bot_app.general.utils import MainMenuList, ParentMenuList, MainBackText, BackRegisterHandler
# from bot_app.general.widgets import MenuButton
from tbgm.settings import TELEGRAM_BOT_TOKEN

from telebot import types
import telebot


bot = telebot.TeleBot(TELEGRAM_BOT_TOKEN)
domain_name = 'https://game.minbashi.com'
api_base_url = 'https://game.minbashi.com/bot-api/'


import requests

def phone_checker(text):
    import re
    pattern1 = re.compile("^(\+)([0-9]{2})(\d{9})$")
    pattern2 = re.compile("^(\+)([0-9]{3})(\d{9})$")
    if pattern1.match(str(text).strip()) or pattern2.match(str(text).strip()):
        return True
    else:
        return False

def check_main_menu(message):
    # bot.send_message(message.chat.id, "---------------- check_main_menu -----------------".format(2))
    return_val = False
    if message.text in MainMenuList or message.text in ParentMenuList:
        return_val = True
    return return_val

def extract_unique_code(text):
    # Extracts the unique_code from the sent /start command.
    return text.split()[1] if len(text.split()) > 1 else None

def delete_message_one_message(chat_id,message_id):
    try:
        bot.delete_message(chat_id=chat_id, message_id=message_id)
    except:
        pass

def show_menu(message, text):
    pass
    # mb = MenuButton(message=message, text=text)
    # mb.main_menu()


def auth_check(message):
    return_val = [False, '',0]
    try:
        _thread.start_new_thread(delete_message_one_message, (message.chat.id, message.message_id))
    except:
        pass
    try:
        r = requests.get('{}/bot-api/get-start/{}/'.format(domain_name,message.chat.id,))
        data = json.loads(r.text)
        if data['code']:
            return_val = [True, data['message'], data['game_status']]
        else:
            r = requests.post('{}/bot-api/next-step-status/{}/'.format(domain_name, message.from_user.id),
                data={
                    'ns_status': 'register_contact_handler'
                })
            keyboard = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=False, one_time_keyboard=False)
            button_phone = types.KeyboardButton(text=emoji.emojize(':adult: Authorize'), request_contact=True, )
            keyboard.add(button_phone)  # Add this button
            bot.send_message(message.chat.id, data['message'], reply_markup=keyboard)  # show the keyboard

    except:
        text = "@{}, please try again later".format(message.from_user.username)
        bot.send_message(message.chat.id, text)

    return return_val



def check_step_handlerstate(message):
    # bot.send_message(message.chat.id, "---------------- {} -----------------".format('check_avatar_handlerstate'))
    try:
        r = requests.get('{}next-step-status/{}/'.format(api_base_url, message.from_user.id, ))
        data = json.loads(r.text)
        step = data['status']
    except:
        step = '-'
    return step


def change_stategy(message):
    markup = types.InlineKeyboardMarkup()
    e_ok = False
    text = "Try again 2"
    try:
        r = requests.get('{}my-strategy-status/{}/'.format(api_base_url, message.chat.id), )
        data = json.loads(r.text)
        result_data = data['result_data']
        markup.row_width = 2
        markup.add(
            types.InlineKeyboardButton("Strength {}".format(result_data['strength']), callback_data="cb_strength_plus"),
            types.InlineKeyboardButton("Quickness {}".format(result_data['quickness']), callback_data="cb_quickness_plus"),
            types.InlineKeyboardButton("Intuition {}".format(result_data['intuition']), callback_data="cb_intuition_plus"),
            types.InlineKeyboardButton("Durability {}".format(result_data['durability']), callback_data="cb_durability_plus"),
        )
        markup.row_width = 1
        markup.add(
            types.InlineKeyboardButton("Reset ( {} )".format(result_data['total_point']), callback_data="cb_reset"),
            types.InlineKeyboardButton(MainBackText, callback_data=BackRegisterHandler[2]),
        )
        markup.row_width = 1
        if result_data['total_point']<=0:
            markup.add(
                types.InlineKeyboardButton("Fight", callback_data="FightCallbackKey"),
            )
        if data['code']:
            text = data['message']
            e_ok = True
    except Exception as e:
        text = str(e)

    if e_ok:
        bot.send_message(message.chat.id, text, reply_markup=markup)
    else:
        bot.send_message(message.chat.id, text)
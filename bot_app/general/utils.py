import emoji



commands = {  # command description used in the "help" command
    'start'       : 'Get used to the bot',
    'help'        : 'Gives you information about the available commands',
    'play': 'Start play the game',
    'invite'    : 'Invite your friend',
    'changestrategy'    : 'Change strategy',
    'my_scores'    : 'My scores list and their logs',
}

GenderChoices = (
    ('Male', 'Male'),
    ('Female', 'Female'),
)

GenderChoicesList = [x[0] for x in GenderChoices]

AtackChoices = (
    ('hit:Head', 'hit: Head'),
    ('hit:Body', 'hit: Body'),
    ('hit:Leg', 'hit: Leg'),
)
DeffenceChoicesNEW = (
    ('block:Head', 'block: Head'),
    ('block:Body', 'block: Body'),
    ('block:Leg',  'block: Leg'),
)

AtackChoicesList = [x[1] for x in AtackChoices]
#    Aranjiman
DeffenceChoices = (
    ('block:Head', 'block: Head'),
    ('block:Body', 'block: Body'),
    ('block:Leg',  'block: Leg'),
)
GameReadyButton = ('LeaveTheGameActionHandler','Leave the game')
GameLeaveButton = ('LeaveTheGameActionHandler','Leave the game')
DeffenceChoicesList = [x[0] for x in DeffenceChoices]

AtackDeffenceList = [x[0] for x in AtackChoices] + [x[0] for x in DeffenceChoices]

AvatarPaidFreeChoices = (
    ('Free', 'Free'),
    ('Paid', 'Paid'),
)
AvatarStatusChoices = (
    ('Active', 'Active'),
    ('Deleted', 'Deleted'),
    ('Waiting', 'Waiting'),
)

UserAvatarStatusChoices = (
    ('Selected', 'Selected'),
    ('Unselected', 'Unselected'),
)
GameTypeChoices = (
    ('Public', 'Public'),
    ('Private', 'Private'),
)

BackRegisterHandler = ["SectionCharacterName","SectionToGender","SectionToAvatar","SectionToSetStrategy"]
AvatarNextPreviewHandler = ["AvatarPreviewHandler","AvatarNextHandler"]

MainBackText = emoji.emojize(':BACK_arrow: Back')
EmptyCallbackHandler = 'EmptyCallbackHandler'
main_back = emoji.emojize(':BACK_arrow: Back')
MAIN_MENU = emoji.emojize(':page_facing_up: Menu')
LanguageList = ['English', 'Russian', 'Ukrainan','Show Menu']
GenderList = ['Male', 'Female']
AvatarList = ['Avatar 1', 'Avatar 2', 'Avatar 3', 'Avatar 4']
GameStatusList = ['Waiting', 'Started', 'Finished']
MainMenuList = [emoji.emojize(':video_game: Play'),emoji.emojize(':chart_increasing_with_yen: My strategy'), emoji.emojize(':vampire: Avatars'), emoji.emojize(':game_die: Items'), emoji.emojize(':soccer_ball: My scores'), emoji.emojize(':wrench: Settings') ,main_back]
AvatarItemTypeList = ['Head','Body','Leg','Foot',]
AvatarMenuList = ['My avatars','Get new']
GameMenuList = ['Public','Private','AllGameRemove',]
DeleteAndBackFightMenuControllerHandler = ('DeleteAndBackFightMenuControllerHandler','Fight again')
GameMenuControllerList = ['FightControllerHandler','PublicControllerHandler','PrivateControllerHandler','BackToFightControllerHandler','CancelTheGameControllerHandler']
GameMenuTypesList = ['FightControllerHandler', 'WaitMenuType']
GameMenuPlacesControllerList = ['Fight','Public','Private',MainBackText,"Cancel the game",]
PrivateGameList = 'Cancel'
ParentMenuList = [main_back,'Public','Private','Language',main_back] + AvatarItemTypeList + AvatarMenuList + GameMenuList + [MAIN_MENU]

PrivateGameAcceptRejectControllerHandler = (
    ("PrivateGameAcceptControllerHandler","Fight now"),
    ("PrivateGameRejectControllerHandler","Reject")
)

GameStatusChoices = (
    ('Waiting', 'Waiting'),
    ('Started', 'Started'),
    ('Finished', 'Finished'),
    ('Started', 'Start'),
    ('CanceledGame', 'Cancel'),
)

GameStartStatusChoices = (
    ('Waiting', 'Waiting'),
    ('Started', 'Started'),
    ('Rejected', 'Rejected'),
)
AvatarItemTypeChoices = ( (x ,x) for x in  AvatarItemTypeList)


AllHandlersList = ['register_contact_handler','register_character_name_handler','register_gender_handler','register_avatar_handler','register_strategy_handler','c_private_game_handler']

CallDataList = ["cb_strength_plus", "cb_quickness_plus", "cb_intuition_plus", "cb_durability_plus",
                  "FightCallbackKey", "cb_reset",]





TOTALUSERGAMEPOINT = 5
TOTALUSERGAMEPENALTYPOINT = 4
TOTALUSERGAMESTARTPOINT = 10
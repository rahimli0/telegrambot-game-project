import json

import copy
import emoji as emoji
import requests
import telebot
import _thread
from telebot import types

from base_user.tools.common import USERGAMESTATUS
from bot_app.general.functions import auth_check, change_stategy, phone_checker, delete_message_one_message
from bot_app.general.widgets import PrivateGameList, RegisterStepModel, GameMenuList, GameModel, \
    ValidateModel
from bot_app.general.utils import GenderList, MAIN_MENU, MainBackText, BackRegisterHandler, GameMenuControllerList
from bot_app.tools.widget import AvatarsButton
from tbgm.settings import TELEGRAM_BOT_TOKEN

bot = telebot.TeleBot(TELEGRAM_BOT_TOKEN)
domain_name = 'https://game.minbashi.com'
api_base_url = 'https://game.minbashi.com/bot-api/'


def menu_handler(message):
    # bot.send_message(message.chat.id, "---------------- check_main_menu true -----------------".format(2))
    # mb = MenuButton(message,'Menu')
    # if message in ParentMenuList:
    auth_check_data = auth_check(message)
    if auth_check_data[0]:
        if auth_check_data[2] != USERGAMESTATUS[3][0]:
            pass
            # mb = MenuButton(message,message.text)
            # mb.parent_menu_show(message.text)
        else:
            try:
                _thread.start_new_thread(delete_message_one_message,(message.chat.id, message.message_id))
            except:
                pass




def register_contact_handler(message):
    try:
        _thread.start_new_thread(delete_message_one_message, (message.chat.id, int(message.message_id-1)))
        _thread.start_new_thread(delete_message_one_message, (message.chat.id, message.message_id))
    except:
        pass
    if message.contact is not None:
        r = requests.post('{}/bot-api/next-step-status/{}/'.format(domain_name,message.from_user.id),
            data={
                'ns_status': 'register_character_name_handler',
                'phone_number': message.contact.phone_number,
            })
        bot.send_message(message.chat.id, "<b>Please create your character.</b>",parse_mode='HTML',reply_markup=types.ReplyKeyboardRemove())
        bot.send_message(message.chat.id, "Please enter your character name:")

    else:
        keyboard = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=False, one_time_keyboard=False)
        button_phone = types.KeyboardButton(text=emoji.emojize(':adult: Authorize'), request_contact=True, )
        keyboard.add(button_phone)  # Add this button
        bot.send_message(message.chat.id, "Try again adding your number:",reply_markup=keyboard)


def register_character_name_handler(message):
    try:
        _thread.start_new_thread(delete_message_one_message, (message.chat.id, int(message.message_id)-2))
        _thread.start_new_thread(delete_message_one_message, (message.chat.id, int(message.message_id)-1))
        _thread.start_new_thread(delete_message_one_message, (message.chat.id, message.message_id))
    except:
        pass
    vm = ValidateModel()

    if vm.name_checker_utf8(text=str(message.text).strip()):
        r = requests.post('{}/bot-api/next-step-status/{}/'.format(domain_name,message.from_user.id),
            data={
                'ns_status': 'register_gender_handler',
                'character_name': str(message.text).strip(),
            })

        markup = types.InlineKeyboardMarkup()
        markup.row_width = 2
        for GenderListItem in GenderList:
            markup.add(
                types.InlineKeyboardButton("{}".format(GenderListItem), callback_data="{}".format(GenderListItem)),
            )
        markup.row_width = 1
        markup.add(
            types.InlineKeyboardButton("{}".format(MainBackText), callback_data="{}".format(BackRegisterHandler[0])),
        )
        bot.send_message(message.chat.id, "Please select your gender:", reply_markup=markup)  # show the keyboard

    else:
        bot.send_message(message.chat.id, "Your character name must be 4-16 between character:",reply_markup=types.ReplyKeyboardRemove())

def register_gender_handler(message):
    try:
        _thread.start_new_thread(delete_message_one_message, (message.chat.id, int(message.message_id-1)))
        _thread.start_new_thread(delete_message_one_message, (message.chat.id, message.message_id))
    except:
        pass
    if message.text not in GenderList:
        bot.send_message(message.from_user.id, message.text)
        bot.send_message(message.chat.id, "Please, choose correct gender:")
    else:
        avatar_item = AvatarsButton(message=message)
        avatar_item.free_avatars(c_id=message.from_user.id,action='')
        try:
            r = requests.post("{}".format(
                domain_name + '/bot-api/next-step-status/{}/'.format(message.from_user.id, )),
                data={
                    'ns_status': 'register_avatar_handler',
                    'gender': message.text,
                })
        except:
            bot.send_message(message.chat.id, "Try again choose your gender :")

def register_avatar_handler(message):
    try:
        _thread.start_new_thread(delete_message_one_message, (message.chat.id, int(message.message_id-1)))
        _thread.start_new_thread(delete_message_one_message, (message.chat.id, message.message_id))
    except:
        pass
    # bot.send_message(message.chat.id, "---------------- {} -----------------".format('register_avatar_handler'))
    avatar_item  = AvatarsButton(message=message)
    avatarList = avatar_item.free_avatars_list()
    if len(avatarList) == 0 or message.text not in list(avatarList):
        bot.send_message(message.from_user.id, message.text)
        bot.send_message(message.chat.id, "Try again adding your avatar:")
    else:
        try:
            r = requests.post('{}create-user/'.format(api_base_url),
                             data={
                                     'first_name': message.from_user.first_name,
                                     'last_name': message.from_user.last_name,
                                     'username': message.from_user.username,
                                     'm_id': message.chat.id,
                                     'avatar':message.text,
                                     })
            data = json.loads(r.text)
            if data['code'] == 1:
                key = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=False)
                key.row_width = 1
                key.add(
                    types.KeyboardButton(text=MAIN_MENU),
                )

                bot.send_message(chat_id=message.chat.id,text=data['message'],parse_mode='HTML',reply_markup=key)
                change_stategy(message)
            else:
                bot.send_message(message.from_user.id,"Choose avatar again")

        except:
            bot.send_message(message.from_user.id,"Try again")

def register_strategy_handler(message):
    try:
        _thread.start_new_thread(delete_message_one_message, (message.chat.id, int(message.message_id-1)))
        _thread.start_new_thread(delete_message_one_message, (message.chat.id, message.message_id))
    except:
        pass
    chat_id = copy.deepcopy(message.chat.id)
    general_message_id = copy.deepcopy(message.message_id)
    general_message = copy.deepcopy(message)
    avatar_item = AvatarsButton(message=general_message)
    avatar_item.free_avatars_by_message(c_id=chat_id, action=None, message_id=general_message_id)

def private_game_handler(message):
    chat_id = copy.deepcopy(message.chat.id)
    general_message_id = copy.deepcopy(message.message_id)
    general_message = copy.deepcopy(message)
    if message.text == PrivateGameList:
        try:
            _thread.start_new_thread(delete_message_one_message, (chat_id, general_message_id))
            _thread.start_new_thread(delete_message_one_message, (chat_id, int(general_message_id-1)))
        except:
            pass
        rsm = RegisterStepModel(chat_id=chat_id)
        rsm.set_status(ns_status='-', extra_value=None)
        mb = GameModel(chat_id=chat_id)
        mb.send_fight_menu_type(menu_type=GameMenuControllerList[0])
    else:
        try:
            _thread.start_new_thread(delete_message_one_message, (chat_id, general_message_id))
            _thread.start_new_thread(delete_message_one_message, (chat_id, int(general_message_id-1)))
        except:
            pass
        if phone_checker(text=message.text):
            gm = GameModel(chat_id=message.chat.id)
            gm.get_new_generate(type=GameMenuList[1],private_number=message.text,message_id=None)
        else:
            key = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=False)
            key.row_width = 1
            key.add(
                types.KeyboardButton(text=PrivateGameList),
            )
            bot.send_message(chat_id=message.chat.id,text="Write correct phone number or choose Cancel: \n For example +380956491025",reply_markup=key)
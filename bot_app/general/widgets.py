import _thread
import json

import requests
import telebot
from telebot import types

from bot_app.general.functions import delete_message_one_message
from bot_app.general.utils import *
from bot_app.tools.functions import change_stategy
from bot_app.tools.widget import AvatarsButton
from panel.models import Game
from tbgm.settings import TELEGRAM_BOT_TOKEN



bot = telebot.TeleBot(TELEGRAM_BOT_TOKEN)
bot2 = telebot.TeleBot(TELEGRAM_BOT_TOKEN)
domain_name = 'https://game.minbashi.com'
api_base_url = 'https://game.minbashi.com/bot-api/'



class RegisterStepModel:
    def __init__(self,chat_id):
        self.chat_id = chat_id

    def get_status(self):
        try:
            r = requests.get('{}next-step-status/{}/'.format(api_base_url, self.chat_id, ))
            data = json.loads(r.text)
            step = data['status']
        except:
            step = '-'
        return step

    def set_status(self,ns_status,extra_value):
        code = 0
        try:
            data={
                'ns_status': ns_status,
            }
            if extra_value:
                data['{}'.format(extra_value[0])] = extra_value[1]
            r = requests.post('{}next-step-status/{}/'.format(api_base_url, self.chat_id, ),
                data=data
            )
            data = json.loads(r.text)
            if data['code']:
                code = 1
        except:
            pass
        return code

class MenuButton:
    def __init__(self,message,text):
        self.message = message
        self.text = text
        self.parent = None

    def main_menu(self):
        key = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=False)
        key.row_width = 2
        key.add(
            types.KeyboardButton(text=MainMenuList[0]),
            types.KeyboardButton(text=MainMenuList[1]),
        )
        key.row_width = 2
        key.add(
            types.KeyboardButton(text=MainMenuList[2]),
            types.KeyboardButton(text=MainMenuList[3]),
            types.KeyboardButton(text=MainMenuList[4]),
            types.KeyboardButton(text=MainMenuList[5]),
        )
        bot.send_message(chat_id=self.message.from_user.id,text=self.text, reply_markup=key,parse_mode='HTML')

    def callback_main_menu(self,chat_id):
        key = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=False)
        key.row_width = 2
        key.add(
            types.KeyboardButton(text=MainMenuList[0]),
            types.KeyboardButton(text=MainMenuList[1]),
        )
        key.row_width = 2
        key.add(
            types.KeyboardButton(text=MainMenuList[2]),
            types.KeyboardButton(text=MainMenuList[3]),
            types.KeyboardButton(text=MainMenuList[4]),
            types.KeyboardButton(text=MainMenuList[5]),
        )
        bot.send_message(chat_id=chat_id,text=self.text, reply_markup=key,parse_mode='HTML')

    def change_language(self):
        markup = types.InlineKeyboardMarkup()
        markup.row_width = 3
        for lang_item in LanguageList:
            markup.add(
                types.InlineKeyboardButton("{}".format(lang_item), callback_data="{}".format(lang_item)),
            )
        bot.send_message(self.message.chat.id, 'Choose your language', reply_markup=markup,parse_mode='HTML')
    #
    # def parent_menu_show(self,parent):
    #     self.parent = parent
    #     if self.parent == MAIN_MENU:
    #         mb = MenuButton(message=self.message, text=self.parent)
    #         mb.main_menu()
    #     if self.parent == MainMenuList[0]:
    #         key = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=False)
    #         key.row_width = 2
    #         key.add(
    #             types.KeyboardButton(text=ParentMenuList[1]),
    #             types.KeyboardButton(text=ParentMenuList[2]),
    #         )
    #         key.row_width = 1
    #         key.add(
    #             types.KeyboardButton(text=GameMenuList[2]),
    #         )
    #         key.row_width = 1
    #         key.add(
    #             types.KeyboardButton(text=ParentMenuList[0]),
    #         )
    #         bot.send_message(chat_id=self.message.from_user.id,text=self.text, reply_markup=key)
    #     elif self.parent == MainMenuList[1]:
    #         change_stategy(self.message)
    #     elif self.parent == MainMenuList[2]:
    #         key = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=False)
    #         key.row_width = 2
    #         key.add(
    #             types.KeyboardButton(text=AvatarMenuList[0]),
    #             types.KeyboardButton(text=AvatarMenuList[1]),
    #         )
    #         key.add(
    #             types.KeyboardButton(text=ParentMenuList[0]),
    #         )
    #         bot.send_message(chat_id=self.message.from_user.id,text=self.text, reply_markup=key)
    #
    #     elif self.parent == MainMenuList[3]:
    #         key = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=False)
    #         key.row_width = 2
    #         key.add(
    #             types.KeyboardButton(text=AvatarItemTypeList[0]),
    #             types.KeyboardButton(text=AvatarItemTypeList[1]),
    #             types.KeyboardButton(text=AvatarItemTypeList[2]),
    #             types.KeyboardButton(text=AvatarItemTypeList[3]),
    #         )
    #         key.row_width = 1
    #         key.add(
    #             types.KeyboardButton(text=ParentMenuList[0]),
    #         )
    #         bot.send_message(chat_id=self.message.from_user.id,text=self.text, reply_markup=key)
    #     elif self.parent == MainMenuList[4]:
    #         pass
    #     elif self.parent == MainMenuList[5]:
    #         key = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=False)
    #         key.row_width = 1
    #         key.add(
    #             types.KeyboardButton(text=ParentMenuList[3]),
    #         )
    #         key.row_width = 1
    #         key.add(
    #             types.KeyboardButton(text=ParentMenuList[0]),
    #         )
    #         bot.send_message(chat_id=self.message.from_user.id,text=self.text, reply_markup=key)
    #
    #     elif self.parent == AvatarMenuList[0]:
    #         ab = AvatarsButton(message=self.message)
    #         ab.user_avatars()
    #     elif self.parent in GameMenuList:
    #         gm = GameModel(chat_id=self.message.chat.id)
    #         if self.parent == GameMenuList[1]:
    #             rsm = RegisterStepModel(chat_id=self.message.from_user.id)
    #             rsm.set_status(ns_status=AllHandlersList[3],extra_value=None)
    #             key = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=False)
    #             key.row_width = 1
    #             key.add(
    #                 types.KeyboardButton(text=PrivateGameList),
    #             )
    #             bot.send_message(chat_id=self.message.chat.id,text="Write your friend number or choose Cancel: \n For example +380956491025",reply_markup=key)
    #         elif self.parent == GameMenuList[0]:
    #             gm.get_new_generate(self.parent,None,None)
    #         elif self.parent == GameMenuList[2]:
    #             text = 'Menu'
    #             try:
    #                 r = requests.post('{}/bot-api/delete-all-games/{}/'.format(domain_name,self.message.from_user.id),)
    #                 data = json.loads(r.text)
    #                 text = data['message']
    #             except:
    #                 pass
    #             mb = MenuButton(message=self.message,text=text)
    #             mb.main_menu()
    #     elif self.parent == ParentMenuList[4]:
    #         self.main_menu()
    #         # dm = DeleteMessage(self.message)
    #         # dm.delete()
    #     elif self.parent == ParentMenuList[3]:
    #         self.change_language()


class GameEditModel():
    def __init__(self,g_id,u1_id,u2_id):
        self.g_id = g_id
        self.u1_id = u1_id
        self.u2_id = u2_id

    def finished_game_message(self):
        self.get_game_status()
    def updated_game_message(self):
        self.get_game_status()
    def get_game_finish_game(self):
        bot.send_message(chat_id=self.u1_id, text='Game finished', parse_mode='HTML')
        bot.send_message(chat_id=self.u2_id, text='Game finished', parse_mode='HTML')
    def get_game_status(self):
        try:
            r = requests.get('{}/bot-api/game-status-with-general-only/{}/'.format(domain_name,self.g_id),)
            data = json.loads(r.text)
            text = data['message']
            game_finished = data['game_finished']
            markup_result = data['markup_result']
            if data['code'] == 1:
                user1_id = markup_result['user1_id']
                user2_id = markup_result['user2_id']
                message1_id = markup_result['message1_id']
                message2_id = markup_result['message2_id']
                image1_url = data['image1_url']
                image2_url = data['image2_url']
                user1_text = data['user1_text']
                user2_text = data['user2_text']
                game_continue = data['game_continue']
                markup = types.InlineKeyboardMarkup()
                if game_finished is False:
                    markup.row_width = 2
                    markup.add(
                        types.InlineKeyboardButton(self.check_result(AtackChoices[0][1],markup_result['atack']),callback_data=AtackChoices[0][0]),
                        types.InlineKeyboardButton(self.check_result(DeffenceChoices[0][1],markup_result['atack']),callback_data=DeffenceChoices[0][0]),

                        types.InlineKeyboardButton(self.check_result(AtackChoices[1][1],markup_result['atack']), callback_data=AtackChoices[1][0]),
                        types.InlineKeyboardButton(self.check_result(DeffenceChoices[1][1],markup_result['atack']),callback_data=DeffenceChoices[1][0]),

                        types.InlineKeyboardButton(self.check_result(AtackChoices[2][1],markup_result['atack']), callback_data=AtackChoices[2][0]),
                        types.InlineKeyboardButton(self.check_result(DeffenceChoices[2][1],markup_result['atack']),callback_data=DeffenceChoices[2][0]),
                    )
                    markup.row_width = 1
                    markup.add(
                        types.InlineKeyboardButton(self.check_ready(GameReadyButton[1],markup_result['ready']),callback_data=GameReadyButton[0]),
                    )
                else:
                    markup.row_width = 2
                    markup.add(
                        types.InlineKeyboardButton(DeleteAndBackFightMenuControllerHandler[1],callback_data=DeleteAndBackFightMenuControllerHandler[0]),
                    )

                #     markup.row_width = 2
                #     markup.add(
                #         types.KeyboardButton(text=MainMenuList[0]),
                #         types.KeyboardButton(text=MainMenuList[1]),
                #     )
                #     markup.row_width = 2
                #     markup.add(
                #         types.KeyboardButton(text=MainMenuList[2]),
                #         types.KeyboardButton(text=MainMenuList[3]),
                #         types.KeyboardButton(text=MainMenuList[4]),
                #         types.KeyboardButton(text=MainMenuList[5]),
                #     )
                import _thread
                try:
                    _thread.start_new_thread(self.send_edit_caption, (self.g_id,user1_id, message1_id,user1_text,image1_url,markup,1))
                    _thread.start_new_thread(self.send_edit_caption, (self.g_id,user2_id, message2_id,user2_text,image2_url,markup,1))
                except:
                    print("Error: unable to start thread")
                # try:
                #     bot.edit_message_caption(chat_id=self.u1_id, caption=data['message'], parse_mode='HTML',
                #                              reply_markup=markup, message_id=message1_id)
                # except:
                #     bot.send_photo(chat_id=self.u1_id, caption=data['message'], photo=data['image_url'],
                #                parse_mode='HTML', reply_markup=markup)
                # try:
                #     bot.edit_message_caption(chat_id=self.u2_id, caption=data['message'], parse_mode='HTML',
                #                              reply_markup=markup, message_id=message2_id)
                # except:
                #     bot.send_photo(chat_id=self.u2_id, caption=data['message'], photo=data['image_url'],
                #                parse_mode='HTML', reply_markup=markup)
            else:
                bot.send_message(chat_id=self.u1_id, text=text, parse_mode='HTML')
                bot.send_message(chat_id=self.u2_id, text=text, parse_mode='HTML')
        except Exception as e:
            text = 'Try again = {}'.format(str(e))
            # bot.send_message(chat_id=chat_id,text=text, parse_mode='HTML')
            # mb = MenuButton(message=self.u1_id, text=text)
            # mb.callback_main_menu(self.u1_id)
            # mb.callback_main_menu(self.u2_id)
    def send_edit_caption(self,g_id,chat_id, message_id,message,image_url,markup,user_num):

        if message_id:
            bot.edit_message_caption(chat_id=chat_id, caption=message, parse_mode='HTML',
                                     reply_markup=markup, message_id=message_id)

    def check_result(self,main_t,result_t,):
        if main_t == result_t:
            return "{} {}".format("*",main_t)
        else:
            return main_t
    def check_ready(self,main_t,result_t,):
        if result_t:
            return "{} {}".format("*",main_t)
        else:
            return main_t

class GameModel():
    def __init__(self,chat_id):
        self.chat_id = chat_id
        self.type = None
        self.g_id = None


    def finish_the_started_game(self):
        text = ""
        try:
            url = "{}/bot-api/finish-the-started-game/{}/".format(domain_name, self.chat_id)
            r = requests.post(url)
            data = json.loads(r.text)
            code = data['code']
            if code == 1:
                self.get_finished_game_status(g_id=data['game_id'])
        except Exception as e:
            text = "{} - {}".format(text,str(e))
            bot.send_message(chat_id=self.chat_id,text=text)

    def get_finished_game_status(self,g_id):
        self.g_id = g_id
        try:
            r = requests.post('{}/bot-api/game-status-for-finished-canceled/{}/'.format(domain_name,self.g_id),)
            data = json.loads(r.text)
            game_finished = data['game_finished']
            markup_result = data['markup_result']
            if data['code'] == 1:
                message1_id = markup_result['message1_id']
                message2_id = markup_result['message2_id']
                # image1_url = data["image1_url"]
                # image2_url = data["image2_url"]
                user1_text = data['user1_text']
                user2_text = data['user2_text']
                user1_id = markup_result['user1_id']
                user2_id = markup_result['user2_id']
                game_continue = data['game_continue']
                markup = types.InlineKeyboardMarkup()
                markup.row_width = 2
                markup.add(
                    types.InlineKeyboardButton(DeleteAndBackFightMenuControllerHandler[1],callback_data=DeleteAndBackFightMenuControllerHandler[0]),
                )

                #     markup.row_width = 2
                #     markup.add(
                #         types.KeyboardButton(text=MainMenuList[0]),
                #         types.KeyboardButton(text=MainMenuList[1]),
                #     )
                #     markup.row_width = 2
                #     markup.add(
                #         types.KeyboardButton(text=MainMenuList[2]),
                #         types.KeyboardButton(text=MainMenuList[3]),
                #         types.KeyboardButton(text=MainMenuList[4]),
                #         types.KeyboardButton(text=MainMenuList[5]),
                #     )
                import _thread
                try:
                    _thread.start_new_thread(self.send_edit_caption, (user1_id, message1_id, user1_text,markup,1))
                    _thread.start_new_thread(self.send_edit_caption, (user2_id, message2_id, user2_text,markup,2))
                except:
                    print("Error: unable to start thread")
                # try:
                #     bot.edit_message_caption(chat_id=self.u1_id, caption=data['message'], parse_mode='HTML',
                #                              reply_markup=markup, message_id=message1_id)
                # except:
                #     bot.send_photo(chat_id=self.u1_id, caption=data['message'], photo=data['image_url'],
                #                parse_mode='HTML', reply_markup=markup)
                # try:
                #     bot.edit_message_caption(chat_id=self.u2_id, caption=data['message'], parse_mode='HTML',
                #                              reply_markup=markup, message_id=message2_id)
                # except:
                #     bot.send_photo(chat_id=self.u2_id, caption=data['message'], photo=data['image_url'],
                #                parse_mode='HTML', reply_markup=markup)
            else:
                pass
                # bot.send_message(chat_id=message1_id, text=text, parse_mode='HTML')
                # bot.send_message(chat_id=self.u2_id, text=text, parse_mode='HTML')
        except Exception as e:
            text = 'Try again = {}'.format(str(e))
            # bot.send_message(chat_id=chat_id,text=text, parse_mode='HTML')
            # mb = MenuButton(message=self.chat_id, text=text)
            # mb.callback_main_menu(self.chat_id)


    def send_edit_caption(self,chat_id,message_id,text,markup,user_num):
        bot.edit_message_caption(chat_id=chat_id, caption=text, parse_mode='HTML',
                                 reply_markup=markup, message_id=message_id)
    def send_main_fight_menu(self):
        avatar_image_url = ''
        code = 0
        text = "Start the game"
        url = ""
        try:
            url = "{}/bot-api/show-fight-menu/{}/".format(domain_name, self.chat_id)
            r = requests.post(url,data={"data":'nese'})
            data = json.loads(r.text)
            avatar_image_url = data['useravatarurl']
        except Exception as e:
            text = "{} - {}".format(text,str(e))
        markup = types.InlineKeyboardMarkup()
        markup.row_width = 1
        markup.add(
            types.InlineKeyboardButton("Fight", callback_data=GameMenuControllerList[0]),
        )
        user_main_message_url = "{}/bot-api/user-main-message/{}/".format(domain_name, self.chat_id)
        user_main_message_r = requests.get(user_main_message_url,)
        user_main_message_r_data = json.loads(user_main_message_r.text)
        message_id = user_main_message_r_data['user_main_message']
        try:
            if message_id:
                bot.edit_message_caption(chat_id=self.chat_id, message_id=message_id, reply_markup=markup, caption=text)
            else:
                if avatar_image_url:
                    menu_bot = bot.send_photo(chat_id=self.chat_id,caption=text,photo=avatar_image_url,reply_markup=markup)
                else:
                    menu_bot = bot.send_message(chat_id=self.chat_id,text=text,reply_markup=markup)
                if menu_bot.message_id:
                    user_main_message_url = "{}/bot-api/user-main-message/{}/".format(domain_name, self.chat_id)
                    user_main_message_r = requests.post(user_main_message_url, data={"user_main_message": '{}'.format(menu_bot.message_id)})
        except:

            try:
                _thread.start_new_thread(delete_message_one_message, (self.chat_id, message_id))
            except:
                pass
            if avatar_image_url:
                menu_bot = bot.send_photo(chat_id=self.chat_id,caption=text,photo=avatar_image_url,reply_markup=markup)
            else:
                menu_bot = bot.send_message(chat_id=self.chat_id,text=text,reply_markup=markup)
            if menu_bot.message_id:
                user_main_message_url = "{}/bot-api/user-main-message/{}/".format(domain_name, self.chat_id)
                user_main_message_r = requests.post(user_main_message_url, data={"user_main_message": '{}'.format(menu_bot.message_id)})
        # if avatar_image_url:
        #     menu_bot = bot.send_photo(chat_id=self.chat_id,caption=text,photo=avatar_image_url,reply_markup=markup)
        # else:
        #     menu_bot = bot.send_message(chat_id=self.chat_id,text=text,reply_markup=markup)
        # if menu_bot.message_id:
        #     user_main_message_url = "{}/bot-api/user-main-message/{}/".format(domain_name, self.chat_id)
        #     user_main_message_r = requests.post(user_main_message_url, data={"user_main_message": '{}'.format(menu_bot.message_id)})

    def send_waiting_fight_menu(self):
        avatar_image_url = ''
        code = 0
        text = "You are waiting the game"
        url = ""
        try:
            url = "{}/bot-api/show-fight-menu/{}/".format(domain_name, self.chat_id)
            r = requests.post(url,data={"data":'nese'})
            data = json.loads(r.text)
            avatar_image_url = data['useravatarurl']
        except Exception as e:
            text = "{} - {}".format(text,str(e))
        markup = types.InlineKeyboardMarkup()
        markup.row_width = 1
        markup.add(
            types.InlineKeyboardButton(GameMenuPlacesControllerList[4], callback_data=GameMenuControllerList[4]),
        )
        user_main_message_url = "{}/bot-api/user-main-message/{}/".format(domain_name, self.chat_id)
        user_main_message_r = requests.get(user_main_message_url,)
        user_main_message_r_data = json.loads(user_main_message_r.text)
        message_id = user_main_message_r_data['user_main_message']
        try:
            if message_id:
                bot.edit_message_caption(chat_id=self.chat_id, message_id=message_id, reply_markup=markup, caption=text)
            else:
                if avatar_image_url:
                    menu_bot = bot.send_photo(chat_id=self.chat_id,caption=text,photo=avatar_image_url,reply_markup=markup)
                else:
                    menu_bot = bot.send_message(chat_id=self.chat_id,text=text,reply_markup=markup)
                if menu_bot.message_id:
                    user_main_message_url = "{}/bot-api/user-main-message/{}/".format(domain_name, self.chat_id)
                    user_main_message_r = requests.post(user_main_message_url, data={"user_main_message": '{}'.format(menu_bot.message_id)})
        except:

            try:
                _thread.start_new_thread(delete_message_one_message, (self.chat_id, message_id))
            except:
                pass
            if avatar_image_url:
                menu_bot = bot.send_photo(chat_id=self.chat_id,caption=text,photo=avatar_image_url,reply_markup=markup)
            else:
                menu_bot = bot.send_message(chat_id=self.chat_id,text=text,reply_markup=markup)
            if menu_bot.message_id:
                user_main_message_url = "{}/bot-api/user-main-message/{}/".format(domain_name, self.chat_id)
                user_main_message_r = requests.post(user_main_message_url, data={"user_main_message": '{}'.format(menu_bot.message_id)})
        # r = requests.post('{}/bot-api/use-referal-api/{}/'.format(domain_name, self.chat_id),
        #              data={'referal-game':g_u_id}
        #              )



    def edit_main_fight_menu(self,menu_type,message_id):
        avatar_image_url = ''
        code = 0
        text = "Start the game"
        url = ""
        try:
            url = "{}/bot-api/show-fight-menu/{}/".format(domain_name, self.chat_id)
            r = requests.post(url,data={"data":'nese'})
            data = json.loads(r.text)
            avatar_image_url = data['useravatarurl']
        except Exception as e:
            text = "{} - {}".format(text,str(e))
        markup = types.InlineKeyboardMarkup()

        if menu_type == GameMenuControllerList[0]:
            text = "Start the game"
            markup.row_width = 2
            markup.add(
                types.InlineKeyboardButton(GameMenuPlacesControllerList[1], callback_data=GameMenuControllerList[1]),
                types.InlineKeyboardButton(GameMenuPlacesControllerList[2], callback_data=GameMenuControllerList[2]),
            )
            markup.row_width = 1
            markup.add(
                types.InlineKeyboardButton(GameMenuPlacesControllerList[3], callback_data=GameMenuControllerList[3]),
            )
        elif menu_type == GameMenuTypesList[1]:
            text = "You are waiting the game"
            markup.row_width = 1
            markup.add(
                types.InlineKeyboardButton(GameMenuPlacesControllerList[4], callback_data=GameMenuControllerList[4]),
            )
        else:
            markup.row_width = 1
            markup.add(
                types.InlineKeyboardButton(GameMenuPlacesControllerList[0], callback_data=GameMenuControllerList[0]),
            )
        bot.edit_message_caption(chat_id=self.chat_id,message_id=message_id,reply_markup=markup,caption=text)

    def send_fight_menu_type(self,menu_type):
        avatar_image_url = ''
        code = 0
        text = "Start the game"
        url = ""
        try:
            url = "{}/bot-api/show-fight-menu/{}/".format(domain_name, self.chat_id)
            r = requests.post(url,data={"data":'nese'})
            data = json.loads(r.text)
            avatar_image_url = data['useravatarurl']
        except Exception as e:
            text = "{} - {}".format(text,str(e))
        markup = types.InlineKeyboardMarkup()

        if menu_type == GameMenuControllerList[0]:
            text = "Start the game"
            markup.row_width = 2
            markup.add(
                types.InlineKeyboardButton(GameMenuPlacesControllerList[1], callback_data=GameMenuControllerList[1]),
                types.InlineKeyboardButton(GameMenuPlacesControllerList[2], callback_data=GameMenuControllerList[2]),
            )
            markup.row_width = 1
            markup.add(
                types.InlineKeyboardButton(GameMenuPlacesControllerList[3], callback_data=GameMenuControllerList[3]),
            )
        elif menu_type == GameMenuTypesList[1]:
            text = "You are waiting the game"
            markup.row_width = 1
            markup.add(
                types.InlineKeyboardButton(GameMenuPlacesControllerList[4], callback_data=GameMenuControllerList[4]),
            )
        else:
            text = "Start the game"
            markup.row_width = 1
            markup.add(
                types.InlineKeyboardButton(GameMenuPlacesControllerList[0], callback_data=GameMenuControllerList[0]),
            )
        user_main_message_url = "{}/bot-api/user-main-message/{}/".format(domain_name, self.chat_id)
        user_main_message_r = requests.get(user_main_message_url,)
        user_main_message_r_data = json.loads(user_main_message_r.text)
        message_id = user_main_message_r_data['user_main_message']
        try:
            if message_id:
                bot.edit_message_caption(chat_id=self.chat_id, message_id=message_id, reply_markup=markup, caption=text)
            else:
                if avatar_image_url:
                    menu_bot = bot.send_photo(chat_id=self.chat_id,caption=text,photo=avatar_image_url,reply_markup=markup)
                else:
                    menu_bot = bot.send_message(chat_id=self.chat_id,text=text,reply_markup=markup)
                if menu_bot.message_id:
                    user_main_message_url = "{}/bot-api/user-main-message/{}/".format(domain_name, self.chat_id)
                    user_main_message_r = requests.post(user_main_message_url, data={"user_main_message": '{}'.format(menu_bot.message_id)})
        except:

            try:
                _thread.start_new_thread(delete_message_one_message, (self.chat_id, message_id))
            except:
                pass
            if avatar_image_url:
                menu_bot = bot.send_photo(chat_id=self.chat_id,caption=text,photo=avatar_image_url,reply_markup=markup)
            else:
                menu_bot = bot.send_message(chat_id=self.chat_id,text=text,reply_markup=markup)
            if menu_bot.message_id:
                user_main_message_url = "{}/bot-api/user-main-message/{}/".format(domain_name, self.chat_id)
                user_main_message_r = requests.post(user_main_message_url, data={"user_main_message": '{}'.format(menu_bot.message_id)})
        # if avatar_image_url:
        #     bot.send_photo(chat_id=self.chat_id,caption=text,photo=avatar_image_url,reply_markup=markup)
        # else:
        #     bot.send_message(chat_id=self.chat_id,text=text,reply_markup=markup)
        # r = requests.post('{}/bot-api/use-referal-api/{}/'.format(domain_name, self.chat_id),
        #              data={'referal-game':g_u_id}
        #              )


    def send_fight_menu_type_with_text(self,menu_type,extra_text=None):
        avatar_image_url = ''
        code = 0
        text = "Start the game {}".format("\n{}".format(extra_text) if extra_text else "")
        url = ""
        try:
            url = "{}/bot-api/show-fight-menu/{}/".format(domain_name, self.chat_id)
            r = requests.post(url,data={"data":'nese'})
            data = json.loads(r.text)
            avatar_image_url = data['useravatarurl']
        except Exception as e:
            text = "{} - {}".format(text,str(e))
        markup = types.InlineKeyboardMarkup()

        if menu_type == GameMenuControllerList[0]:
            text = "Start the game {}".format("\n{}".format(extra_text) if extra_text else "")
            markup.row_width = 2
            markup.add(
                types.InlineKeyboardButton(GameMenuPlacesControllerList[1], callback_data=GameMenuControllerList[1]),
                types.InlineKeyboardButton(GameMenuPlacesControllerList[2], callback_data=GameMenuControllerList[2]),
            )
            markup.row_width = 1
            markup.add(
                types.InlineKeyboardButton(GameMenuPlacesControllerList[3], callback_data=GameMenuControllerList[3]),
            )
        elif menu_type == GameMenuTypesList[1]:
            text = "You are waiting the game {}".format("\n{}".format(extra_text) if extra_text else "")
            markup.row_width = 1
            markup.add(
                types.InlineKeyboardButton(GameMenuPlacesControllerList[4], callback_data=GameMenuControllerList[4]),
            )
        else:
            text = "Start the game {}".format("\n{}".format(extra_text) if extra_text else "")
            markup.row_width = 1
            markup.add(
                types.InlineKeyboardButton(GameMenuPlacesControllerList[0], callback_data=GameMenuControllerList[0]),
            )
        user_main_message_url = "{}/bot-api/user-main-message/{}/".format(domain_name, self.chat_id)
        user_main_message_r = requests.get(user_main_message_url,)
        user_main_message_r_data = json.loads(user_main_message_r.text)
        message_id = user_main_message_r_data['user_main_message']
        try:
            if message_id:
                bot.edit_message_caption(chat_id=self.chat_id, message_id=message_id, reply_markup=markup, caption=text)
            else:
                if avatar_image_url:
                    menu_bot = bot.send_photo(chat_id=self.chat_id,caption=text,photo=avatar_image_url,reply_markup=markup)
                else:
                    menu_bot = bot.send_message(chat_id=self.chat_id,text=text,reply_markup=markup)
                if menu_bot.message_id:
                    user_main_message_url = "{}/bot-api/user-main-message/{}/".format(domain_name, self.chat_id)
                    user_main_message_r = requests.post(user_main_message_url, data={"user_main_message": '{}'.format(menu_bot.message_id)})
        except:

            try:
                _thread.start_new_thread(delete_message_one_message, (self.chat_id, message_id))
            except:
                pass
            if avatar_image_url:
                menu_bot = bot.send_photo(chat_id=self.chat_id,caption=text,photo=avatar_image_url,reply_markup=markup)
            else:
                menu_bot = bot.send_message(chat_id=self.chat_id,text=text,reply_markup=markup)
            if menu_bot.message_id:
                user_main_message_url = "{}/bot-api/user-main-message/{}/".format(domain_name, self.chat_id)
                user_main_message_r = requests.post(user_main_message_url, data={"user_main_message": '{}'.format(menu_bot.message_id)})
        # if avatar_image_url:
        #     bot.send_photo(chat_id=self.chat_id,caption=text,photo=avatar_image_url,reply_markup=markup)
        # else:
        #     bot.send_message(chat_id=self.chat_id,text=text,reply_markup=markup)
        # r = requests.post('{}/bot-api/use-referal-api/{}/'.format(domain_name, self.chat_id),
        #              data={'referal-game':g_u_id}
        #              )


    def game_referal(self,g_u_id):
        try:
            r = requests.post('{}/bot-api/use-referal-api/{}/'.format(domain_name, self.chat_id),
                         data={'referal-game':g_u_id}
                         )
            data = json.loads(r.text)
        except:
            pass
        # if data['code'] == 1:
        #     self.game_acception_markup(text=data['message'], g_id=g_id)

    def game_acception_markup(self,text,image_url,g_id):

        markup = types.InlineKeyboardMarkup()
        markup.row_width = 2
        markup.add(
            types.InlineKeyboardButton(GameStatusChoices[3][1], callback_data="{}&{}".format(GameStatusChoices[3][0],g_id)),
            types.InlineKeyboardButton(GameStatusChoices[4][1], callback_data="{}&{}".format(GameStatusChoices[4][0],g_id)),
        )
        try:
            if image_url:
                bot.send_photo(chat_id=self.chat_id,caption=text,photo=image_url,reply_markup=markup)
            else:
                bot.send_message(chat_id=self.chat_id,text=text,reply_markup=markup)
        except Exception as e:
            bot.send_message(chat_id=self.chat_id,text="{}.---{}".format(text,str(e)),reply_markup=markup)

    def game_acception_choosen(self,g_id):
        self.g_id = g_id
        try:
            r = requests.get('{}/bot-api/start-the-game/{}/{}/'.format(domain_name,self.chat_id,self.g_id),)
            data = json.loads(r.text)
            if data['code'] == 1:
                # self.star
                self.game_acception_markup(text=data['message'],image_url=data['image_url'],g_id=g_id)

            elif data['code'] == 3:
                pass
            else:
                bot.send_message(chat_id=self.chat_id,text=data['message'])
        except Exception as e:
            bot.send_message(chat_id=self.chat_id,text=str(e))

    # def game_acception_choosen(self,g_id):
    #     self.g_id = g_id
    #     try:
    #         r = requests.get('{}/bot-api/start-the-game/{}/{}/'.format(domain_name,self.chat_id,self.g_id),)
    #         data = json.loads(r.text)
    #         if data['code'] == 1:
    #             self.game_acception_markup(text=data['message'],image_url=data['image_url'],g_id=g_id)
    #
    #         elif data['code'] == 3:
    #             pass
    #         else:
    #             bot.send_message(chat_id=self.chat_id,text=data['message'])
    #     except Exception as e:
    #         bot.send_message(chat_id=self.chat_id,text=str(e))
    def get_new_generate(self,type,private_number,message_id):
        self.type = type
        try:
            post_data = {}
            if private_number:
                post_data['private_number'] = private_number
            r = requests.post('{}/bot-api/generate-new-game/{}/{}/'.format(domain_name,self.chat_id,self.type),
                              data=post_data
                              )
            data = json.loads(r.text)
            game_code = data['game_code']
            if game_code == "send-main":
                if message_id:
                    self.edit_main_fight_menu(menu_type=GameMenuControllerList[0],message_id=message_id)
                else:
                    self.send_main_fight_menu()
            elif game_code == "send-game-waiting-menu":
                if message_id:
                    self.edit_main_fight_menu(menu_type=GameMenuTypesList[1],message_id=message_id)
                else:
                    self.send_fight_menu_type(menu_type=GameMenuTypesList[1])
            elif game_code == "send-friend-text":

                if message_id:
                    self.edit_main_fight_menu(menu_type=GameMenuTypesList[1],message_id=message_id)
                else:
                    self.send_fight_menu_type(menu_type=GameMenuTypesList[1])
                bot.send_message(self.chat_id,text=data['message'],parse_mode="HTML")
        except:
            text = 'Try again'

    def game_acception(self,g_id,choice,message_id):
        self.g_id = g_id
        text = ''
        try:
            r = requests.post('{}/bot-api/start-the-game/{}/{}/'.format(domain_name,self.chat_id,self.g_id),
                              data={'choice':choice})
            data = json.loads(r.text)
            if data['code'] == 1:
                pass
            # elif data['code'] == 11:
            #     pass
            elif data['code'] == 0:
                text = data['message']
                self.send_fight_menu_type_with_text(menu_type=GameMenuControllerList[0], extra_text=text)

        except Exception as e:
            pass
            # text = 'Something happend {}'.format(str(e))
            # bot.send_message(chat_id=self.chat_id,text=text)
        return text

    def game_acception_code(self,g_id,choice,message_id):
        self.g_id = g_id
        code = 0
        try:
            r = requests.post('{}/bot-api/start-the-game/{}/{}/'.format(domain_name,self.chat_id,self.g_id),
                              data={'choice':choice})
            data = json.loads(r.text)
            code = data['code']
            if data['code'] == 1:
                pass
            # elif data['code'] == 11:
            #     pass
            elif data['code'] == 0:
                text = data['message']
                self.send_fight_menu_type_with_text(menu_type=GameMenuControllerList[0], extra_text=text)

        except Exception as e:
            pass
            # text = 'Something happend {}'.format(str(e))
            # bot.send_message(chat_id=self.chat_id,text=text)
        return code

    def check_private_game_status(self):
        g_uid = ""
        try:
            url = "{}/bot-api/check-private-game/{}/".format(domain_name, self.chat_id)
            r = requests.post(url)
            data = json.loads(r.text)
            if data['code'] == 1 and data['g_u_id']:
                g_uid = data['g_u_id']
        except:
            pass
        return g_uid

    # def get_remove_all(self,type):
    #     self.type = type
    #     try:
    #         r = requests.post('{}/bot-api/generate-new-game/{}/{}/'.format(domain_name,self.chat_id,self.type),)
    #         data = json.loads(r.text)
    #         text = data['message']
    #     except:
    #         text = 'Try again'
    #     # bot.send_message(chat_id=self.chat_id,text=text, parse_mode='HTML')
    #     mb = MenuButton(message=self.chat_id, text=text)
    #     mb.callback_main_menu(self.chat_id)


    def get_game_status(self):
        try:
            r = requests.post('{}/bot-api/game-status/{}/'.format(domain_name,self.chat_id),)
            data = json.loads(r.text)
            text = data['message']
            markup_result = data['markup_result']
            if data['code'] == 1:

                markup = types.InlineKeyboardMarkup()
                markup.row_width = 2
                markup.add(
                    types.InlineKeyboardButton(self.check_result_generation(AtackChoices[0][1],AtackChoices[0][0],markup_result['atack']),callback_data=AtackChoices[0][0]),
                    types.InlineKeyboardButton(self.check_result_generation(DeffenceChoices[0][1],DeffenceChoices[0][0],markup_result['deffence']),callback_data=DeffenceChoices[0][0]),

                    types.InlineKeyboardButton(self.check_result_generation(AtackChoices[1][1],AtackChoices[1][0],markup_result['atack']), callback_data=AtackChoices[1][0]),
                    types.InlineKeyboardButton(self.check_result_generation(DeffenceChoices[1][1],DeffenceChoices[1][0],markup_result['deffence']),callback_data=DeffenceChoices[1][0]),

                    types.InlineKeyboardButton(self.check_result_generation(AtackChoices[2][1],AtackChoices[2][0],markup_result['atack']), callback_data=AtackChoices[2][0]),
                    types.InlineKeyboardButton(self.check_result_generation(DeffenceChoices[2][1],DeffenceChoices[2][0],markup_result['deffence']),callback_data=DeffenceChoices[2][0]),
                )
                markup.row_width = 1
                markup.add(
                    types.InlineKeyboardButton(GameLeaveButton[1],callback_data=GameLeaveButton[0]),
                )
                bot.send_photo(chat_id=self.chat_id,caption=data['user_text'],photo=data['image_url'],parse_mode='HTML',reply_markup=markup)
            else:
                bot.send_message(chat_id=self.chat_id,text=text, parse_mode='HTML')
        except Exception as e:
            text = '3232----Try again {}'.format(str(e))
            # bot.send_message(chat_id=self.chat_id,text=text, parse_mode='HTML')
            # mb = MenuButton(message=self.chat_id, text=text)
            # mb.callback_main_menu(self.chat_id)


    def get_game_status_send_all_user(self, chat1_id, chat2_id,g_id):
        user1MessageID = 0
        user2MessageID = 0
        message = ''
        try:
            headers = {
            'accept': "application/json",
            # 'apikey': "cd6b6c96799847698d87dec9f9a731d6"
            }
            url = '{}/bot-api/game-status-witget-starth-general-for-start/{}/'.format(domain_name,g_id)
            r = requests.post('{}/bot-api/game-status-witget-starth-general-for-start/{}/'.format(domain_name,g_id))
            r.headers = headers
            # r = requests.get('{}/bot-api/game-status-with-general-only/{}/'.format(domain_name,g_id),)
            message = r.text
            #
            # bot.send_message(chat_id=int(chat1_id), text="{}".format(r.status_code), parse_mode='HTML')
            # bot.send_message(chat_id=int(chat1_id), text="{}".format(url), parse_mode='HTML')
            data = json.loads(message)
            text = data['message']
            image1_url = data["image1_url"]
            image2_url = data["image2_url"]
            user1_text = data['user1_text']
            user2_text = data['user2_text']

            markup_result = data['markup_result']
            user1ID = markup_result['user1_id']
            user2ID = markup_result['user2_id']
            if data['code'] == 1:

                markup = types.InlineKeyboardMarkup()
                markup.row_width = 2
                markup.add(
                    types.InlineKeyboardButton(self.check_result(AtackChoices[0][1],markup_result['atack']),callback_data=AtackChoices[0][0]),
                    types.InlineKeyboardButton(self.check_result(DeffenceChoices[0][1],markup_result['atack']),callback_data=DeffenceChoices[0][0]),

                    types.InlineKeyboardButton(self.check_result(AtackChoices[1][1],markup_result['atack']), callback_data=AtackChoices[1][0]),
                    types.InlineKeyboardButton(self.check_result(DeffenceChoices[1][1],markup_result['atack']),callback_data=DeffenceChoices[1][0]),

                    types.InlineKeyboardButton(self.check_result(AtackChoices[2][1],markup_result['atack']), callback_data=AtackChoices[2][0]),
                    types.InlineKeyboardButton(self.check_result(DeffenceChoices[2][1],markup_result['atack']),callback_data=DeffenceChoices[2][0]),
                )
                markup.row_width = 1
                markup.add(
                    types.InlineKeyboardButton(GameLeaveButton[1],callback_data=GameLeaveButton[0]),
                )
                # Create two threads as follows
                import _thread
                try:

                    userMessage1 = bot.send_photo(chat_id=int(user1ID), caption=user1_text, photo=image1_url,parse_mode='HTML', reply_markup=markup)
                    userMessage2 = bot.send_photo(chat_id=int(user2ID), caption=user2_text, photo=image2_url,parse_mode='HTML', reply_markup=markup)
                    game_obj = Game.objects.filter(id=g_id).get()
                    game_obj.user_message1 = userMessage1.message_id
                    game_obj.user_message2 = userMessage2.message_id
                    game_obj.round = 1
                    game_obj.save()


                    try:
                        user_main_message_r1 = requests.get("{}/bot-api/user-main-message/{}/".format(domain_name, user1ID), )
                        user_main_message1_r_data = json.loads(user_main_message_r1.text)
                        message1_id = user_main_message1_r_data['user_main_message']
                        if message1_id:
                            _thread.start_new_thread(delete_message_one_message, (self.chat_id, message1_id))
                    except:
                        pass
                    try:
                        user_main_message_r2 = requests.get("{}/bot-api/user-main-message/{}/".format(domain_name, user2ID), )
                        user_main_message2_r_data = json.loads(user_main_message_r2.text)
                        message2_id = user_main_message2_r_data['user_main_message']
                        if message2_id:
                            _thread.start_new_thread(delete_message_one_message, (self.chat_id, message2_id))
                    except:
                        pass
                    # _thread.start_new_thread(self.send_caption, (g_id,user1ID, user1_text,image1_url,markup,1))
                    # _thread.start_new_thread(self.send_caption, (g_id,user2ID, user2_text,image2_url,markup,2))
                except:
                    print("Error: unable to start thread")

                # user1Message = bot.send_photo(chat_id=int(chat1_id), caption=data['message'], photo=data['image_url'], parse_mode='HTML', reply_markup=markup)
                # user2Message = bot2.send_photo(chat_id=int(chat2_id), caption=data['message'], photo=data['image_url'], parse_mode='HTML', reply_markup=markup)
                # user1MessageID = user1Message.message_id
                # user2MessageID = user2Message.message_id
            else:
                bot.send_message(chat_id=int(chat1_id),text=text, parse_mode='HTML')
                bot2.send_message(chat_id=int(chat2_id),text=text, parse_mode='HTML')
        except Exception as e:
            text = 's----Try again {}'.format(str(e))
            # bot.send_message(chat_id=self.chat_id,text=text, parse_mode='HTML')
        return [user1MessageID,user2MessageID]

    def send_caption(self,g_id,chat_id,message, image_url,markup,user_num):
        try:
            userMessage = bot.send_photo(chat_id=int(chat_id), caption=message, photo=image_url,
                                          parse_mode='HTML', reply_markup=markup)
            game_obj = Game.objects.filter(id=g_id).get()
            if user_num == 1:
                game_obj.user_message1 = userMessage.message_id
            if user_num == 2:
                game_obj.user_message2 = userMessage.message_id
            game_obj.save()
            if game_obj.user_message1 and game_obj.user_message2:
                game_obj.round = 1
            game_obj.save()

        except Exception as e:
            pass


    def check_result_generation(self,main_t,main_k,result_t,):
        if main_k in result_t:
            return "{} {}".format("*",main_t)
        else:
            return main_t

    def check_result(self,main_t,result_t,):
        if main_t in result_t:
            return "{} {}".format("*",main_t)
        else:
            return main_t
    def check_ready(self,main_t,result_t,):
        if result_t:
            return "{} {}".format("*",main_t)
        else:
            return main_t


    def get_game_status_data(self,markup_result,message_id):
        # self.type = type
        try:
            r = requests.post('{}/bot-api/game-status/{}/'.format(domain_name,self.chat_id),)
            data = json.loads(r.text)
            text = data['message']
            if data['code'] == 1:
                # bot.send_message(chat_id=self.chat_id,text="text = {} = {}".format(str(markup_result['atack']),str(markup_result['atack'])))
                markup = types.InlineKeyboardMarkup()
                markup.row_width = 2
                markup.add(
                    types.InlineKeyboardButton(self.check_result_generation(AtackChoices[0][1],AtackChoices[0][0],markup_result['atack']),callback_data=AtackChoices[0][0]),
                    types.InlineKeyboardButton(self.check_result_generation(DeffenceChoices[0][1],DeffenceChoices[0][0],markup_result['deffence']),callback_data=DeffenceChoices[0][0]),

                    types.InlineKeyboardButton(self.check_result_generation(AtackChoices[1][1],AtackChoices[1][0],markup_result['atack']), callback_data=AtackChoices[1][0]),
                    types.InlineKeyboardButton(self.check_result_generation(DeffenceChoices[1][1],DeffenceChoices[1][0],markup_result['deffence']),callback_data=DeffenceChoices[1][0]),

                    types.InlineKeyboardButton(self.check_result_generation(AtackChoices[2][1],AtackChoices[2][0],markup_result['atack']), callback_data=AtackChoices[2][0]),
                    types.InlineKeyboardButton(self.check_result_generation(DeffenceChoices[2][1],DeffenceChoices[2][0],markup_result['deffence']),callback_data=DeffenceChoices[2][0]),
                )
                markup.row_width = 1
                markup.add(
                    types.InlineKeyboardButton(GameLeaveButton[1],callback_data=GameLeaveButton[0]),
                )
                bot.edit_message_caption(chat_id=self.chat_id,caption=data['user_text'],parse_mode='HTML',reply_markup=markup,message_id=message_id)
            else:
                bot.send_message(chat_id=self.chat_id,text=text, parse_mode='HTML')
        except Exception as e:
            text = 'Try again {} '.format(str(e))
            # bot.send_message(chat_id=self.chat_id,text=text, parse_mode='HTML')
            # mb = MenuButton(message=self.chat_id, text=text)
            # mb.callback_main_menu(self.chat_id)


class StrategyModel:
    def __init__(self,message):
        self.message = message
    def change_stategy(self):
        # r2 = requests.get("{}".format(
        #     domain_name + '/bot-api/only-delete-messages/{}/{}/'.format(message.chat.id, message.message_id, )),
        #     params={'c_id': message.chat.id,
        #             'l_m_id': message.message_id})
        markup = types.InlineKeyboardMarkup()
        e_ok = False
        image_url = ''
        text = "Try again 2"
        try:
            r = requests.get('{}/bot-api/my-strategy-status/{}/'.format(domain_name, self.message.chat.id), )
            data = json.loads(r.text)
            result_data = data['result_data']
            markup.row_width = 2
            markup.add(
                types.InlineKeyboardButton("Strength {}".format(result_data['strength']), callback_data="cb_strength_plus"),
                types.InlineKeyboardButton("Quickness {}".format(result_data['quickness']), callback_data="cb_quickness_plus"),
                types.InlineKeyboardButton("Intuition {}".format(result_data['intuition']), callback_data="cb_intuition_plus"),
                types.InlineKeyboardButton("Durability {}".format(result_data['durability']), callback_data="cb_durability_plus"),
            )
            markup.row_width = 1
            markup.add(
                types.InlineKeyboardButton("Reset ( {} )".format(result_data['total_point']), callback_data="cb_reset"),
                types.InlineKeyboardButton(MainBackText, callback_data=BackRegisterHandler[2]),
            )
            markup.row_width = 1
            if result_data['total_point']<=0:
                markup.add(
                    types.InlineKeyboardButton("Fight", callback_data="FightCallbackKey"),
                )
            image_url = result_data['image_url']
            if data['code'] and image_url:
                text = data['message']
                e_ok = True
        except Exception as e:
            text = str(e)

        if e_ok:
            bot.send_photo(chat_id=self.message.chat.id, caption=text, photo=image_url, reply_markup=markup)
        else:
            bot.send_message(self.message.chat.id, text)

class ValidateModel:

    def phone_number_checker(self,text):
        import re
        pattern1 = re.compile("^(\+)([0-9]{2})(\d{9})$")
        pattern2 = re.compile("^(\+)([0-9]{3})(\d{9})$")
        if pattern1.match(str(text).strip()) or pattern2.match(str(text).strip()):
            return True
        else:
            return False
    def name_checker(self,text,regex_pattern):
        # ^ (?=.{4, 16}$)[a - zA - Z] + (?:[-'\s][a-zA-Z]+)*$
        import re
        if re.compile(regex_pattern).match(str(text).strip()):
            return True
        else:
            return False

    def name_checker_utf8(self,text):
        import re
        text_data = re.findall('\w+', text.replace("_", ""), re.UNICODE)
        text_data = "".join(text_data)
        if text_data == text and len(text) >= 4 and len(text) <= 16 and self.checkNumbers(text) is False:
            return True
        else:
            return False
    def checkNumbers(self,str):
        import re
        array = re.findall(r'[0-9]+', str)
        return True if len(array) > 0 else False
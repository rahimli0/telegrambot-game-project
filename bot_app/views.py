import copy
import datetime
from django.contrib.auth import get_user_model
from django.shortcuts import render
# Create your views here.

from bot_app.functions import to_number
from bot_app.general.utils import AllHandlersList, GenderChoicesList, EmptyCallbackHandler, AvatarNextPreviewHandler, \
    CallDataList, GameMenuControllerList, DeleteAndBackFightMenuControllerHandler, GameLeaveButton, \
    PrivateGameAcceptRejectControllerHandler
from bot_app.general.widgets import RegisterStepModel, GameModel, StrategyModel
from bot_app.general.utils import commands, GenderChoices, GenderList, AvatarList, MainMenuList, ParentMenuList, \
    LanguageList, AtackDeffenceList, GameReadyButton, GameStatusChoices, PrivateGameList, GameMenuList
from bot_app.tools.functions import change_stategy
from bot_app.tools.tools import game_accept_cancel_callback
from bot_app.tools.widget import EmployeesButton, AvatarsButton, DeleteMessage
from tbgm.settings import TELEGRAM_BOT_TOKEN
import telebot
from bot_app.general.functions import *
from bot_app.general.handler_functions import *
import _thread


from telebot.types import ReplyKeyboardMarkup, KeyboardButton, CallbackQuery, InlineKeyboardMarkup, InlineKeyboardButton
GUser = get_user_model()
# https://api.telegram.org/bot1000027230:AAErocEWBsbTndnCwJPidhVszIhnk3jSnmM/setWebhook?url=https://www.book-plays.com//bot-api/update/

bot = telebot.TeleBot(TELEGRAM_BOT_TOKEN)

domain_name = 'https://game.minbashi.com'
image_domain_name = 'https://game.minbashi.com'
# menu - Operation menu
# me - About myself
# setpoint - Set point to user
# show_section_user_info - Show user info
# show_user_info - Show section user info
# user_info - Show user info




class UpdateBot(APIView):
    def post(self,request):
        json_string = request.body.decode("UTF-8")
        update = telebot.types.Update.de_json(json_string)
        bot.process_new_updates([update])
        # bot.process_new_updates([telebot.types.Update.de_json(request.stream.read().decode("utf-8"))])
        return Response({'code':200})


def extract_unique_code(text):
    # Extracts the unique_code from the sent /start command.
    return text.split()[1] if len(text.split()) > 1 else None


@bot.message_handler(commands=['start'])
def start(message):
    general_message = copy.deepcopy(message)
    auth_check_data = auth_check(general_message)

    unique_code = extract_unique_code(general_message.text)

    gModel = GameModel(chat_id=general_message.chat.id)
    if unique_code:
        gModel.game_referal(unique_code)
    else:
        gModel.game_referal('-')

    if auth_check_data[0]:
        if unique_code:
            gm = GameModel(chat_id=general_message.chat.id)
            gm.game_acception(g_id=unique_code,choice=GameStatusChoices[3][0],message_id=None)
            # bot.send_message(text=str(unique_code),chat_id=message.chat.id)
        else:
            if auth_check_data[2] == USERGAMESTATUS[0][0]:

                gm = GameModel(chat_id=general_message.chat.id)
                gm.send_main_fight_menu()
                import _thread
                try:
                    _thread.start_new_thread(delete_message_one_message,(message.chat.id, message.message_id))
                except:
                    pass
            else:
                pass


@bot.callback_query_handler(func=lambda call: True)
def callback_query(call):
    chat_id = copy.deepcopy(call.message.chat.id)
    general_message_id = copy.deepcopy(call.message.message_id)
    general_message = copy.deepcopy(call.message)
    call_data = copy.deepcopy(call.data)
    if call_data == EmptyCallbackHandler:
        bot.answer_callback_query(call.id, '')
    elif call_data == GameLeaveButton[0]:
        gm = GameModel(chat_id=chat_id)
        gm.finish_the_started_game()
        bot.answer_callback_query(call.id, "{}".format('Canceled'))
    elif call_data == PrivateGameAcceptRejectControllerHandler[0][0] or  call_data == PrivateGameAcceptRejectControllerHandler[1][0]:
        if call_data == PrivateGameAcceptRejectControllerHandler[1][0]:
            pass
        if call_data == PrivateGameAcceptRejectControllerHandler[1][0]:
            try:
                _thread.start_new_thread(delete_message_one_message, (chat_id, general_message_id))
            except:
                pass
            gm = GameModel(chat_id=chat_id)
            gm.send_fight_menu_type_with_text(menu_type=GameMenuControllerList[0],extra_text="You have rejected the game")
            g_uid = gm.check_private_game_status()
            if g_uid:
                gm.game_acception_code(g_id=g_uid,choice=GameStatusChoices[4][0],message_id=None)


    elif call_data == DeleteAndBackFightMenuControllerHandler[0]:
        try:
            _thread.start_new_thread(delete_message_one_message, (chat_id, general_message_id))
        except:
            pass
        gm = GameModel(chat_id=chat_id)
        gm.send_main_fight_menu()
            # bot.answer_callback_query(call.id, str(call_data).replace("ControllerHandler",''))
    elif call_data in GameMenuControllerList:
        if call_data == 'CancelTheGameControllerHandler':
            try:
                r = requests.post('{}/bot-api/delete-all-games/{}/'.format(domain_name, chat_id), )
                data = json.loads(r.text)
                gm = GameModel(chat_id=chat_id)
                gm.edit_main_fight_menu(menu_type=GameMenuControllerList[0],message_id=general_message_id)
                bot.answer_callback_query(call.id, "Okay")
            except:
                bot.answer_callback_query(call.id, str(call_data))
                pass
        elif call_data == GameMenuControllerList[0] or call_data == GameMenuControllerList[3]:
            gm = GameModel(chat_id=chat_id)
            gm.edit_main_fight_menu(call_data,general_message_id)
            bot.answer_callback_query(call.id, str(call_data).replace("ControllerHandler",''))
        elif call_data == GameMenuControllerList[1]:
            gm = GameModel(chat_id=chat_id)
            gm.get_new_generate(type=str(call_data).replace("ControllerHandler",''),private_number=None,message_id=general_message_id)
            bot.answer_callback_query(call.id, "")
        elif call_data == GameMenuControllerList[2]:
            # bot.answer_callback_query(call.id, GameMenuControllerList[2])
            try:
                _thread.start_new_thread(delete_message_one_message, (chat_id, general_message_id))
            except:
                pass
            try:
                rsm = RegisterStepModel(chat_id=chat_id)
                rsm.set_status(ns_status=AllHandlersList[5],extra_value=None)
                key = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=False)
                key.row_width = 1
                key.add(
                    types.KeyboardButton(text=PrivateGameList),
                )
                bot.send_message(chat_id=chat_id,text="Write your friend number or choose Cancel: \n For example +380956491025",reply_markup=key)
            except Exception as e:
                bot.send_message(chat_id=chat_id,text="{}".format(e))
        else:
            bot.answer_callback_query(call.id, "call_data = {}".format(call_data))
    elif call_data in BackRegisterHandler:
        bot.delete_message(chat_id=chat_id,message_id=general_message_id)
        if call_data == BackRegisterHandler[0]:
            r = requests.post('{}/bot-api/next-step-status/{}/'.format(domain_name,chat_id),
                data={
                    'ns_status': 'register_character_name_handler',
                })
            bot.send_message(chat_id, "Please enter your character name:")
        elif call_data == BackRegisterHandler[1]:
            r = requests.post('{}/bot-api/next-step-status/{}/'.format(domain_name,chat_id),
                data={
                    'ns_status': 'register_gender_handler',
                })
            markup = types.InlineKeyboardMarkup()
            markup.row_width = 2
            for GenderListItem in GenderList:
                markup.add(
                    types.InlineKeyboardButton("{}".format(GenderListItem), callback_data="{}".format(GenderListItem)),
                )
            markup.row_width = 1
            markup.add(
                types.InlineKeyboardButton("{}".format(MainBackText), callback_data="{}".format(BackRegisterHandler[0])),
            )
            bot.send_message(chat_id, "Please select your gender:", reply_markup=markup)  # show the keyboard
        elif call_data == BackRegisterHandler[2]:
            r = requests.post("{}".format(
                domain_name + '/bot-api/next-step-status/{}/'.format(chat_id, )),
                data={
                    'ns_status': 'register_avatar_handler',
                })
            avatar_item = AvatarsButton(message=general_message)
            avatar_item.free_avatars(c_id=chat_id,action='')
        elif call_data == BackRegisterHandler[3]:
            r = requests.post("{}".format(
                domain_name + '/bot-api/next-step-status/{}/'.format(chat_id, )),
                data={
                    'ns_status': 'register_strategy_handler',
                })
            strategy_item = StrategyModel(message=general_message)
            strategy_item.change_stategy()

    elif call_data in AvatarNextPreviewHandler:
        avatar_item = AvatarsButton(message=general_message)
        avatar_item.free_avatars_by_message(c_id=chat_id,action=call_data,message_id=general_message_id)
        bot.answer_callback_query(call.id, '')
    elif call_data in GenderChoicesList:
        bot.delete_message(chat_id=chat_id,message_id=general_message_id)
        try:
            r = requests.post("{}".format(
                domain_name + '/bot-api/next-step-status/{}/'.format(chat_id, )),
                data={
                    'ns_status': 'register_avatar_handler',
                    'gender': call_data,
                })
            avatar_item = AvatarsButton(message=general_message)
            avatar_item.free_avatars(c_id=chat_id,action='')

        except:
            bot.answer_callback_query(call.id, 'Try again choose your gender')
            markup = types.InlineKeyboardMarkup()
            markup.row_width = 2
            for GenderListItem in GenderList:
                markup.add(
                    types.InlineKeyboardButton("{}".format(GenderListItem), callback_data="{}".format(GenderListItem)),
                )
            markup.row_width = 1
            markup.add(
                types.InlineKeyboardButton("{}".format(MainBackText), callback_data="{}".format(BackRegisterHandler[0])),
            )
            bot.send_message(chat_id, "Please select your gender:", reply_markup=markup)  # show the keyboard

    if call_data in CallDataList:

        if call_data == "FightCallbackKey":
            try:
                _thread.start_new_thread(delete_message_one_message, (chat_id, general_message_id))
            except:
                pass
            # bot.answer_callback_query(call.id, call_data)
            try:
                r = requests.post('{}/bot-api/check-create-or-edit-user/{}/'.format(domain_name ,chat_id ),)
                data = json.loads(r.text)
                if data['start_game_status']:
                    # r_user_referal = requests.get('{}/bot-api/use-referal-api/{}/'.format(domain_name ,chat_id ),)
                    data_user_referal = data['start_game_status']
                    if data_user_referal and data_user_referal != '-':
                        gm = GameModel(chat_id=chat_id)
                        code = gm.game_acception_code(g_id=data_user_referal,choice=GameStatusChoices[1][0],message_id=0)
                        if code != 1:
                            gm = GameModel(chat_id=chat_id)
                            gm.send_main_fight_menu()
                        # gModel = GameModel(chat_id=chat_id)
                        # gModel.game_acception_choosen(data_user_referal['referal'])
                    else:
                        gm = GameModel(chat_id=chat_id)
                        gm.send_main_fight_menu()
                    # bot.answer_callback_query(call.id, data['message'])
                else:
                    gm = GameModel(chat_id=chat_id)
                    gm.send_main_fight_menu()
                try:
                    _thread.start_new_thread(delete_message_one_message, (chat_id, general_message_id))
                except:
                    pass
                # bot.answer_callback_query(call.id, data['message'])
            except Exception as e:
                bot.answer_callback_query(call.id, 'error = {}'.format(str(e)))
            # if data['code'] == 1:
            #     bot.delete_message(chat_id=chat_id,message_id=general_message_id)
            #     r_user_referal = requests.get('{}/bot-api/use-referal-api/{}/'.format(domain_name ,chat_id ),)
            #     data_user_referal = json.loads(r_user_referal.text)
            #     if data_user_referal['referal'] and data_user_referal['referal'] != '-':
            #         gModel = GameModel(chat_id=chat_id)
            #         gModel.game_acception_choosen(data_user_referal['referal'])
            #     else:
            #         mb = MenuButton(message=call.message, text='Menu')
            #         mb.callback_main_menu(chat_id)
            #     bot.answer_callback_query(call.id, data['message'])
            # else:
            #     bot.answer_callback_query(call.id, data['message'])
            # bot.answer_callback_query(call.id, data['message'])
            # show_menu(message=call.message)
        else:
            r = requests.post('{}/bot-api/my-strategy-change/{}/'.format(domain_name ,chat_id),
                              data={'call_data':call_data}
                             )
            data = json.loads(r.text)
            if data['code']:
                result_data = data['result_data']
                markup = InlineKeyboardMarkup()
                markup.row_width = 2
                markup.add(
                    types.InlineKeyboardButton("Strength {}".format(result_data['strength']), callback_data="cb_strength_plus"),
                    types.InlineKeyboardButton("Quickness {}".format(result_data['quickness']), callback_data="cb_quickness_plus"),
                    types.InlineKeyboardButton("Intuition {}".format(result_data['intuition']), callback_data="cb_intuition_plus"),
                    types.InlineKeyboardButton("Durability {}".format(result_data['durability']), callback_data="cb_durability_plus"),
                )
                markup.row_width = 1
                markup.add(
                    types.InlineKeyboardButton("Reset ( {} )".format(result_data['total_point']), callback_data="cb_reset"),
                    types.InlineKeyboardButton(MainBackText, callback_data=BackRegisterHandler[2]),
                )
                markup.row_width = 1
                if result_data['total_point']<=0:
                    markup.add(
                        types.InlineKeyboardButton("Fight", callback_data="FightCallbackKey"),
                    )
                if result_data['data_changed']:
                    bot.edit_message_reply_markup(chat_id=chat_id,message_id=general_message_id,reply_markup=markup)
                # bot.send_message(text=data['message'],chat_id=chat_id,reply_markup=markup)
                bot.answer_callback_query(call.id,"{}".format(result_data['total_point']))
            else:
                bot.answer_callback_query(call.id, "Unsuccessfully. Something happened")
            bot.answer_callback_query(call.id, data['message'])

        # except Exception as e:
        #     text = "@{}, please try again later".format(str(e))
        #     bot.answer_callback_query(call.id, text)
    # bot.answer_callback_query(call.id, general_message_id)
    # call_data = call.data.replace('_{}'.format(user_id), '')
    # call_data_list = ["cb_strength_plus", "cb_quickness_plus", "cb_intuition_plus", "cb_durability_plus",
    #                   "FightCallbackKey", "cb_reset",]
    elif call_data in AtackDeffenceList:
        try:
            r = requests.post("{}".format(domain_name + '/bot-api/game-action/{}/'.format(chat_id )),
                             data={'atack_def':call_data}
                             )
            data = json.loads(r.text)
            if data['code'] == 1:
                gm = GameModel(chat_id=chat_id)
                gm.get_game_status_data(markup_result=data['markup_result'],message_id=general_message_id)
                bot.answer_callback_query(call.id, 'You chose')

            elif data['code'] == 2:
                bot.answer_callback_query(call.id, 'You have already chosen {}'.format(general_message_id))
            else:
                bot.answer_callback_query(call.id, 'Try to choose agin - {}'.format(data['code']))

        except:
            pass

    # elif call_data == 'Show Menu':
    #     mb = MenuButton(message=call.message, text='Menu')
    #     mb.callback_main_menu(user_id)
    #     bot.answer_callback_query(call.id, LanguageList[3])
    #     # dm = DeleteMessage(call.message)
    #     # dm.delete_call(user_id,general_message_id)
    # elif game_accept_cancel_callback(call_data)[0] != '':
    #     import _thread
    #     try:
    #         _thread.start_new_thread(delete_message_one_message, (user_id,general_message_id))
    #     except:
    #         print("Error: unable to start thread")
    #     data_result = game_accept_cancel_callback(call_data)
    #     # bot.answer_callback_query(call.id, data_result[0])
    #     gModel = GameModel(chat_id=user_id)
    #     text = gModel.game_acception(data_result[1],data_result[0],general_message_id)
    #     bot.answer_callback_query(call.id, text)
    #
    # elif call_data in LanguageList:
    #     try:
    #         r = requests.post("{}".format(domain_name + '/bot-api/set-user-language/{}/'.format(user_id )),
    #                          data={'language':call_data}
    #                          )
    #         data = json.loads(r.text)
    #         text = data['message']
    #     except:
    #         text = 'Try again'
    #     bot.answer_callback_query(call.id, text)
    # bot.answer_callback_query(call.id, 'Try again')



@bot.message_handler(func=lambda message: True, content_types=['text','contact'])
def command_default(message):
    # if check_main_menu(message=message):
    #     menu_handler(message=message)
    # else:
    general_message = copy.deepcopy(message)
    step_handler_state = check_step_handlerstate(message=general_message)
    if step_handler_state in AllHandlersList:
        if step_handler_state == AllHandlersList[0]:
            register_contact_handler(message=general_message)
        elif step_handler_state == AllHandlersList[1]:
            register_character_name_handler(message=general_message)
        elif step_handler_state == AllHandlersList[2]:
            register_gender_handler(message=general_message)
        elif step_handler_state == AllHandlersList[3]:
            register_avatar_handler(message=general_message)
        elif step_handler_state == AllHandlersList[4]:
            register_strategy_handler(message=general_message)
        elif step_handler_state == AllHandlersList[5]:
            private_game_handler(message=general_message)
    else:
        auth_check_data = auth_check(general_message)
        if auth_check_data[0]:
            if auth_check_data[2] == USERGAMESTATUS[0][0]:

                gm = GameModel(chat_id=general_message.chat.id)
                gm.send_main_fight_menu()
                import _thread
                try:
                    _thread.start_new_thread(delete_message_one_message,(message.chat.id, message.message_id))
                except:
                    pass
            else:
                pass







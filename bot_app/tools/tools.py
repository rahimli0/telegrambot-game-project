from threading import Timer

from bot_app.general.utils import GameStatusChoices


def game_accept_cancel_callback(text):
    return_val = ['','']
    try:
        if GameStatusChoices[3][0] in text:
            return_val = [GameStatusChoices[3][0],text.split('&')[1],]
        elif GameStatusChoices[4][0] in text:
            return_val = [GameStatusChoices[4][0],text.split('&')[1],]
    except:
        pass
    return return_val

class RepeatedTimer(object):
    def __init__(self, interval, function, *args, **kwargs):
        self._timer = None
        self.interval = interval
        self.function = function
        self.args = args
        self.kwargs = kwargs
        self.is_running = False
        self.result = False
        self.start()

    def _run(self):
        self.is_running = False
        self.start()
        self.result = self.function(*self.args, **self.kwargs)

        if self.result[0]:
            self.stop()
        else:
            print(self.result)

    def start(self):

        if not self.is_running:
            self._timer = Timer(self.interval, self._run)
            self._timer.start()
            self.is_running = True

    def stop(self):
        self._timer.cancel()
        self.is_running = False


import json
from io import BytesIO

import requests
from PIL import Image
from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton

from bot_app.general.utils import MainMenuList, ParentMenuList, LanguageList, AvatarItemTypeList, AvatarMenuList, \
    GameMenuList, AtackChoices, DeffenceChoices, GameReadyButton, GameStatusChoices, MainBackText, BackRegisterHandler, \
    EmptyCallbackHandler, AvatarNextPreviewHandler
from bot_app.tools.functions import change_stategy
from tbgm.settings import TELEGRAM_BOT_TOKEN
import telebot
from telebot import types

from telebot import formatter


class DeleteMessage():
    def __init__(self,messsage):
        self.message = messsage
        self.user_id = None
        self.message_id = None
    def delete(self):
        try:
            r2 = requests.get('{}/bot-api/only-delete-messages/{}/{}/'.format(domain_name,self.message.chat.id, self.message.message_id, ), )
            return True
        except:
            return False
    def delete_dosms(self):
        try:
            r2 = requests.get('{}/bot-api/delete-messages/{}/{}/'.format(domain_name,self.message.chat.id, self.message.message_id),
                             params={'c_id': self.message.chat.id,'l_m_id': self.message.message_id })
            return True
        except:
            return False
    def delete_call(self,user_id,message_id):
        self.user_id = user_id
        self.message_id = message_id
        try:
            r2 = requests.get('{}/bot-api/only-delete-messages/{}/{}/'.format(domain_name,self.user_id, self.message_id, ), )
            return True
        except:
            return False


bot = telebot.TeleBot(TELEGRAM_BOT_TOKEN)
domain_name = 'https://game.minbashi.com'



class AvatarsButton:
    def __init__(self,message):
        self.message = message
        self.user_id = None
        self.type = None
    def free_avatars(self,c_id,action):
        try:
            val_data = {
                'action': action
            }
            r = requests.post("{}/bot-api/user-avatar-list-with-image/{}/".format(domain_name,str(c_id)),
                              data=val_data
                              )
            data = json.loads(r.text)
            # key = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
            # print(data)
            markup = types.InlineKeyboardMarkup()
            markup.row_width = 3
            # for GenderListItem in GenderList:
            markup.add(
                    types.InlineKeyboardButton("{}".format('<'), callback_data="{}".format(AvatarNextPreviewHandler[0])),
                    types.InlineKeyboardButton("{}".format('{} / {}'.format(data['user_avatar_index'],len(data['list']))), callback_data="{}".format(EmptyCallbackHandler)),
                    types.InlineKeyboardButton("{}".format('>'), callback_data="{}".format(AvatarNextPreviewHandler[1])),
            )
            markup.row_width = 1
            markup.add(
                types.InlineKeyboardButton("{}".format('Select'), callback_data="{}".format(BackRegisterHandler[3])),
                types.InlineKeyboardButton("{}".format(MainBackText), callback_data="{}".format(BackRegisterHandler[1])),
            )
            # msg = bot.reply_to(message, data['result'])
            text = data['text']
            if data['code'] == 200:
                pass
                # for i in range(len(data['list'])):
                #     # button = KeyboardButton(text=data['list'][i]['username'])
                #     key.add(types.KeyboardButton(text=data['list'][i]['title']))
                bot.send_photo(chat_id=c_id, caption=text, photo="{}{}".format('',data['result_image']), reply_markup=markup)
            else:
                # pass
                bot.send_message(c_id, text)
        except Exception as e:
            pass
            # bot.send_message(c_id, "{} - {}  -- {}".format(c_id,action,str(e)))
        return True
    def free_avatars_by_message(self,c_id,action,message_id):
        try:
            val_data = {
                'action': action
            }
            r = requests.post("{}/bot-api/user-avatar-list-with-image/{}/".format(domain_name,c_id),
                              data=val_data
                              )
            data = json.loads(r.text)
            # key = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
            # print(data)
            markup = types.InlineKeyboardMarkup()
            markup.row_width = 3
            # for GenderListItem in GenderList:
            markup.add(
                    types.InlineKeyboardButton("{}".format('<'), callback_data="{}".format(AvatarNextPreviewHandler[0])),
                    types.InlineKeyboardButton("{}".format('{} / {}'.format(data['user_avatar_index'],len(data['list']))), callback_data="{}".format(EmptyCallbackHandler)),
                    types.InlineKeyboardButton("{}".format('>'), callback_data="{}".format(AvatarNextPreviewHandler[1])),
            )
            markup.row_width = 1
            markup.add(
                types.InlineKeyboardButton("{}".format('Select'), callback_data="{}".format(BackRegisterHandler[3])),
                types.InlineKeyboardButton("{}".format(MainBackText), callback_data="{}".format(BackRegisterHandler[1])),
            )
            # msg = bot.reply_to(message, data['result'])
            text = data['text']
            if data['code'] == 200:
                pass
                # for i in range(len(data['list'])):
                #     # button = KeyboardButton(text=data['list'][i]['username'])
                #     key.add(types.KeyboardButton(text=data['list'][i]['title']))
                if data['change_markup'] and data['result_image']:
                    # response = requests.get("/media/photos/avatar/all/2020/03/13/avatar_09L5M2j_zKXgktG.png")
                    # img = Image.open(BytesIO(response.content))
                    # img  = Image.open(data['result_image'])
                    # file_data_2 = open(data['result_image'], 'rb')
                    # bot.edit_message_media(chat_id=c_id,message_id=message_id, media=data['text'], reply_markup=markup)
                    # bot.edit_message_media(chat_id=c_id, message_id=message_id, media=types.InputMediaPhoto(media=img, caption=text), reply_markup=markup)
                    bot.edit_message_caption(chat_id=c_id,message_id=message_id, caption="{}".format(data['text']),reply_markup=markup)
            else:
                bot.send_message(c_id, text)
        except Exception as e:
            bot.send_message(c_id, "{} - {}  -- {}".format(c_id,action,str(e)))
        return True
    def free_avatars_list(self):
        return_val = []
        try:
            r = requests.get("{}".format(domain_name + '/bot-api/free-avatars-title/'),
                params={'first_name': self.message.from_user.first_name, 'last_name': self.message.from_user.last_name, })
            data = json.loads(r.text)
            if data['code'] == 200:
                return_val = data['list']
        except Exception as e:
            pass
        return return_val
    def user_avatars(self):
        r = requests.post("{}".format('{}/bot-api/get-user-avatars/{}/'.format(domain_name,self.message.chat.id)),)
        data = json.loads(r.text)
        markup = InlineKeyboardMarkup()
        markup.row_width = 3
        for data_item in data['list']:
            markup.add(
                InlineKeyboardButton("{}".format(data_item), callback_data="{}".format(data_item)),
            )
        bot.send_photo(chat_id=self.message.chat.id, photo=data['html_list'], reply_markup=markup)

        # print(data)



def EmployeesButton(message):
    try:
        r = requests.get("{}".format(domain_name + '/bot-api/get-employees-text/{}/{}/'.format(message.from_user.id, message.from_user.username, )),
            params={'first_name': message.from_user.first_name, 'last_name': message.from_user.last_name, })
        data = json.loads(r.text)
        key = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
        print(data)
        # msg = bot.reply_to(message, data['result'])
        text = data['text']
        if data['code'] == 200:
            for i in range(len(data['list'])):
                # button = KeyboardButton(text=data['list'][i]['username'])
                key.add(types.KeyboardButton(text=data['list'][i]['username']))
            bot.send_message(message.from_user.id,text, reply_markup=key)
        else:
            bot.send_message(message.chat.id, text)
    except Exception as e:
        bot.send_message(message.chat.id, str(e))
    return True


class ReplyKeyboardMarkupCustom:
    def __init__(self,message):
        self.message = message
    def register(self):
        try:
            r = requests.get("{}".format(domain_name + '/bot-api/get-employees-text/{}/{}/'.format(self.message.from_user.id, self.message.from_user.username, )),
                params={'first_name': self.message.from_user.first_name, 'last_name': self.message.from_user.last_name, })
            data = json.loads(r.text)
            key = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
            print(data)
            # msg = bot.reply_to(message, data['result'])
            text = data['text']
            if data['code'] == 200:
                for i in range(len(data['list'])):
                    # button = KeyboardButton(text=data['list'][i]['username'])
                    key.add(types.KeyboardButton(text=data['list'][i]['username']))
                bot.send_message(self.message.from_user.id,text, reply_markup=key)
            else:
                bot.send_message(self.message.chat.id, text)
        except Exception as e:
            bot.send_message(self.message.chat.id, str(e))
        return True
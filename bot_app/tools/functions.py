import json

import requests
import telebot
from telebot import types
from telebot.types import InlineKeyboardButton, InlineKeyboardMarkup

from bot_app.general.utils import LanguageList, MainBackText, BackRegisterHandler
from tbgm.settings import TELEGRAM_BOT_TOKEN

bot = telebot.TeleBot(TELEGRAM_BOT_TOKEN)
domain_name = 'https://game.minbashi.com'



def change_stategy(message):
    # r2 = requests.get("{}".format(
    #     domain_name + '/bot-api/only-delete-messages/{}/{}/'.format(message.chat.id, message.message_id, )),
    #     params={'c_id': message.chat.id,
    #             'l_m_id': message.message_id})
    markup = InlineKeyboardMarkup()
    e_ok = False
    text = "Try again 2"
    try:
        r = requests.get('{}/bot-api/my-strategy-status/{}/'.format(domain_name, message.chat.id), )
        data = json.loads(r.text)
        result_data = data['result_data']
        markup.row_width = 2
        markup.add(
            types.InlineKeyboardButton("Strength {}".format(result_data['strength']), callback_data="cb_strength_plus"),
            types.InlineKeyboardButton("Quickness {}".format(result_data['quickness']), callback_data="cb_quickness_plus"),
            types.InlineKeyboardButton("Intuition {}".format(result_data['intuition']), callback_data="cb_intuition_plus"),
            types.InlineKeyboardButton("Durability {}".format(result_data['durability']), callback_data="cb_durability_plus"),
        )
        markup.row_width = 1
        markup.add(
            types.InlineKeyboardButton("Reset ( {} )".format(result_data['total_point']), callback_data="cb_reset"),
            types.InlineKeyboardButton(MainBackText, callback_data=BackRegisterHandler[2]),
        )
        markup.row_width = 1
        if result_data['total_point']<=0:
            markup.add(
                types.InlineKeyboardButton("Fight", callback_data="FightCallbackKey"),
            )
        if data['code']:
            text = data['message']
            e_ok = True
    except Exception as e:
        text = str(e)

    if e_ok:
        bot.send_message(message.chat.id, text, reply_markup=markup)
    else:
        bot.send_message(message.chat.id, text)
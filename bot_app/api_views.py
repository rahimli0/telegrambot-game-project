import copy
import hashlib
import json
import random

import requests
import telebot
from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from django.db.models import Q
from django.shortcuts import render
# Create your views here.
from django.urls import reverse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from django.utils.translation import ugettext as _

from base_user.tools.common import USERGAMESTATUS
from bot_app.general.functions import delete_message_one_message
from panel.models import MainSettings, Avatar, UserAvatar, AllUserStatus, Game, GameStatusChoices, GameTypeChoices, \
    GameAction, AtackChoices, DeffenceChoices, GameReadyButton, AvatarGenderType
from tbgm.settings import TELEGRAM_BOT_NAME, TELEGRAM_BOT_TOKEN
from .functions import make_avatar_total_images, make_game_image
from .general.widgets import RegisterStepModel
from .general.utils import GenderChoices, AvatarPaidFreeChoices, AvatarNextPreviewHandler, TOTALUSERGAMEPOINT, \
    CallDataList, TOTALUSERGAMESTARTPOINT, GameMenuControllerList
from .general.widgets import GameModel
from .views import domain_name
import _thread

GUser = get_user_model()

class AvatarListWithImage(APIView):
    def post(self,request):
        text = ''
        code = 403
        data = {'list':[],'html_list':''}
        # try:
        gender = request.POST.get('gender',None)
        if gender is None:
            gender = GenderChoices[0][0]
        data_list = Avatar.objects.filter(is_active=True,is_deleted=False,gender=gender).order_by('status')
        html_list = ''
        list_data = []
        if data_list:
            for item in data_list:
                list_data.append((item.title,str(item.photo)))
                # html_list = "{}<div><img style='width:150px;' src='{}{}'></div>".format(html_list,domain_name,item.avatar.photo.url)
            avatar_gender_type_obj = AvatarGenderType.objects.filter(gender=gender,paid_status=AvatarPaidFreeChoices[0][0]).first()
            data['list'] = data['list'] = data_list.values('id', 'title')
            data['html_list'] = "{}{}".format(domain_name,avatar_gender_type_obj.photo.url if avatar_gender_type_obj and avatar_gender_type_obj.photo else make_avatar_total_images(list_data))
            code = 200
        else:
            text = "{}".format(_("You have no any avatar"))
            code = 201
        # except:
        #     text = "{}".format(_("You have no permission for this action"))
        data['code'] = code
        data['text'] = text
        return Response(data=data)


class UserAvatarListWithImage(APIView):
    def post(self,request,c_id):
        text = ''
        user_avatar_index = 1
        code = 403
        result_image = '-'
        data = {'list':[],'html_list':''}
        # try:
        # gender = request.POST.get('gender',None)
        action = request.POST.get('action',None)
        change_markup = False
        try:
            user_status = AllUserStatus.objects.filter(chat_id=c_id).get()
        except:
            user_status = AllUserStatus(chat_id=c_id).save()
        if user_status.gender:
            gender = user_status.gender
        else:
            gender = GenderChoices[0][0]
        data_list = Avatar.objects.filter(is_active=True,is_deleted=False,gender=gender).order_by('status')
        data_list_ids = []
        list_data = []
        if data_list:
            data_list_ids = data_list.values_list('id',flat=True)
            html_list = ''
            if action:
                user_avatar_index = list(data_list_ids).index(user_status.avatar_id) + 1 if user_status.avatar_id in data_list_ids else 1
                if action == AvatarNextPreviewHandler[0]:
                    if user_avatar_index > 1:
                        change_markup = True
                        user_avatar_index -= 1
                    else:
                        user_avatar_index = 1
                elif action == AvatarNextPreviewHandler[1]:
                    if user_avatar_index < len(list(data_list_ids)):
                        change_markup = True
                        user_avatar_index += 1
                data_list_i = 1
                for data_list_item in data_list:
                    if data_list_i == user_avatar_index:
                        user_status.avatar = data_list_item
                        user_status.save()
                        break
                    data_list_i += 1
                if user_status.avatar_id in data_list_ids:

                    user_avatar_id = user_status.avatar_id
                else:
                    user_avatar_id = data_list[0]

            else:
                user_avatar_index = list(data_list_ids).index(user_status.avatar_id) + 1 if user_status.avatar_id in data_list_ids else 1
                if user_status.avatar_id in data_list_ids:
                    pass
                    # user_avatar_id = user_status.avatar_id
                else:
                    user_status.avatar_id = data_list[0]
                user_status.save()

            user_status = AllUserStatus.objects.filter(chat_id=c_id).get()
            if user_status.avatar:
                result_image = str(user_status.avatar.photo)
            # else:
            #     result_image = '-'
            # data['html_list'] = str(user_status.avatar.photo.url) if user_status.avatar and user_status.avatar.photo else ''

            # for item in data_list:
            #     list_data.append((item.title,str(item.photo)))
            #     # html_list = "{}<div><img style='width:150px;' src='{}{}'></div>".format(html_list,domain_name,item.avatar.photo.url)
            # avatar_gender_type_obj = AvatarGenderType.objects.filter(gender=gender,paid_status=AvatarPaidFreeChoices[0][0]).first()
            text = "Please select your avatar"
            data['list'] = data_list.values('id', 'title')
            data['change_markup'] = change_markup
            data['user_avatar_index'] = user_avatar_index
            # data['html_list'] = "{}{}".format(domain_name,avatar_gender_type_obj.photo.url if avatar_gender_type_obj and avatar_gender_type_obj.photo else make_avatar_total_images(list_data))
            code = 200
        else:
            text = "{}".format(_("You have no any avatar"))
            code = 201
        # except:
        #     text = "{}".format(_("You have no permission for this action"))
        if result_image:
            data['result_image'] = "http://game.minbashi.com/media/{}".format(result_image)
        else:
            data['result_image'] = ""
        data['code'] = code
        data['text'] = text
        return Response(data=data)
    def get(self,request,c_id):
        text = ''
        user_avatar_index = 1
        code = 403
        result_image = ''
        data = {'list':[],'html_list':''}
        # try:
        # gender = request.POST.get('gender',None)
        action = request.GET.get('action',None)
        try:
            user_status = AllUserStatus.objects.get(chat_id=c_id)
        except:
            user_status = AllUserStatus(chat_id=c_id).save()
        if user_status.gender:
            gender = user_status.gender
        else:
            gender = GenderChoices[0][0]
        data_list = Avatar.objects.filter(is_active=True,is_deleted=False,gender=gender).order_by('status')
        data_list_ids = []
        list_data = []
        if data_list:
            data_list_ids = data_list.values_list('id',flat=True)
            html_list = ''
            if action:
                user_avatar_index = list(data_list_ids).index(user_status.avatar_id) + 1 if user_status.avatar_id in data_list_ids else 1
                if action == AvatarNextPreviewHandler[0]:
                    user_avatar_index = user_avatar_index - 1 if user_avatar_index > 1 else 1
                elif action == AvatarNextPreviewHandler[1]:
                    user_avatar_index = user_avatar_index + 1
                data_list_i = 1
                for data_list_item in data_list:
                    if data_list_i == user_avatar_index:
                        user_status.avatar = data_list_item
                        user_status.save()
                        break
                    data_list_i += 1
                if user_status.avatar_id in data_list_ids:

                    user_avatar_id = user_status.avatar_id
                else:
                    user_avatar_id = data_list[0]

            else:
                user_avatar_index = list(data_list_ids).index(user_status.avatar_id) + 1 if user_status.avatar_id in data_list_ids else 1
                if user_status.avatar_id in data_list_ids:
                    pass
                    # user_avatar_id = user_status.avatar_id
                else:
                    user_status.avatar_id = data_list[0]
                user_status.save()
            if user_status.avatar and user_status.avatar.photo:
                result_image = str(user_status.avatar.photo.url)
            else:
                result_image = '-'
            # data['html_list'] = str(user_status.avatar.photo.url) if user_status.avatar and user_status.avatar.photo else ''

            # for item in data_list:
            #     list_data.append((item.title,str(item.photo)))
            #     # html_list = "{}<div><img style='width:150px;' src='{}{}'></div>".format(html_list,domain_name,item.avatar.photo.url)
            # avatar_gender_type_obj = AvatarGenderType.objects.filter(gender=gender,paid_status=AvatarPaidFreeChoices[0][0]).first()
            text = "Please select your avatar"
            data['list'] = data_list.values('id', 'title')
            data['user_avatar_index'] = user_avatar_index
            # data['html_list'] = "{}{}".format(domain_name,avatar_gender_type_obj.photo.url if avatar_gender_type_obj and avatar_gender_type_obj.photo else make_avatar_total_images(list_data))
            code = 200
        else:
            text = "{}".format(_("You have no any avatar"))
            code = 201
        # except:
        #     text = "{}".format(_("You have no permission for this action"))
        data['result_image'] = result_image
        data['code'] = code
        data['text'] = text
        return Response(data=data)

class UserAvatarList(APIView):
    def post(self,request,c_id):
        text = ''
        code = 403
        data = {'list':[],'html_list':''}
        # try:
        data_list = UserAvatar.objects.filter(user__m_id=c_id).order_by('status')
        html_list = ''
        list_data = []
        if data_list:
            for item in data_list:
                list_data.append((item.avatar.title,str(item.avatar.photo)))
                # html_list = "{}<div><img style='width:150px;' src='{}{}'></div>".format(html_list,domain_name,item.avatar.photo.url)

            data['list'] = data_list.values_list('avatar__title',flat=True)
            data['html_list'] = "{}{}".format(domain_name,make_avatar_total_images(list_data))
            code = 200

        else:
            text = "{}".format(_("You have no any avatar"))
            code = 201
        # except:
        #     text = "{}".format(_("You have no permission for this action"))
        data['code'] = code
        data['text'] = text
        return Response(data=data)

class GetFreeAvatarList(APIView):


    def get(self,request):
        text = ''
        code = 403
        data = {'list':[]}
        try:
            data_list = Avatar.objects.filter(is_active=True,is_deleted=False)
            if data_list:
                data['list'] = data_list.values('id', 'title')
                text = "{}".format(_("Avatars"))
                code = 200
            else:
                text = "{}".format(_("There is not any avatar"))
                code = 201
        except:
            text = "{}".format(_("You have no permission for this action"))
        data['code'] = code
        data['text'] = text
        return Response(data=data)



class GetFreeAvatarListTitle(APIView):


    def get(self,request):
        text = ''
        code = 403
        data = {'list':[]}
        try:
            data_list = Avatar.objects.filter(is_active=True,is_deleted=False)
            if data_list:
                data['list'] = data_list.values_list('title',flat=True)
                text = "{}".format(_("Employees"))
                code = 200
            else:
                text = "{}".format(_("There is not any avatar"))
                code = 201
        except:
            text = "{}".format(_("You have no permission for this action"))
        data['code'] = code
        data['text'] = text
        return Response(data=data)


class GetList(APIView):
    def get(self,request):
        data = {'list':[]}
        data['list'] = GUser.objects.filter().values('id', 'username')
        return Response(data)

    def post(self,request):
        pass


class GetEmployeeListText(APIView):

    def get(self,request,m_id,username):
        text = ''
        code = 403
        data = {'list':[]}
        try:
            user = GUser.objects.get(username=username)
            user.m_id = m_id

            if request.GET.get('first_name', ''):
                user.first_name = request.GET.get('first_name', '')
            if request.GET.get('last_name', ''):
                user.last_name = request.GET.get('last_name', '')
            user.save()
            for_i = 0
            users_list = GUser.objects.filter(usertype=2)
            if users_list:
                data['list'] = users_list.values('id', 'username')
                text = "{}".format(_("Employees"))
                code = 200
            else:
                text = "{}".format(_("There is not any user"))
                code = 201
        except:
            text = "{}".format(_("You have no permission for this action"))
        data['code'] = code
        data['text'] = text
        return Response(data=data)


class GetEmployeeListMarkText(APIView):

    def get(self,request,m_id,username):
        text = ''
        code = 403
        data = {}
        try:
            user = GUser.objects.get(username=username)
            user.m_id = m_id

            if request.GET.get('first_name', ''):
                user.first_name = request.GET.get('first_name', '')
            if request.GET.get('last_name', ''):
                user.last_name = request.GET.get('last_name', '')
            user.save()
            for_i = 0
            users_list = GUser.objects.filter(usertype=2)
            if users_list:
                for item in users_list:
                    for_i += 1
                    text = "\n{}{}".format(text,
                                         "{}. @{}  -  {}  point: {}".format(for_i, item.username,item.get_full_name(),item.point))
                text = "{}{}".format(_("All employees"),text)
                code = 200
            else:
                text = "{}".format(_("There is not any user"))
                code = 201
        except:
            text = "{}".format(_("You have no permission for this action"))
        data['code'] = code
        data['message'] = text
        return Response(data=data)

    # def post(self,request):
    #     pass



class DeleteMessages(APIView):
    def get(self,request,c_id,l_m_id):
        code = 0
        try:
            from bot_app.tasks import delete_messages
            delete_messages.delay(c_id=c_id,l_m_id=l_m_id)
            code = 1
        except:
            code = 0
        data = {'code':code}
        return Response(data)


from .tasks import delete_messages, make_avatar


class OnlyDeleteMessages(APIView):
    def get(self,request,c_id,l_m_id):
        code = 0
        try:
            from bot_app.tasks import only_delete_messages
            only_delete_messages.delay(c_id=c_id,l_m_id=l_m_id)
            code = 1
        except:
            code = 0
        data = {'code':code}
        return Response(data)
    def post(self,request,c_id,l_m_id):
        try:
            from bot_app.tasks import one_delete_messages
            one_delete_messages.delay(c_id=c_id,l_m_id=l_m_id)
            code = 1
        except:
            code = 0
        data = {'code':code}
        return Response(data)


class GetStarted(APIView):
    def get(self,request,c_id):
        game_status = 0
        user_main_message = ''
        try:
            user = GUser.objects.get(m_id=c_id)
            text = _('Welcomeback, {}. You can start to play the game.'.format(user.get_full_name()))
            code = 1
            user_main_message = user.main_message_id
            game_status = user.game_status
        except:
            code = 0
            text = _('Welcome, please authorize')
        data = {'message':  text,'code':code,'game_status':game_status,'user_main_message':user_main_message}
        return Response(data)



class UserMainMessage(APIView):
    def post(self,request,c_id):
        game_status = 0
        user_main_message = request.POST.get("user_main_message")
        try:
            user = GUser.objects.get(m_id=c_id)
            code = 1
            game_status = user.game_status
            user.main_message_id = user_main_message
            user.save()
        except:
            code = 0
        data = {'code':code,'game_status':game_status}
        return Response(data)

    def get(self,request,c_id):
        game_status = 0
        user_main_message = ''
        try:
            user = GUser.objects.get(m_id=c_id)
            code = 1
            user_main_message = user.main_message_id
            game_status = user.game_status
        except:
            code = 0
            text = _('Welcome, please authorize')
        data = {'code':code,'game_status':game_status,'user_main_message':user_main_message}
        return Response(data)



@api_view(['GET', 'POST'])
def hello_world(request,c_id):
    user_avatar_url = ""
    hello = ""
    try:
        user_obj = GUser.objects.filter(m_id=c_id).get()
        try:
            user_avatar_url = "{}".format(user_obj.avatar_result_photo_url)
            code = 1
        except Exception as er:
            code = 2
    except:
        code = 0
    data = {'useravatarurl':user_avatar_url,'code':code}
    return Response(data)

class ShowFightMenu(APIView):
    def post(self,request,c_id):
        user_avatar_url = ""
        try:
            user_obj = GUser.objects.filter(m_id=c_id).get()
            try:
                user_avatar_url = "{}".format(user_obj.avatar_result_photo_url)
                code = 1
            except Exception as er:
                code = 2
        except:
            code = 0
        if user_avatar_url:
            user_avatar_url = "http://game.minbashi.com/media/{}".format(user_avatar_url)
        data = {'useravatarurl':user_avatar_url,'code':code}
        return Response(data)


class GenerateGame(APIView):
    def post(self,request,c_id,type):
        private_number = request.POST.get('private_number',None)
        game_code = 'send-main'
        messages_dict = {
            "user1":'',
            "user2":'',
        }
        try:
            text = ''
            url = ''
            code = 0
            user = GUser.objects.get(m_id=c_id)
            if user.get_total_point() > 0 :
                text = "You can not create a game because first of all you have to set your strategy"
                code = 2
            elif user.game_status == 3 :
                text = "You can not create a game becuase you are in a game now"
                code = 2
            elif user.game_status == 1 or user.game_status == 2 :
                text = "You can not create a game becuase you have waiting a game"
                code = 2
            elif user.game_status == 0:
                if (type == GameTypeChoices[0][0]) or type == GameTypeChoices[1][0] and private_number:

                    any_public_game = Game.objects.filter(status=GameStatusChoices[0][0]).exclude(user1=user).first()
                    if type == GameTypeChoices[0][0] and any_public_game:
                        gm = GameModel(chat_id=user.m_id)
                        text = gm.game_acception(g_id=any_public_game.u_id,choice=GameStatusChoices[1][0],message_id=0)
                        code = 11
                    else:

                        user2 = GUser.objects.filter(phone=str(private_number).replace("+","")).first()
                        if type == GameTypeChoices[1][0] and private_number and user2 and user2.game_status != USERGAMESTATUS[0][0]:
                            code = 5
                            text = "Your friend is not available now"
                        else:
                            game_obj = Game(user1=user,status=GameStatusChoices[0][0],type=type)
                            if type == GameTypeChoices[1][0] and private_number:

                                game_obj.private_number = private_number
                                user2 = GUser.objects.filter(phone=private_number).first()
                                if user2 and user2.game_status == USERGAMESTATUS[0][0]:
                                    game_obj.user2 = user2
                            game_obj.save()
                            if type == GameTypeChoices[1][0] and private_number:
                                try:
                                    all_user_status = AllUserStatus.objects.filter(chat_id=c_id).get()
                                except:
                                    all_user_status = AllUserStatus(chat_id=c_id).save()
                                all_user_status.referal_game = game_obj.u_id
                                all_user_status.save()
                            text = "You have created the game successfully."
                            game_code = 'send-game-waiting-menu'
                            if type == GameTypeChoices[0][0]:
                                user.game_status = USERGAMESTATUS[2][0]
                                user.save()
                                code = 1
                            elif type == GameTypeChoices[1][0]:
                                if game_obj.user2:
                                    code = 1
                                else:
                                    link_text = "To start to play the game"
                                    text = "{} -> <a href='{}'>{}</a>".format(link_text, 'https://t.me/{}?start={}'.format(TELEGRAM_BOT_NAME,game_obj.u_id),'Click here')
                                    game_code = "send-friend-text"
                                user.game_status = USERGAMESTATUS[1][0]
                                user.save()
                                code = 10
                elif type == GameTypeChoices[1][0] and private_number is None:
                    text = "Please insert correct data"
                    code =  2
                else:
                    text = "Try again"
        except Exception as e:
            code = 0
            text = _('Something happened, try again - {}'.format(e))
        if type == GameTypeChoices[1][0] and private_number:
            rsm = RegisterStepModel(chat_id=c_id)
            rsm.set_status(ns_status='-', extra_value=None)
        data = {'message':  text,'code':code,'game_code':game_code}
        return Response(data)



class CheckPrivateGame(APIView):
    def post(self,request,c_id):
        text = ''
        code = 0
        g_u_id = ""
        try:
            user = GUser.objects.get(m_id=str(c_id))
            game_obj = Game.objects.filter(user2=user).filter(status=GameStatusChoices[0][0]).order_by("-date").first()
            if game_obj:
                g_u_id = game_obj.u_id
                code = 1
        except:
            g_u_id = ""
        data = {'code': code,'g_u_id': g_u_id}
        return Response(data)



class StartTheGame(APIView):
    def post(self,request,c_id,g_u_id):
        text = ''
        code = 0
        choice = request.POST.get('choice')
        # try:
        user = GUser.objects.get(m_id=str(c_id))
        if user.get_total_point() > 0 :
            text = 'You can not play the game because first of all you have to set your strategy'
        elif user.game_status == 0 :
            game_obj = Game.objects.exclude(user1=user).filter(status=GameStatusChoices[0][0],u_id=g_u_id).get()
            user1 = copy.deepcopy(game_obj.user1)
            if choice == GameStatusChoices[3][0]:
                game_obj.status = GameStatusChoices[3][0]
                game_obj.user1.game_status = USERGAMESTATUS[3][0]
                game_obj.user1.save()
                game_obj.user2 = user
                # photo_url = make_game_image(
                #     str(game_obj.user1.avatar_result_photo) if game_obj.user1 and game_obj.user1.avatar_result_photo else '',
                #     str(user.avatar_result_photo) if user.avatar_result_photo else '',
                #     'static/versus-back.png')
                # if photo_url:
                #     game_obj.game_result_photo.name = photo_url
                # game_obj.save()
                # game_obj.user_message1 = message_ids[0]
                # game_obj.user_message2 = message_ids[1]
                user.game_status = USERGAMESTATUS[3][0]
                user.save()
                game_obj.save()
                gm = GameModel(chat_id=game_obj.user1.m_id)
                gm.get_game_status_send_all_user(game_obj.user1.m_id,game_obj.user2.m_id,game_obj.pk,)

                chat1_id = user.m_id
                chat2_id = game_obj.user2.m_id
                message1_id = game_obj.user1.main_message_id if game_obj.user1.main_message_id else ''
                message2_id = user.main_message_id if user.main_message_id else ''

                try:
                    _thread.start_new_thread(delete_message_one_message, (chat1_id, message1_id))
                    _thread.start_new_thread(delete_message_one_message, (chat2_id, message2_id))
                except:
                    pass
                # game_obj.user_message1 = message_ids[0]
                # game_obj.user_message2 = message_ids[1]
                # game_obj.save()
                code = 1
            else:
                if game_obj.type == GameTypeChoices[1][0] and (game_obj.private_number == user.phone or game_obj.user2 == user):
                    game_obj.user1.game_status = USERGAMESTATUS[0][0]
                    game_obj.user1.save()
                    game_obj.status = GameStatusChoices[4][0]
                    game_obj.save()
                    # game_obj = Game.objects.exclude(user1=user).filter(status=GameStatusChoices[0][0],u_id=g_u_id).get()
                    gm = GameModel(chat_id=game_obj.user1.m_id)
                    try:
                        gm.send_fight_menu_type_with_text(menu_type=GameMenuControllerList[0], extra_text="{} has rejected to play with you".format(game_obj.user1.get_full_name()))
                    except:
                        pass
                    code = 2
                    text = "You have canceled"
        else:
            text = 'You can not play the game'
        # except Exception as e:
        #     text = "{}".format("The game does not exist")
        data = {'message': "{}".format(text),'code': code}
        return Response(data)

    def get(self,request,c_id,g_u_id):
        text = ''
        image_url = ''
        code = 0
        g_id = 0
        try:
            user = GUser.objects.get(m_id=c_id)
            if user.game_status == 0 :
                if g_u_id != '-':
                    game_obj = Game.objects.exclude(user1=user).filter(status=GameStatusChoices[0][0],u_id=g_u_id).get()
                    if game_obj.type == GameTypeChoices[1][0] and str(game_obj.private_number).replace('+','') != str(user.phone).replace('+',''):
                        code = 2
                        text = "You can't play the game"
                    else:
                        text = "Start the game"
                        image_url = "{}{}".format(domain_name,game_obj.game_result_photo.url )  if game_obj.game_result_photo else ''
                        code = 1
                        g_id = game_obj.id
                else:
                    code = 3
            else:

                code = 2
                text = "You can't play the game"
        except Exception as e:
            text = "The game is not available for you "
            try:
                user = AllUserStatus.objects.get(chat_id=c_id)
            except:
                user = AllUserStatus(chat_id=c_id).save()
            user.referal_game = '-'
            user.save()
        data = {'message': text, 'image_url':image_url, 'code': code, 'g_id': g_id}
        return Response(data)


class FinishTheGame(APIView):
    def post(self,request,g_u_id):
        text = ''
        code = 0
        try:
            g_obj = Game.objects.filter(status=GameStatusChoices[0][0],u_id=g_u_id).get()
            user1_point = g_obj.get_points_lte(g_obj.user1_id)
            user2_point = g_obj.get_points_lte(g_obj.user2_id)
            g_obj.status = GameStatusChoices[2][0]
            g_obj.user1.game_status = USERGAMESTATUS[0][0]
            g_obj.user2.game_status = USERGAMESTATUS[0][0]
            g_obj.user1.save()
            g_obj.user2.save()
            if user1_point > user2_point:
                g_obj.win_user = g_obj.user1
            elif user1_point < user2_point:
                g_obj.win_user = g_obj.user2
            g_obj.win_user = g_obj.user2
        except Exception as e:
            text = "{}".format(str(e))
        data = {'message': "{}".format(text),'code': code}
        return Response(data)


class FinishStartedTheGame(APIView):
    def post(self,request,c_id):
        text = ''
        code = 0
        game_id = 0
        try:
            user = GUser.objects.get(m_id=c_id)
            g_obj = Game.objects.filter(status=GameStatusChoices[1][0]).filter(Q(user1=user) | Q(user2=user)).order_by('-date').first()
            game_id = copy.deepcopy(g_obj.id)
            user1_point = g_obj.get_points_lte(g_obj.user1_id)
            user2_point = g_obj.get_points_lte(g_obj.user2_id)
            g_obj.status = GameStatusChoices[4][0]
            g_obj.user1.game_status = USERGAMESTATUS[0][0]
            g_obj.user2.game_status = USERGAMESTATUS[0][0]
            g_obj.user1.save()
            g_obj.user2.save()
            if g_obj.user1 == user:
                g_obj.win_user = g_obj.user2
            else:
                g_obj.win_user = g_obj.user1
            g_obj.save()
            code = 1
        except Exception as e:
            text = "{}".format(str(e))
        data = {'message': "{}".format(text),'code': code,'game_id': game_id}
        return Response(data)


class DeleteAllGames(APIView):
    def post(self,request,c_id):
        text = ''
        code = 0
        try:
            from bot_app.tasks import cancel_allgame
            user = GUser.objects.get(m_id=c_id)
            user.game_status = USERGAMESTATUS[0][0]
            user.save()
            cancel_allgame.delay(c_id)
            code = 1
            text = "You have done successfully"
        except:
            text = "Something happend"
        data = {'message': text,'code': code}
        return Response(data)


class GameStatus(APIView):
    def post(self,request,c_id):
        text = ''
        user_text = ''
        image_url = ''
        code = 0
        atack_list = []
        deffence_list = []
        markup_result = {
            'atack':[],
            'deffence':[],
            'ready': False,
        }
        try:
            user = GUser.objects.get(m_id=c_id)
            if user.game_status == 3 :
                last_game = Game.objects.by_user(user).filter(status=GameStatusChoices[1][0]).order_by('-date').first()
                if last_game:
                    if last_game.user1 == user:
                        user_point = last_game.get_points(last_game.user1_id)
                        user_text = "{}\n{}".format("<b>Round:</b> {}".format(last_game.round+1),
                                                   "<b>{}:</b> {}".format(last_game.user1.get_full_name(),"|"*user_point) if last_game.user1 else '',
                                                   # "<b>{}:</b> {}".format(last_game.user2.get_full_name(),user2_point) if last_game.user2 else ''
                                                         )
                        image_url = "http://game.minbashi.com/media/{}".format(last_game.user1.avatar_result_photo_url) if last_game.user1.avatar_result_photo_url else ""
                    else:
                        user_point = last_game.get_points(last_game.user2_id)
                        user_text = "{}\n{}".format("<b>Round:</b> {}".format(last_game.round+1),
                                               # "<b>{}:</b> {}".format(last_game.user1.get_full_name(),user1_point) if last_game.user1 else '',
                                               "<b>{}:</b> {}".format(last_game.user2.get_full_name(),"|"*user_point) if last_game.user2 else ''
                                                     )
                        image_url = "http://game.minbashi.com/media/{}".format(last_game.user2.avatar_result_photo_url) if last_game.user2.avatar_result_photo_url else ""

                    # text = "{}\n{}\n{}".format("<b>Round:</b> {}".format(last_game.round),"<b>{}:</b> {}".format(last_game.user1.get_full_name(),user1_point),"<b>{}:</b> {}".format(last_game.user2.get_full_name(),user2_point))
                    # image_url = "{}{}".format(domain_name,last_game.game_result_photo.url if last_game.game_result_photo else '')
                    # image1_url = "http://game.minbashi.com/media/{}".format(last_game.user1.avatar_result_photo_url) if last_game.user1.avatar_result_photo_url else ""
                    # image2_url = "http://game.minbashi.com/media/{}".format(last_game.user2.avatar_result_photo_url) if last_game.user2.avatar_result_photo_url else ""
                    # image_url = "http://game.minbashi.com/media/photos/user-avatar/all/9cf6e8ead91b4a8090f84cc627caae46.png"
                    # image_url = "http://game.minbashi.com/media/photos/user-avatar/all/9cf6e8ead91b4a8090f84cc627caae46.png"
                    code = 1
                    try:
                        ga_obj, created = GameAction.objects.get(user=user, game=last_game, round=last_game.round)

                        if ga_obj.attack:
                            atack_list.append(ga_obj.attack)
                        if ga_obj.attack2:
                            atack_list.append(ga_obj.attack2)
                        if ga_obj.deffence:
                            deffence_list.append(ga_obj.deffence)
                        if ga_obj.deffence2:
                            deffence_list.append(ga_obj.deffence2)
                        markup_result = {
                            'atack': atack_list,
                            'deffence': atack_list,
                            'ready': ga_obj.ready,
                        }
                    except:
                        pass
            if user.game_status == 1 or user.game_status == 2 :
                user1_text = "You have to wait to strart to play the game"
                text = "You have to wait to strart to play the game"
                code = 2
            if user.game_status == 0:
                user1_text = "You have not any waiting game"
                text = "You have not any waiting game"
                code = 3
        except:
            code = 0
            text = _('Something happened')
        data = {'message': text,'image2_url': image_url,'user_text': user_text,'image_url': image_url,'code': code,'markup_result':markup_result}
        return Response(data)
    def get(self,request,c_id):
        text = ''
        user1_text = ''
        user2_text = ''
        image_url = ''
        image1_url = ''
        image2_url = ''
        code = 0
        markup_result = {
            'atack':'',
            'deffence':'',
            'ready': False,
        }
        try:
            user = GUser.objects.get(m_id=c_id)
            if user.game_status == 3 :
                last_game = Game.objects.by_user(user).filter(status=GameStatusChoices[1][0]).order_by('-date').first()
                if last_game:
                    user1_point = last_game.get_points(last_game.user1_id)
                    user2_point = last_game.get_points(last_game.user2_id)
                    user1_text = "{}\n{}".format("<b>Round:</b> {}".format(last_game.round+1),
                                               "<b>{}:</b> {}".format(last_game.user1.get_full_name(),"|"*user1_point) if last_game.user1 else '',
                                               # "<b>{}:</b> {}".format(last_game.user2.get_full_name(),user2_point) if last_game.user2 else ''
                                                     )
                    user2_text = "{}\n{}".format("<b>Round:</b> {}".format(last_game.round+1),
                                           # "<b>{}:</b> {}".format(last_game.user1.get_full_name(),user1_point) if last_game.user1 else '',
                                           "<b>{}:</b> {}".format(last_game.user2.get_full_name(),"|"*user2_point) if last_game.user2 else ''
                                                 )
                    # text = "{}\n{}\n{}".format("<b>Round:</b> {}".format(last_game.round),"<b>{}:</b> {}".format(last_game.user1.get_full_name(),user1_point),"<b>{}:</b> {}".format(last_game.user2.get_full_name(),user2_point))
                    # image_url = "{}{}".format(domain_name,last_game.game_result_photo.url if last_game.game_result_photo else '')
                    # image1_url = "http://game.minbashi.com/media/{}".format(last_game.user1.avatar_result_photo_url) if last_game.user1.avatar_result_photo_url else ""
                    # image2_url = "http://game.minbashi.com/media/{}".format(last_game.user2.avatar_result_photo_url) if last_game.user2.avatar_result_photo_url else ""
                    image1_url = "http://game.minbashi.com/media/photos/user-avatar/all/9cf6e8ead91b4a8090f84cc627caae46.png"
                    image2_url = "http://game.minbashi.com/media/photos/user-avatar/all/9cf6e8ead91b4a8090f84cc627caae46.png"
                    code = 1
                    try:
                        ga_obj, created = GameAction.objects.get(user=user, game=last_game, round=last_game.round)
                        markup_result = {
                            'atack': ga_obj.attack,
                            'deffence': ga_obj.deffence,
                            'ready': ga_obj.ready,
                        }
                    except:
                        pass
            if user.game_status == 1 or user.game_status == 2 :
                user1_text = "You have to wait to strart to play the game"
                text = "You have to wait to strart to play the game"
                code = 2
            if user.game_status == 0:
                user1_text = "You have not any waiting game"
                text = "You have not any waiting game"
                code = 3
        except:
            code = 0
            text = _('Something happened')
        data = {'message': text,'image1_url': image1_url,'image2_url': image2_url,'user1_text': user1_text,'user2_text': user2_text,'image_url': image_url,'code': code,'markup_result':markup_result}
        return Response(data)

class GameStatusWithId(APIView):
    def post(self,request,c_id,g_id):
        text = ''
        image_url = ''
        code = 0
        message_id = 0
        markup_result = {
            'atack':'',
            'deffence':'',
            'ready': False,
        }
        try:
            user = GUser.objects.get(m_id=c_id)

            if user.game_status == 3 :
                last_game = Game.objects.by_user(user).filter(status=GameStatusChoices[1][0],id=g_id).get()
                if last_game.user1 == user:
                    message_id = last_game.user_message1
                else:
                    message_id = last_game.user_message2
                if last_game:
                    text = "{}\n{}\n{}".format("<b>Round:</b> {}".format(last_game.round),"<b>{}:</b> {}".format(last_game.user1.get_full_name(),last_game.get_points(last_game.user1_id)),"<b>{}:</b> {}".format(last_game.user2.get_full_name(),last_game.get_points(last_game.user2_id)))
                    image_url = "{}{}".format(domain_name,last_game.game_result_photo.url if last_game.game_result_photo else '')
                    code = 1
                    try:
                        ga_obj, created = GameAction.objects.get(user=user, game=last_game, round=last_game.round)
                        markup_result = {
                            'atack': ga_obj.attack,
                            'deffence': ga_obj.deffence,
                            'ready': ga_obj.ready,
                        }
                    except:
                        pass
            if user.game_status == 1 or user.game_status == 2 :
                text = "You have to wait to strart to play the game"
                code = 2
            elif user.game_status == 0:
                text = "You have not any waiting game"
                code = 3
        except:
            code = 0
            text = _('Something happened')
        data = {'message': text,'image_url': image_url,'code': code,'markup_result':markup_result, 'message_id':message_id}
        return Response(data)
    def get(self,request,c_id,g_id):
        text = ''
        image_url = ''
        code = 0
        message_id = 0
        markup_result = {
            'atack':'',
            'deffence':'',
            'ready': False,
        }
        try:
            user = GUser.objects.get(m_id=c_id)
            if user.game_status == 3 :
                last_game = Game.objects.by_user(user).filter(status=GameStatusChoices[1][0],id=g_id).get()
                if last_game.user1 == user:
                    message_id = last_game.user_message1
                else:
                    message_id = last_game.user_message2
                if last_game:
                    text = "{}\n{}\n{}".format("<b>Round:</b> {}".format(last_game.round),"<b>{}:</b> {}".format(last_game.user1.get_full_name(),last_game.get_points(last_game.user1_id)),"<b>{}:</b> {}".format(last_game.user2.get_full_name(),last_game.get_points(last_game.user2_id)))
                    image_url = "{}{}".format(domain_name,last_game.game_result_photo.url if last_game.game_result_photo else '')
                    code = 1
                    try:
                        ga_obj, created = GameAction.objects.get(user=user, game=last_game, round=last_game.round)
                        markup_result = {
                            'atack': ga_obj.attack,
                            'deffence': ga_obj.deffence,
                            'ready': ga_obj.ready,
                        }
                    except:
                        pass
            if user.game_status == 1 or user.game_status == 2 :
                text = "You have to wait to strart to play the game"
                code = 2
            elif user.game_status == 0:
                text = "You have not any waiting game"
                code = 3
        except:
            code = 0
            text = _('Something happened')
        data = {'message': text,'image_url': image_url,'code': code,'markup_result':markup_result, 'message_id':message_id}
        return Response(data)


class GameStatusWithGeneral(APIView):
    def post(self,request,c_id,g_id):
        text = ''
        image_url = ''
        code = 0
        message_id = 0
        game_continue = True
        markup_result = {
            'atack':'',
            'deffence':'',
            'ready': False,
        }
        try:
            user = GUser.objects.get(m_id=c_id)

            last_game = Game.objects.filter(id=g_id).get()
            if last_game.status != GameStatusChoices[1][0]:
                game_continue = False
            if last_game.user1 == user:
                message_id = last_game.user_message1
            else:
                message_id = last_game.user_message2
            if last_game:
                text = "{}\n{}\n{}".format("<b>Round:</b> {}".format(last_game.round),"<b>{}:</b> {}".format(last_game.user1.get_full_name(),last_game.get_points(last_game.user1_id)),"<b>{}:</b> {}".format(last_game.user2.get_full_name(),last_game.get_points(last_game.user2_id)))
                image_url = "{}{}".format(domain_name,last_game.game_result_photo.url if last_game.game_result_photo else '')
                code = 1
        except:
            code = 0
            text = _('Something happened')
        data = {'message': text,'image_url': image_url,'code': code,'markup_result':markup_result, 'message_id':message_id,'game_continue':game_continue}
        return Response(data)
    def get(self,request,c_id,g_id):
        text = ''
        image_url = ''
        code = 0
        message_id = 0
        game_continue = True
        markup_result = {
            'atack':'',
            'deffence':'',
            'ready': False,
        }
        try:
            user = GUser.objects.get(m_id=c_id)

            last_game = Game.objects.filter(id=g_id).get()
            if last_game.status != GameStatusChoices[1][0]:
                game_continue = False
            if last_game.user1 == user:
                message_id = last_game.user_message1
            else:
                message_id = last_game.user_message2
            if last_game:
                text = "{}\n{}\n{}".format("<b>Round:</b> {}".format(last_game.round),"<b>{}:</b> {}".format(last_game.user1.get_full_name(),last_game.get_points(last_game.user1_id)),"<b>{}:</b> {}".format(last_game.user2.get_full_name(),last_game.get_points(last_game.user2_id)))
                image_url = "{}{}".format(domain_name,last_game.game_result_photo.url if last_game.game_result_photo else '')
                code = 1
        except:
            code = 0
            text = _('Something happened')
        data = {'message': text,'image_url': image_url,'code': code,'markup_result':markup_result, 'message_id':message_id,'game_continue':game_continue}
        return Response(data)


class GameStatusWithGeneralOnly(APIView):
    def post(self,request,g_id):
        text = ''
        user1_text = ''
        user2_text = ''
        image_url = ''
        image1_url = ''
        image2_url = ''
        code = 0
        user1_point = TOTALUSERGAMESTARTPOINT
        user2_point = TOTALUSERGAMESTARTPOINT
        game_continue = True
        game_finished = False
        markup_result = {
            'atack':'',
            'deffence':'',
            'ready': False,
            'message1_id':0,
            'message2_id':0
        }
        # try:
        last_game = Game.objects.filter(id=g_id).get()
        markup_result['user1_id'] = last_game.user1.m_id if last_game.user1 else 0
        markup_result['user2_id'] = last_game.user2.m_id if last_game.user2 else 0
        if last_game.status != GameStatusChoices[1][0]:
            game_continue = False
        if last_game:
            user1_point = last_game.get_points_lte(last_game.user1_id)
            user2_point = last_game.get_points_lte(last_game.user2_id)

            if user1_point <= 0 or user2_point <= 0:
                game_finished = True
                if user1_point == user2_point:
                   game1_result = "Draw"
                   game2_result = "Draw"
                else:
                    if user1_point > user2_point:
                        game1_result = "{}, congratulations. You won the game".format(last_game.user1.get_full_name())
                        game2_result = "{}, sorry. You lost the game".format(last_game.user2.get_full_name())
                    else:
                        game1_result = "{}, sorry. You lost the game".format(last_game.user1.get_full_name())
                        game2_result = "{}, congratulations. You won the game".format(last_game.user2.get_full_name())
                user1_text = "{}\n{}\n{}\n{}".format("<b>{}</b>".format('Game Finished',),
                                   game1_result,
                                   "<b>Round:</b> {} ".format(last_game.round+1),
                                   "<b>{}:</b> {}".format(last_game.user1.get_full_name(),"|"*user1_point) if last_game.user1 else '',
                                           )
                user2_text = "{}\n{}\n{}\n{}".format("<b>{}</b>".format('Game Finished',),
                                   game2_result,
                                   "<b>Round:</b> {} ".format(last_game.round+1),
                                   "<b>{}:</b> {}".format(last_game.user2.get_full_name(),"|"*user2_point) if last_game.user2 else ''
                                           )
            else:
                user1_text = "{}\n{}".format("<b>Round:</b> {}".format(last_game.round+1),
                                           "<b>{}:</b> {}".format(last_game.user1.get_full_name(),"|"*user1_point) if last_game.user1 else '',
                                           # "<b>{}:</b> {}".format(last_game.user2.get_full_name(),user2_point) if last_game.user2 else ''
                                                 )
                user2_text = "{}\n{}".format("<b>Round:</b> {}".format(last_game.round+1),
                                           # "<b>{}:</b> {}".format(last_game.user1.get_full_name(),user1_point) if last_game.user1 else '',
                                           "<b>{}:</b> {}".format(last_game.user2.get_full_name(),"|"*user2_point) if last_game.user2 else ''
                                                 )
            # image_url = "{}{}".format(domain_name,last_game.game_result_photo.url if last_game.game_result_photo else '')
            # image1_url = "http://game.minbashi.com/media/{}".format(last_game.user1.avatar_result_photo_url) if last_game.user1.avatar_result_photo_url else ""
            # image2_url = "http://game.minbashi.com/media/{}".format(last_game.user2.avatar_result_photo_url) if last_game.user2.avatar_result_photo_url else ""
            # image1_url = "{}{}".format(domain_name,last_game.game_result_photo.url if last_game.game_result_photo else '')
            image1_url = "http://game.minbashi.com/media/photos/user-avatar/all/9cf6e8ead91b4a8090f84cc627caae46.png"
            image2_url = "http://game.minbashi.com/media/photos/user-avatar/all/9cf6e8ead91b4a8090f84cc627caae46.png"
            code = 1

            markup_result['message1_id'] = last_game.user_message1 if last_game.user_message1 else 0
            markup_result['message2_id'] = last_game.user_message2 if last_game.user_message2 else 0
    # except:
    #     code = 0
    #     text = _('Something happened')
        data = {'message': text,'image1_url': image1_url,'image2_url': image2_url,'user1_text': user1_text,'user2_text': user2_text,'image_url': image_url,'code': code,'markup_result':markup_result,'game_continue':game_continue,'game_finished':game_finished}
        return Response(data)
    def get(self,request,g_id):
        text = ''
        user1_text = ''
        user2_text = ''
        image_url = ''
        image1_url = ''
        image2_url = ''
        code = 0
        user1_point = TOTALUSERGAMESTARTPOINT
        user2_point = TOTALUSERGAMESTARTPOINT
        game_continue = True
        game_finished = False
        markup_result = {
            'atack':'',
            'deffence':'',
            'ready': False,
            'message1_id':0,
            'message2_id':0
        }
        # try:
        last_game = Game.objects.filter(id=g_id).get()
        markup_result['user1_id'] = last_game.user1.m_id if last_game.user1 else 0
        markup_result['user2_id'] = last_game.user2.m_id if last_game.user2 else 0
        if last_game.status != GameStatusChoices[1][0]:
            game_continue = False
        if last_game:
            user1_point = last_game.get_points_lte(last_game.user1_id)
            user2_point = last_game.get_points_lte(last_game.user2_id)

            if user1_point <= 0 or user2_point <= 0:
                game_finished = True
                if user1_point == user2_point:
                   game1_result = "Draw"
                   game2_result = "Draw"
                else:
                    if user1_point > user2_point:
                        game1_result = "{}, congratulations. You won the game".format(last_game.user1.get_full_name())
                        game2_result = "{}, sorry. You lost the game".format(last_game.user2.get_full_name())
                    else:
                        game1_result = "{}, sorry. You lost the game".format(last_game.user1.get_full_name())
                        game2_result = "{}, congratulations. You won the game".format(last_game.user2.get_full_name())
                user1_text = "{}\n{}\n{}\n{}".format("<b>{}</b>".format('Game Finished',),
                                   game1_result,
                                   "<b>Round:</b> {} ".format(last_game.round+1),
                                   "<b>{}:</b> {}".format(last_game.user1.get_full_name(),"|"*user1_point) if last_game.user1 else '',
                                           )
                user2_text = "{}\n{}\n{}\n{}".format("<b>{}</b>".format('Game Finished',),
                                   game2_result,
                                   "<b>Round:</b> {} ".format(last_game.round+1),
                                   "<b>{}:</b> {}".format(last_game.user2.get_full_name(),"|"*user2_point) if last_game.user2 else ''
                                           )
            else:
                user1_text = "{}\n{}".format("<b>Round:</b> {}".format(last_game.round+1),
                                           "<b>{}:</b> {}".format(last_game.user1.get_full_name(),"|"*user1_point) if last_game.user1 else '',
                                           # "<b>{}:</b> {}".format(last_game.user2.get_full_name(),user2_point) if last_game.user2 else ''
                                                 )
                user2_text = "{}\n{}".format("<b>Round:</b> {}".format(last_game.round+1),
                                           # "<b>{}:</b> {}".format(last_game.user1.get_full_name(),user1_point) if last_game.user1 else '',
                                           "<b>{}:</b> {}".format(last_game.user2.get_full_name(),"|"*user2_point) if last_game.user2 else ''
                                                 )
            # image_url = "{}{}".format(domain_name,last_game.game_result_photo.url if last_game.game_result_photo else '')
            # image1_url = "http://game.minbashi.com/media/{}".format(last_game.user1.avatar_result_photo_url) if last_game.user1.avatar_result_photo_url else ""
            # image2_url = "http://game.minbashi.com/media/{}".format(last_game.user2.avatar_result_photo_url) if last_game.user2.avatar_result_photo_url else ""
            image1_url = "http://game.minbashi.com/media/photos/user-avatar/all/9cf6e8ead91b4a8090f84cc627caae46.png"
            image2_url = "http://game.minbashi.com/media/photos/user-avatar/all/9cf6e8ead91b4a8090f84cc627caae46.png"
            # image1_url = "{}{}".format(domain_name,last_game.game_result_photo.url if last_game.game_result_photo else '')
            code = 1

            markup_result['message1_id'] = last_game.user_message1 if last_game.user_message1 else 0
            markup_result['message2_id'] = last_game.user_message2 if last_game.user_message2 else 0
    # except:
    #     code = 0
    #     text = _('Something happened')
        data = {'message': text,'image1_url': image1_url,'image2_url': image2_url,'user1_text': user1_text,'user2_text': user2_text,'image_url': image_url,'code': code,'markup_result':markup_result,'game_continue':game_continue,'game_finished':game_finished}
        return Response(data)


class GameStatusForCancelFinished(APIView):
    def post(self,request,g_id):
        text = ''
        user1_text = ''
        user2_text = ''
        image_url = ''
        image1_url = ''
        image2_url = ''
        code = 0
        user1_point = TOTALUSERGAMESTARTPOINT
        user2_point = TOTALUSERGAMESTARTPOINT
        game_continue = True
        game_finished = False
        markup_result = {
            'atack':'',
            'deffence':'',
            'ready': False,
            'message1_id':0,
            'message2_id':0
        }
        # try:
        last_game = Game.objects.filter(id=g_id).get()
        markup_result['user1_id'] = last_game.user1.m_id if last_game.user1 else 0
        markup_result['user2_id'] = last_game.user2.m_id if last_game.user2 else 0
        if last_game.status != GameStatusChoices[1][0]:
            game_continue = False
        if last_game:
            # user1_point = last_game.get_points_lte(last_game.user1_id)
            # user2_point = last_game.get_points_lte(last_game.user2_id)

            if last_game.status == 'Finished' or last_game.status == 'CanceledGame':
                game_finished = True
                if last_game.win_user is None:
                   game1_result = "Draw"
                   game2_result = "Draw"
                else:
                    if last_game.win_user == last_game.user1:
                        game1_result = "{}, congratulations. You won the game".format(last_game.user1.get_full_name())
                        game2_result = "{}, sorry. You lost the game".format(last_game.user2.get_full_name())
                    else:
                        game1_result = "{}, sorry. You lost the game".format(last_game.user1.get_full_name())
                        game2_result = "{}, congratulations. You won the game".format(last_game.user2.get_full_name())
                user1_text = "{}\n{}\n{}\n{}".format("<b>{}</b>".format('Game Finished',),
                                   game1_result,
                                   "<b>Round:</b> {} ".format(last_game.round+1),
                                   "<b>{}:</b> {}".format(last_game.user1.get_full_name(),"|"*user1_point) if last_game.user1 else '',
                                           )
                user2_text = "{}\n{}\n{}\n{}".format("<b>{}</b>".format('Game Finished',),
                                   game2_result,
                                   "<b>Round:</b> {} ".format(last_game.round+1),
                                   "<b>{}:</b> {}".format(last_game.user2.get_full_name(),"|"*user2_point) if last_game.user2 else ''
                                           )
            else:
                game_finished = False
            # image1_url = "http://game.minbashi.com/media/{}".format(last_game.user1.avatar_result_photo_url) if last_game.user1.avatar_result_photo_url else ""
            # image2_url = "http://game.minbashi.com/media/{}".format(last_game.user2.avatar_result_photo_url) if last_game.user2.avatar_result_photo_url else ""
            # image1_url = "http://game.minbashi.com/media/photos/user-avatar/all/9cf6e8ead91b4a8090f84cc627caae46.png"
            # image2_url = "http://game.minbashi.com/media/photos/user-avatar/all/9cf6e8ead91b4a8090f84cc627caae46.png"
            # image1_url = "{}{}".format(domain_name,last_game.game_result_photo.url if last_game.game_result_photo else '')
            code = 1
            # if image1_url:
            #     image1_url = image1_url.split('/')[-1]
            # if image2_url:
            #     image2_url = image2_url.split('/')[-1]
            # image1_url = encoded_cipher
            markup_result['message1_id'] = last_game.user_message1 if last_game.user_message1 else 0
            markup_result['message2_id'] = last_game.user_message2 if last_game.user_message2 else 0
    # except:
    #     code = 0
    #     text = _('Something happened')
        data = {'message': text,'image1_url': image1_url,'image2_url': image2_url,'user1_text': user1_text,'user2_text': user2_text,'code': code,'markup_result':markup_result,'game_continue':game_continue,'game_finished':game_finished}
        return Response(data)

class GameStatusWithGeneralForStart(APIView):
    def post(self,request,g_id):
        text = ''
        user1_text = ''
        user2_text = ''
        image_url = ''
        image1_url = ''
        image2_url = ''
        code = 0
        user1_point = TOTALUSERGAMESTARTPOINT
        user2_point = TOTALUSERGAMESTARTPOINT
        game_continue = True
        game_finished = False
        markup_result = {
            'atack':'',
            'deffence':'',
            'ready': False,
            'message1_id':0,
            'message2_id':0
        }
        # try:
        last_game = Game.objects.filter(id=g_id).get()
        markup_result['user1_id'] = last_game.user1.m_id if last_game.user1 else 0
        markup_result['user2_id'] = last_game.user2.m_id if last_game.user2 else 0
        if last_game.status != GameStatusChoices[1][0]:
            game_continue = False
        if last_game:
            # user1_point = last_game.get_points_lte(last_game.user1_id)
            # user2_point = last_game.get_points_lte(last_game.user2_id)

            if user1_point <= 0 or user2_point <= 0:
                game_finished = True
                if user1_point == user2_point:
                   game1_result = "Draw"
                   game2_result = "Draw"
                else:
                    if user1_point > user2_point:
                        game1_result = "{}, congratulations. You won the game".format(last_game.user1.get_full_name())
                        game2_result = "{}, sorry. You lost the game".format(last_game.user2.get_full_name())
                    else:
                        game1_result = "{}, sorry. You lost the game".format(last_game.user1.get_full_name())
                        game2_result = "{}, congratulations. You won the game".format(last_game.user2.get_full_name())
                user1_text = "{}\n{}\n{}\n{}".format("<b>{}</b>".format('Game Finished',),
                                   game1_result,
                                   "<b>Round:</b> {} ".format(last_game.round+1),
                                   "<b>{}:</b> {}".format(last_game.user1.get_full_name(),"|"*user1_point) if last_game.user1 else '',
                                           )
                user2_text = "{}\n{}\n{}\n{}".format("<b>{}</b>".format('Game Finished',),
                                   game2_result,
                                   "<b>Round:</b> {} ".format(last_game.round+1),
                                   "<b>{}:</b> {}".format(last_game.user2.get_full_name(),"|"*user2_point) if last_game.user2 else ''
                                           )
            else:
                user1_text = "{}\n{}".format("<b>Round:</b> {}".format(last_game.round+1),
                                           "<b>{}:</b> {}".format(last_game.user1.get_full_name(),"|"*user1_point) if last_game.user1 else '',
                                           # "<b>{}:</b> {}".format(last_game.user2.get_full_name(),user2_point) if last_game.user2 else ''
                                                 )
                user2_text = "{}\n{}".format("<b>Round:</b> {}".format(last_game.round+1),
                                           # "<b>{}:</b> {}".format(last_game.user1.get_full_name(),user1_point) if last_game.user1 else '',
                                           "<b>{}:</b> {}".format(last_game.user2.get_full_name(),"|"*user2_point) if last_game.user2 else ''
                                                 )
            # image_url = "{}{}".format(domain_name,last_game.game_result_photo.url if last_game.game_result_photo else '')
            image1_url = "http://game.minbashi.com/media/{}".format(last_game.user1.avatar_result_photo_url) if last_game.user1.avatar_result_photo_url else ""
            image2_url = "http://game.minbashi.com/media/{}".format(last_game.user2.avatar_result_photo_url) if last_game.user2.avatar_result_photo_url else ""
            # image1_url = "http://game.minbashi.com/media/photos/user-avatar/all/9cf6e8ead91b4a8090f84cc627caae46.png"
            # image2_url = "http://game.minbashi.com/media/photos/user-avatar/all/9cf6e8ead91b4a8090f84cc627caae46.png"
            # image1_url = "{}{}".format(domain_name,last_game.game_result_photo.url if last_game.game_result_photo else '')
            code = 1
            # if image1_url:
            #     image1_url = image1_url.split('/')[-1]
            # if image2_url:
            #     image2_url = image2_url.split('/')[-1]
            # image1_url = encoded_cipher
            markup_result['message1_id'] = last_game.user_message1 if last_game.user_message1 else 0
            markup_result['message2_id'] = last_game.user_message2 if last_game.user_message2 else 0
    # except:
    #     code = 0
    #     text = _('Something happened')
        data = {'message': text,'image1_url': image1_url,'image2_url': image2_url,'user1_text': user1_text,'user2_text': user2_text,'code': code,'markup_result':markup_result,'game_continue':game_continue,'game_finished':game_finished}
        return Response(data)
    def get(self,request,g_id):
        text = ''
        user1_text = ''
        user2_text = ''
        image_url = ''
        image1_url = ''
        image2_url = ''
        code = 0
        user1_point = TOTALUSERGAMESTARTPOINT
        user2_point = TOTALUSERGAMESTARTPOINT
        game_continue = True
        game_finished = False
        markup_result = {
            'atack':'',
            'deffence':'',
            'ready': False,
            'message1_id':0,
            'message2_id':0
        }
        # try:
        last_game = Game.objects.filter(id=g_id).get()
        markup_result['user1_id'] = last_game.user1.m_id if last_game.user1 else 0
        markup_result['user2_id'] = last_game.user2.m_id if last_game.user2 else 0
        if last_game.status != GameStatusChoices[1][0]:
            game_continue = False
        if last_game:
            # user1_point = last_game.get_points_lte(last_game.user1_id)
            # user2_point = last_game.get_points_lte(last_game.user2_id)

            if user1_point <= 0 or user2_point <= 0:
                game_finished = True
                if user1_point == user2_point:
                   game1_result = "Draw"
                   game2_result = "Draw"
                else:
                    if user1_point > user2_point:
                        game1_result = "{}, congratulations. You won the game".format(last_game.user1.get_full_name())
                        game2_result = "{}, sorry. You lost the game".format(last_game.user2.get_full_name())
                    else:
                        game1_result = "{}, sorry. You lost the game".format(last_game.user1.get_full_name())
                        game2_result = "{}, congratulations. You won the game".format(last_game.user2.get_full_name())
                user1_text = "{}\n{}\n{}\n{}".format("<b>{}</b>".format('Game Finished',),
                                   game1_result,
                                   "<b>Round:</b> {} ".format(last_game.round+1),
                                   "<b>{}:</b> {}".format(last_game.user1.get_full_name(),"|"*user1_point) if last_game.user1 else '',
                                           )
                user2_text = "{}\n{}\n{}\n{}".format("<b>{}</b>".format('Game Finished',),
                                   game2_result,
                                   "<b>Round:</b> {} ".format(last_game.round+1),
                                   "<b>{}:</b> {}".format(last_game.user2.get_full_name(),"|"*user2_point) if last_game.user2 else ''
                                           )
            else:
                user1_text = "{}\n{}".format("<b>Round:</b> {}".format(last_game.round+1),
                                           "<b>{}:</b> {}".format(last_game.user1.get_full_name(),"|"*user1_point) if last_game.user1 else '',
                                           # "<b>{}:</b> {}".format(last_game.user2.get_full_name(),user2_point) if last_game.user2 else ''
                                                 )
                user2_text = "{}\n{}".format("<b>Round:</b> {}".format(last_game.round+1),
                                           # "<b>{}:</b> {}".format(last_game.user1.get_full_name(),user1_point) if last_game.user1 else '',
                                           "<b>{}:</b> {}".format(last_game.user2.get_full_name(),"|"*user2_point) if last_game.user2 else ''
                                                 )
            # image_url = "{}{}".format(domain_name,last_game.game_result_photo.url if last_game.game_result_photo else '')
            image1_url = "http://game.minbashi.com/media/{}".format(last_game.user1.avatar_result_photo_url) if last_game.user1.avatar_result_photo_url else ""
            image2_url = "http://game.minbashi.com/media/{}".format(last_game.user2.avatar_result_photo_url) if last_game.user2.avatar_result_photo_url else ""
            # image1_url = "http://game.minbashi.com/media/photos/user-avatar/all/9cf6e8ead91b4a8090f84cc627caae46.png"
            # image2_url = "http://game.minbashi.com/media/photos/user-avatar/all/9cf6e8ead91b4a8090f84cc627caae46.png"
            # image1_url = "{}{}".format(domain_name,last_game.game_result_photo.url if last_game.game_result_photo else '')
            code = 1
            # if image1_url:
            #     image1_url = image1_url.split('/')[-1]
            # if image2_url:
            #     image2_url = image2_url.split('/')[-1]
            # image1_url = encoded_cipher
            markup_result['message1_id'] = last_game.user_message1 if last_game.user_message1 else 0
            markup_result['message2_id'] = last_game.user_message2 if last_game.user_message2 else 0
    # except:
    #     code = 0
    #     text = _('Something happened')
        data = {'message': text,'image1_url': image1_url,'image2_url': image2_url,'user1_text': user1_text,'user2_text': user2_text,'code': code,'markup_result':markup_result,'game_continue':game_continue,'game_finished':game_finished}
        return Response(data)


class GameActionApiView(APIView):
    def post(self,request,c_id):
        text = ''
        image_url = ''
        code = 0
        game_id = 0
        markup_result = {
            'atack':[],
            'deffence':[],
            'ready': False,
        }
        atack_list = []
        deffence_list = []

        atack_def = request.POST.get('atack_def')
        try:
            user = GUser.objects.get(m_id=c_id)
            if user.game_status == 3 :
                last_game = Game.objects.by_user(user).filter(status=GameStatusChoices[1][0]).order_by('-date').first()
                if last_game:
                    game_id = last_game.id
                    ga_obj, created = GameAction.objects.get_or_create(user=user, game=last_game,round=last_game.round)
                    if ga_obj.ready is False:
                        if atack_def in [x[0] for x in AtackChoices]:
                            atack_def = str(atack_def)
                            if ga_obj.attack == atack_def or ga_obj.attack2 == atack_def:
                                if ga_obj.attack == atack_def:
                                    ga_obj.attack = None
                                if ga_obj.attack2 == atack_def:
                                    ga_obj.attack2 = None
                            else:
                                if ga_obj.attack:
                                    ga_obj.attack2 = atack_def
                                else:
                                    ga_obj.attack = atack_def

                        elif atack_def in [x[0] for x in DeffenceChoices]:
                            atack_def = str(atack_def)
                            if ga_obj.deffence == atack_def or ga_obj.deffence2 == atack_def:
                                if ga_obj.deffence == atack_def:
                                    ga_obj.deffence = None
                                if ga_obj.deffence2 == atack_def:
                                    ga_obj.deffence2 = None
                            else:
                                if ga_obj.deffence:
                                    ga_obj.deffence2 = atack_def
                                else:
                                    ga_obj.deffence = atack_def

                        ga_obj.save()
                        result_list = []
                        if ga_obj.attack:
                            atack_list.append(ga_obj.attack)
                            result_list.append(ga_obj.attack)
                        if ga_obj.attack2:
                            atack_list.append(ga_obj.attack2)
                            result_list.append(ga_obj.attack2)
                        if ga_obj.deffence:
                            deffence_list.append(ga_obj.deffence)
                            result_list.append(ga_obj.deffence)
                        if ga_obj.deffence2:
                            deffence_list.append(ga_obj.deffence2)
                            result_list.append(ga_obj.deffence2)
                        if len(result_list) >= 3:
                            ga_obj.ready = True
                        ga_obj.save()
                        code = 1
                    else:
                        code = 2
                    markup_result = {
                        'atack': atack_list,
                        'deffence': deffence_list,
                        'ready': ga_obj.ready,
                    }
            if user.game_status == 1 or user.game_status == 2 :
                text = "You have to wait to strart to play the game"
                code = 2
            if user.game_status == 0:
                text = "You have not any waiting game"
                code = 3
        except:
            code = 0
            text = _('Something happened')
        data = {'message': text,'image_url': image_url,'code': code,'markup_result':markup_result,'game_id':game_id}
        return Response(data)
    def get(self,request,c_id):
        text = ''
        image_url = ''
        code = 0
        game_id = 0
        try:
            user = GUser.objects.get(m_id=c_id)
            if user.game_status == 3 :
                last_game = Game.objects.by_user(user).order_by('-date').first()
                if last_game:
                    game_id = last_game.id
                    user1_point = last_game.get_points(last_game.user1_id)
                    user2_point = last_game.get_points(last_game.user2_id)
                    text = "{}\n{}\n{}".format("<b>Round:</b> {}".format(last_game.round),"<b>{}:</b> {}".format(last_game.user1.get_full_name(),"|"*user1_point),"<b>{}:</b> {}".format(last_game.user2.get_full_name(),"|"*user2_point))
                    image_url = "{}{}".format(domain_name,last_game.game_result_photo.url if last_game.game_result_photo else '')
                    code = 1
            if user.game_status == 1 or user.game_status == 2 :
                text = "You have to wait to strart to play the game"
                code = 2
            if user.game_status == 0:
                text = "You have not any waiting game"
                code = 3
        except:
            code = 0
            text = _('Something happened')
        data = {'message': text,'image_url': image_url,'code': code,'game_id': game_id,}
        return Response(data)

class UserActiveGame(APIView):
    def post(self,request,c_id):
        try:
            text = ''
            url = ''
            code = 0
            user = GUser.objects.get(m_id=c_id)
            if user.game_status == 3 :
                text = "You can not create a game becuase you are in a game now"
                code = 2
            elif user.game_status == 1 or user.game_status == 2 :
                text = "You can not create a game becuase you have waiting a game"
                code = 2
            elif user.game_status == 0:
                game_obj = Game(user1=user,status=GameStatusChoices[0][0],type=type)
                game_obj.save()
                text = "You have created the game successfully."
                if type == GameTypeChoices[0][0]:
                    user.game_status = USERGAMESTATUS[2][0]
                    user.save()
                    text = "{} {}".format(text, "Please wait any player to start to play the game")
                elif type == GameTypeChoices[1][0]:
                    text = "{} {} -> {}".format(text, "Please send the link any telegram user to start to play the game ", 'https://t.me/{}?startgame={}'.format(TELEGRAM_BOT_NAME,game_obj.u_id))
                    user.game_status = USERGAMESTATUS[1][0]
                    user.save()
                code = 1
        except:
            code = 0
            text = _('Please authorize')
        data = {'message':  text,'code':code}
        return Response(data)






class UserReferalApiView(APIView):
    def post(self,request,c_id):
        referal_game = request.POST.get('referal-game', '')
        if referal_game:
            try:
                user = AllUserStatus.objects.get(chat_id=c_id)
            except:
                user = AllUserStatus(chat_id=c_id).save()
            if referal_game:
                user.referal_game = referal_game
                user.save()
            data = {'code':1}
        else:
            data = {'code':0}
        return Response(data)
    def get(self,request,c_id):
        try:
            user = AllUserStatus.objects.get(chat_id=c_id)
        except:
            user = AllUserStatus(chat_id=c_id)
            user.save()
        data = {'code':1,'referal':user.referal_game}
        return Response(data)



class NextStepStatus(APIView):
    def get(self,request,m_id):
        try:
            user = AllUserStatus.objects.get(chat_id=str(m_id))
        except:
            user = AllUserStatus(chat_id=str(m_id)).save()
        status = user.next_step_status
        data = {'status':  status,'code':1}
        return Response(data)
    def post(self,request,m_id):
        ns_status = request.POST.get('ns_status', '-')
        character_name = request.POST.get('character_name', None)
        phone_number = request.POST.get('phone_number', None)
        gender = request.POST.get('gender', None)
        if ns_status:
            try:
                user = AllUserStatus.objects.get(chat_id=m_id)
            except:
                user = AllUserStatus(chat_id=m_id)
            user.next_step_status = ns_status
            if character_name:
                user.character_name = character_name
            if phone_number:
                if "+" in phone_number:
                    user.phone_number = phone_number
                else:
                    user.phone_number = "+{}".format(phone_number)
            if gender:
                user.gender = gender
            user.save()
            data = {'code':1}
        else:
            data = {'code':0}
        return Response(data)





class MyStrategyStatus(APIView):

    def get(self,request,c_id):
        result_data = {
                "strength" :0,
                "quickness": 0,
                "intuition":0,
                "durability":0,
                "total_point":TOTALUSERGAMEPOINT,
                "data_changed":False,
        }
        try:
            user_status = AllUserStatus.objects.get(chat_id=c_id)
            mainSettings = MainSettings.objects.filter().first()
            my_points = user_status.strength + user_status.quickness + user_status.intuition + user_status.durability
            total_points = mainSettings.limit_point - my_points if mainSettings and mainSettings.limit_point else TOTALUSERGAMEPOINT
            text = '{}'.format("Fill your stats")
            result_data = {
                    "strength" :user_status.strength,
                    "quickness": user_status.quickness,
                    "intuition":user_status.intuition,
                    "durability":user_status.durability,
                    "total_point":total_points,
                    "data_changed":False,
            }
            image_url = str(user_status.avatar.photo.url) if user_status.avatar.photo and user_status.avatar else ''
            code = 1
        except:
            code = 0
            image_url = ''
            text = _('Please authorize')
        if image_url:
            image_url = "{}{}".format(domain_name,image_url)
        result_data['image_url'] = image_url
        data = {'message':  text,'code':code,'result_data':result_data}
        return Response(data)


class CheckCreateOrEditUser(APIView):
    def post(self,request,c_id):
        start_game_status = ''
        try:
            user_status_obj = AllUserStatus.objects.get(chat_id=str(c_id))
            if user_status_obj.get_total_point() > 0:
                code = 2
                text = 'You have {} points, You have to use them'.format(user_status_obj.get_total_point())
            else:
                user_obj = GUser.objects.filter(m_id=c_id).first()
                new_data = False
                if user_obj:
                    if user_obj.active_avatar_photo != user_status_obj.avatar:
                        user_obj.active_avatar_photo = user_status_obj.avatar
                else:
                    new_data = True
                    user_obj = GUser()
                    random_string = str(random.random()).encode('utf8')
                    salt = hashlib.sha1(random_string).hexdigest()[:5]
                    password = make_password(user_status_obj.phone_number, salt=salt)
                    user_obj.email = "user{}@ourdom.com".format(user_status_obj.chat_id)
                    user_obj.last_name = ''
                    user_obj.is_active = True
                    user_obj.password = password
                    user_obj.active_avatar_photo = user_status_obj.avatar
                user_obj.first_name = user_status_obj.character_name
                user_obj.username = str(user_status_obj.chat_id)
                user_obj.m_id = user_status_obj.chat_id
                user_obj.gender = user_status_obj.gender
                user_obj.phone = user_status_obj.phone_number

                user_obj.strength = user_status_obj.strength
                user_obj.quickness = user_status_obj.quickness
                user_obj.intuition = user_status_obj.intuition
                user_obj.durability = user_status_obj.durability

                user_obj.usertype = 3
                user_obj.save()
                make_avatar(user_obj.pk)
                if new_data:
                    UserAvatar(user=user_obj,avatar=user_status_obj.avatar,status='Selected').save()
                else:
                    us_av = UserAvatar.objects.filter(user=user_obj,avatar=user_status_obj.avatar)
                    if us_av:
                        UserAvatar.objects.filter(user=user_obj, status='Unselected')
                        us_av.update(status='Unselected')
                text = _('<b> You have finished the registration. </b>')
                user_status_obj.next_step_status = '-'
                user_status_obj.save()
                if user_status_obj.referal_game and user_status_obj.referal_game != '-':
                    try:
                        game_obj = Game.objects.filter(u_id=user_status_obj.referal_game,status=GameStatusChoices[0][0]).get()
                        start_game_status = game_obj.u_id
                    except:
                        pass
                code = 1
        except Exception as e:
            code = 0
            text = 'Try Again {}'.format(str(e))
        data = {'code':code,'message':text,'start_game_status':start_game_status}
        return Response(data)


class ChangeMyStrategy(APIView):
    def get(self,request,c_id):
        try:
            user = AllUserStatus.objects.get(chat_id=str(c_id))
            if user.get_total_point() > 0:
                code = 2
                text = 'You have {} points, You have to use them'.format(user.get_total_point())
            else:
                code = 1
                text = 'Saved'
        except:
            code = 0
            text = 'Try Again'
        data = {'code':code,'message':text}
        return Response(data)

    def post(self,request,c_id):
        result_data = {
                "strength" :0,
                "quickness": 0,
                "intuition":0,
                "durability":0,
                "total_point":TOTALUSERGAMEPOINT,
                "data_changed":False,
        }
        call_data = request.POST.get('call_data',None)
        try:
            user_status = AllUserStatus.objects.get(chat_id=str(c_id))
            mainSettings = MainSettings.objects.filter().first()
            my_points = copy.deepcopy(user_status.strength + user_status.quickness + user_status.intuition + user_status.durability)
            total_points = mainSettings.limit_point - my_points if mainSettings and mainSettings.limit_point > 0  else TOTALUSERGAMEPOINT
            if call_data == "cb_reset":
                user_status.strength = 0
                user_status.quickness = 0
                user_status.intuition = 0
                user_status.durability = 0
                user_status.save()
            else:
                if total_points > 0:
                    if call_data == "cb_strength_plus":
                        user_status.strength = user_status.strength + 1
                    elif call_data == "cb_quickness_plus":
                        user_status.quickness = user_status.quickness + 1
                    elif call_data == "cb_intuition_plus":
                        user_status.intuition = user_status.intuition + 1
                    elif call_data == "cb_durability_plus":
                        user_status.durability = user_status.durability + 1
                    user_status.save()
            r = requests.get('{}/bot-api/my-strategy-status/{}/'.format(domain_name,c_id),)
            data = json.loads(r.text)
            result_data = data['result_data']
            text = call_data
            if total_points != result_data['total_point']:
                result_data['data_changed'] = True
            code = 1
        except:
            code = 0
            text = _('Please authorize')
        data = {'message':  text,'code':code,'result_data':result_data,'call_data':call_data}
        return Response(data)


class ChangeMyStrategyData(APIView):

    def get(self,request,c_id):
        result_data = {
                "strength" :0,
                "quickness": 0,
                "intuition":0,
                "durability":0,
                "total_point":TOTALUSERGAMEPOINT,
        }
        call_data = request.GET.get('call_data',None)
        text = "call_data = {}".format(call_data)
        try:
            user_status = AllUserStatus.objects.get(chat_id=str(c_id))
            if call_data == "cb_reset":
                user_status.strength = 0
                user_status.quickness = 0
                user_status.intuition = 0
                user_status.durability = 0
                user_status.save()
                # text = call_data
            elif call_data in CallDataList:
                mainSettings = MainSettings.objects.filter().first()
                my_points = user_status.strength + user_status.quickness + user_status.intuition + user_status.durability
                total_points = mainSettings.limit_point - my_points if mainSettings and mainSettings.limit_point > 0  else TOTALUSERGAMEPOINT
                if total_points > 0:
                    if call_data == "cb_strength_plus":
                        user_status.strength = user_status.strength + 1
                    if call_data == "cb_quickness_plus":
                        user_status.quickness = user_status.quickness + 1
                    if call_data == "cb_intuition_plus":
                        user_status.intuition = user_status.intuition + 1
                    if call_data == "cb_durability_plus":
                        user_status.durability = user_status.durability + 1
                    user_status.save()
                # text = call_data
            else:
                text = "call_data = {}".format(call_data)
            r = requests.get('{}/bot-api/my-strategy-status/{}/'.format(domain_name,c_id),)
            data = json.loads(r.text)
            result_data = data['result_data']
            code = 1
        except:
            code = 0
            text = _('Please authorize')
        data = {'message':  text,'code':code,'result_data':result_data,'call_data':call_data}
        return Response(data)

from decimal import Decimal



class SetUserLanguage(APIView):
    def post(self,request,m_id):
        language = request.POST.get('language')
        code = 0
        try:
            user = GUser.objects.get(m_id=m_id)
            user.language = language
            user.save()
            text = _('Language have changed successfully')
        except:
            text = _('Try Again')
        data = {'message':  text,'code':code}
        return Response(data)

class CreateUser(APIView):
    def post(self,request):
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        m_id = request.POST.get('m_id')
        avatar = request.POST.get('avatar')
        code = 0
        if GUser.objects.filter(m_id=m_id):
            text = _('You have Account')
        else:
            try:
                try:
                    user_status_obj = AllUserStatus.objects.get(chat_id=m_id)
                except:
                    user_status_obj = AllUserStatus(chat_id=m_id)
                phone = user_status_obj.phone_number
                gender = user_status_obj.gender
                random_string = str(random.random()).encode('utf8')
                salt = hashlib.sha1(random_string).hexdigest()[:5]
                avatar_obj = Avatar.objects.filter(title=avatar).first()
                if avatar_obj:
                    password = make_password(phone, salt=salt)
                    user_obj = GUser()
                    if first_name:
                        user_obj.first_name = first_name
                    else:
                        user_obj.first_name = ''
                    if last_name:
                        user_obj.last_name = last_name
                    else:
                        user_obj.last_name = ''
                    user_obj.email = "user{}@ourdom.com".format(m_id)
                    user_obj.username = str(m_id)
                    user_obj.m_id = m_id
                    user_obj.gender = gender
                    user_obj.phone = phone
                    user_obj.usertype = 3
                    user_obj.is_active = True
                    user_obj.password = password
                    user_obj.active_avatar_photo = avatar_obj
                    user_obj.save()
                    us_av = UserAvatar(user=user_obj,avatar=avatar_obj,status='Selected').save()
                    code = 1
                    text = _('You have finished the registration. <b>Please set your strategy: </b>')
                    user_status_obj.next_step_status = '-'
                    user_status_obj.save()
                    code = 1
                else:
                    text = _('Please select correct Avatar')
            except  Exception as e:
                text = _(str(e))
        data = {'message':  text,'code':code}
        rsm = RegisterStepModel(chat_id=m_id)
        rsm.set_status(ns_status='-',extra_value=None)
        return Response(data)
# class SetPoint(APIView):
#     def get(self,request,m_id,username,set_username):
#         try:
#             author = GUser.objects.get(username=username)
#             if author.usertype != 2:
#                 try:
#                     set_user = GUser.objects.get(username=set_username,usertype=2)
#                     try:
#                         user_point = UserPoint(user=set_user,author=author)
#                         user_point.point = Decimal(request.GET.get('point'))
#                         user_point.save()
#                         text = _('Point successfully added. total point is {}'.format(user_point.user.point))
#                     except Exception as e:
#                         text = _('Point is not decimal')
#                 except:
#                     text = _('No employees found')
#             else:
#                 text = _('You can not do this operation')
#         except:
#             text = _('You have no account in the system')
#         data = {'message':  text}
#         return Response(data)


class SetLimitPoint(APIView):
    def get(self,request,m_id,username):
        try:
            author = GUser.objects.get(username=username)
            if author.usertype != 2:
                try:
                    setting_item = MainSettings.objects.filter().order_by('-date').first()
                    if setting_item:
                        setting_item.limit_point = Decimal('{}'.format(float(request.GET.get('point'))))
                        setting_item.save()
                    else:
                        setting_item = MainSettings(title='Main')
                        setting_item.limit_point = Decimal('{}'.format(float(request.GET.get('point'))))
                        setting_item.save()
                    text = _('Point have successfully changed.')
                except Exception as e:
                    text = _('Point is not decimal')
            else:
                text = _('You can not do this operation')
        except:
            text = _('You have no account in the system')
        data = {'message':  text}
        return Response(data)


# class GetMe(APIView):
#     def get(self,request,m_id,username):
#         try:
#             user = GUser.objects.get(username=username)
#             user.m_id = m_id
#
#             if request.POST.get('first_name', ''):
#                 user.first_name = request.POST.get('first_name', '')
#             if request.POST.get('last_name', ''):
#                 user.last_name = request.POST.get('last_name', '')
#             user.save()
#             if user.usertype == 2:
#                 text = 'Welcomeback {}, \n Your point: {}'.format(user.get_full_name(),'{}'.format(user.point),)
#                 points_text = ''
#                 points = UserPoint.objects.filter(user=user)
#                 total_point = 0.0
#                 if points:
#                     total_point = points.extra({'get_my_point': 'sum(point)'}).values('get_my_point')[0]['get_my_point'] if points else 0.0
#                     for point_item in points:
#                         points_text = "{}  \n{}   -   {}".format(points_text, point_item.point, point_item.date)
#                 points_text = "{}  \n Total point    -   {}".format(points_text, total_point)
#                 text = "{}{}".format(text,points_text)
#             else:
#                 text = 'Welcomeback {} '.format(user.get_full_name())
#         except:
#             text = _('You have no account in the system')
#         data = {'message':  text}
#         return Response(data)

class GetMenu(APIView):
    def get(self,request,m_id,username):
        try:
            user = GUser.objects.get(username=username)
            user.m_id = m_id
            if request.POST.get('first_name', ''):
                user.first_name = request.POST.get('first_name', '')
            if request.POST.get('last_name', ''):
                user.last_name = request.POST.get('last_name', '')
            user.save()
            if user.usertype == 2:
                text = '{}, Menu \n {} \n {}'.format(user.get_full_name(),
                                                          '/{} - {}'.format('menu',_("Operation menu")),
                                                          '/{} - {}'.format('me',_("About myself")),
                                                          )
            else:
                text = '{}, Menu ' \
                       '\n {}\n {}\n {}\n {}\n {}'.format(user.get_full_name(),
                        '/{} - {}'.format('menu',_("Operation menu")),
                        '/{} - {}'.format('me',_("About myself")),
                        '/{} - {}'.format('set_limit_point',_("Set limit point")),
                        '/{} - {}'.format('setpoint',_("Set point to user")),
                        '/{} - {}'.format('user_info',_("Show user info")),
                        '/{} - {}'.format('show_section_user_info',_("Show section user info")),
                )

        except:
            text = _('You have no account in the system')
        data = {'message':  text}
        return Response(data)
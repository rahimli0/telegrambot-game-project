import os

from celery import Celery, shared_task

from base_user.tools.common import USERGAMESTATUS
from bot_app.general.utils import TOTALUSERGAMEPENALTYPOINT
from bot_app.tools.tools import RepeatedTimer
from bot_app.general.widgets import GameEditModel, GameModel

from tbgm.settings import TELEGRAM_CHANNEL_NAME
# from threading import Timer
from time import sleep
def xrange(x):

    return iter(range(x))

@shared_task
def cancel_allgame(c_id):
    from panel.models import Game
    from bot_app.general.utils import GameStatusChoices
    from base_user.tools.common import USERGAMESTATUS
    from django.contrib.auth import get_user_model
    from django.db.models import Count, F
    import copy
    GUser = get_user_model()
    user = GUser.objects.get(m_id=c_id)
    Game.objects.by_user(user=user).filter(status=GameStatusChoices[0][0]).update(status=GameStatusChoices[4][0])
    user_actives = Game.objects.by_user(user=user).filter(status=GameStatusChoices[1][0])
    if user_actives:
        user_list = copy.deepcopy(user_actives.values_list('user1_id',flat=True).order_by('user1_id'))
        user2_list = copy.deepcopy(user_actives.values_list('user2_id',flat=True).order_by('user2_id'))
        GUser.objects.filter(id__in=list(user_list) + list(user2_list)).update(game_status=USERGAMESTATUS[0][0])
        user_actives.filter(user1=user).update(status=GameStatusChoices[4][0],win_user=F('user2'))
        user_actives.filter(user2=user).update(status=GameStatusChoices[4][0],win_user=F('user1'))
        print("{}".format(list(user_list) + list(user2_list)))
    # if user1_actives:
    #     user1_actives.update(win_user = F('user2'),user1__game_status=USERGAMESTATUS[0][0],user2__game_status=USERGAMESTATUS[0][0],status=GameStatusChoices[4][0])
    # user2_actives = Game.objects.filter(user2=user, status=GameStatusChoices[1][0])
    # if user2_actives:
    #     user2_actives.update(win_user = F('user1'),user1__game_status=USERGAMESTATUS[0][0],user2__game_status=USERGAMESTATUS[0][0],status=GameStatusChoices[4][0])

@shared_task
def delete_messages(c_id,l_m_id):
    import telebot
    import requests
    import time
    import copy
    from panel.models import AllUserStatus
    from bot_app.general.utils import commands
    from tbgm.settings import TELEGRAM_BOT_TOKEN
    bot = telebot.TeleBot(TELEGRAM_BOT_TOKEN)
    domain_name = 'https://game.minbashi.com'
    l_m_id = int(l_m_id)
    l_m_id_dc = copy.deepcopy(l_m_id) + 1
    try:
        user_status = AllUserStatus.objects.get(chat_id=str(c_id))
    except:
        user_status = AllUserStatus.objects.get(chat_id=str(c_id)).save()
    last_deleted_message = user_status.deleted_last_message_id
    help_text = "<b>The following commands are available:</b> \n"
    for key in commands:  # generate help text out of the commands dictionary defined at the top
        help_text += "/" + key + ": "
        help_text += commands[key] + "\n"
    bot.send_message(c_id, help_text,parse_mode='HTML')
    while l_m_id>=0 and l_m_id >= last_deleted_message:
        if l_m_id_dc - 1 == l_m_id:
            time.sleep(3)
            if l_m_id_dc > 0:
                try:
                    r2 = requests.get('https://api.telegram.org/bot{}/deleteMessage?chat_id={}&message_id={}'.format(
                        TELEGRAM_BOT_TOKEN, c_id, l_m_id_dc))
                except:
                    pass
        try:
            r2 = requests.get('https://api.telegram.org/bot{}/deleteMessage?chat_id={}&message_id={}'.format(
                TELEGRAM_BOT_TOKEN, c_id, l_m_id))
        except:
            pass
        l_m_id -= 1
    user_status.deleted_last_message_id = l_m_id_dc
    user_status.save()

@shared_task
def only_delete_messages(c_id,l_m_id):
    import telebot
    import requests
    import time
    import copy
    from bot_app.general.utils import commands
    from tbgm.settings import TELEGRAM_BOT_TOKEN
    from panel.models import AllUserStatus
    bot = telebot.TeleBot(TELEGRAM_BOT_TOKEN)
    domain_name = 'https://game.minbashi.com'
    l_m_id = int(l_m_id)
    l_m_id_dc = copy.deepcopy(l_m_id) + 1

    try:
        user_status = AllUserStatus.objects.get(chat_id=str(c_id))
    except:
        user_status = AllUserStatus(chat_id=str(c_id)).save()
    last_deleted_message = user_status.deleted_last_message_id
    while l_m_id>=0 and l_m_id >= last_deleted_message:
        try:
            r2 = requests.get('https://api.telegram.org/bot{}/deleteMessage?chat_id={}&message_id={}'.format(
                TELEGRAM_BOT_TOKEN, c_id, l_m_id))
        except:
            pass
        l_m_id -= 1
    user_status.deleted_last_message_id = l_m_id_dc
    user_status.save()
    return True

@shared_task
def one_delete_messages(c_id,l_m_id):
    import requests
    from tbgm.settings import TELEGRAM_BOT_TOKEN
    try:
        r2 = requests.get('https://api.telegram.org/bot{}/deleteMessage?chat_id={}&message_id={}'.format(
            TELEGRAM_BOT_TOKEN, c_id, l_m_id))
    except:
        pass

@shared_task
def channel_message(text):
    import requests
    from tbgm.settings import TELEGRAM_BOT_TOKEN
    try:
        import telebot
        bot = telebot.TeleBot(TELEGRAM_BOT_TOKEN)
        bot.send_message(chat_id=TELEGRAM_CHANNEL_NAME,text=text)
        # r2 = requests.post('https://api.telegram.org/bot{}/sendMessage?chat_id={}&text={}'.format(
        #     TELEGRAM_BOT_TOKEN, TELEGRAM_CHANNEL_NAME, text))
    except:
        pass

@shared_task
def make_avatar(u_pk):
    from panel.models import Avatar,AvatarItem,UserAvatarItem
    from django.contrib.auth import get_user_model
    from PIL import Image, ImageDraw, ImageFilter
    from django.conf import settings
    from uuid import uuid4
    import datetime
    GUser = get_user_model()
    now = datetime.datetime.now()
    # try:
    user = GUser.objects.filter(pk=u_pk).get()
    new_file = ''
    if user.active_avatar_photo:

        avatar_photo = Image.open(os.path.join(settings.MEDIA_ROOT ,str(user.active_avatar_photo.photo)))
        ext = str(user.active_avatar_photo.photo.url).split('.')[-1]
        back_im = avatar_photo.copy()
        selected_avatar_items = UserAvatarItem.objects.filter(status='Selected')
        for user_avatarItem_item in selected_avatar_items:
            im2 = Image.open(os.path.join(settings.MEDIA_ROOT ,str(user_avatarItem_item.avatar_item.photo)))
            back_im.paste(im2, (user_avatarItem_item.avatar_item.x_coordinate, user_avatarItem_item.avatar_item.y_coordinate), Image.open(os.path.join(settings.MEDIA_ROOT ,str(user_avatarItem_item.avatar_item.photo))))
            # back_im.paste(im2, (395, 270), im2)
        new_file = 'photos/user-avatar/all/{}'.format('{}.{}'.format(uuid4().hex, ext))
        back_im.save(os.path.join(settings.MEDIA_ROOT ,str(new_file)), quality=60)
        user.avatar_result_photo.name = new_file
        user.avatar_result_photo_url = "{}".format(new_file)
        user.save()
    # except:
    #     pass
    return new_file

def none_to_space(data):
    if data is None:
        return ''
    else:
        return data

def len_array(arr):
    new_arr = []
    for x in arr:
        if str(x).strip():
            new_arr.append(x)
    return_val = len(new_arr)
    return return_val

def split_data(deff):
  new_arr = []
  for x in deff.split('&'):
      if str(x).strip():
          new_arr.append(str(x).strip())
  return new_arr


def atack_deffence(atack,deff):
    atack = dict(atack)
    deff = dict(deff)

    total_point = (atack["Head"] - deff["Head"]) + (atack["Body"] - deff["Body"]) + (atack["Leg"] - deff["Leg"])

    return total_point

def at_last(g_id):
    from panel.models import GameAction, Game
    from bot_app.general.utils import GameStatusChoices
    g_obj = Game.objects.filter(id=g_id).get()
    geModel = GameEditModel(g_id=g_id,u1_id=g_obj.user1.m_id,u2_id=g_obj.user2.m_id)
    ga_obj, created_ga  = GameAction.objects.get_or_create(round=g_obj.round,ready=True, game=g_obj,user=g_obj.user1)
    gao_obj, created_gao  = GameAction.objects.get_or_create(round=g_obj.round,ready=True, game=g_obj,user=g_obj.user2)
    return_val = [True,'continue']
    game_finished = False
    try:
        ga_atack_list  = [none_to_space(ga_obj.attack)]
        gao_atack_list  = [none_to_space(gao_obj.attack)]
        # ga_obj_res = z = list(set(ga_obj.attack) - set(ga_obj.attack.split('&')))
        if created_ga is False and created_gao is False:
            user1_point = g_obj.get_points_lte(g_obj.user1_id)
            user2_point = g_obj.get_points_lte(g_obj.user2_id)
            if user1_point <= 0 or user2_point <= 0:
                game_finished = True
                g_obj.status = GameStatusChoices[2][0]
                g_obj.user1.game_status = USERGAMESTATUS[0][0]
                g_obj.user2.game_status = USERGAMESTATUS[0][0]
                g_obj.user1.save()
                g_obj.user2.save()
                if user1_point > user2_point:
                    g_obj.win_user = g_obj.user1
                elif user1_point < user2_point:
                    g_obj.win_user = g_obj.user2
            else:
                g_obj.round = g_obj.round + 1
            g_obj.save()
            # if game_finished is not True:
            if g_obj.status == GameStatusChoices[4][0]:
                pass
            else:
                geModel.updated_game_message()
            print("*-------------- created_ga == False and created_gao == False")
        elif created_ga and created_gao:
            print("*--------------elif created_ga and created_gao")
            ga_obj.point = TOTALUSERGAMEPENALTYPOINT
            ga_obj.passed = True
            gao_obj.point = TOTALUSERGAMEPENALTYPOINT
            gao_obj.passed = True
            ga_obj.save()
            gao_obj.save()
            user1_point = g_obj.get_points_lte(g_obj.user1_id)
            user2_point = g_obj.get_points_lte(g_obj.user2_id)
            if user1_point <= 0 or user2_point <= 0:
                game_finished = True
                g_obj.status = GameStatusChoices[2][0]
                g_obj.user1.game_status = USERGAMESTATUS[0][0]
                g_obj.user2.game_status = USERGAMESTATUS[0][0]
                g_obj.user1.save()
                g_obj.user2.save()
                if user1_point > user2_point:
                    g_obj.win_user = g_obj.user1
                elif user1_point < user2_point:
                    g_obj.win_user = g_obj.user2
            else:
                g_obj.round = g_obj.round + 1
            g_obj.save()
            if g_obj.status == GameStatusChoices[4][0]:
                pass
            else:
                geModel.updated_game_message()
        else:
            print("*--------------else created_ga == {} and created_gao == {}  ---".format(created_ga,created_gao))
            ga_obj_action = atack_deffence(gao_obj.get_result_formated()[3], ga_obj.get_result_formated()[4])
            gao_obj_action = atack_deffence(ga_obj.get_result_formated()[3], gao_obj.get_result_formated()[4])
            ga_obj.point = ga_obj_action if ga_obj_action >= 0 else 0
            ga_obj.passed = True
            gao_obj.point = gao_obj_action if ga_obj_action >= 0 else 0
                # (TOTALUSERGAMEPENALTYPOINT * len_array(gao_obj_res) - TOTALUSERGAMEPENALTYPOINT * len_array(ga_obj_res))  if TOTALUSERGAMEPENALTYPOINT * len_array(gao_obj_res) - TOTALUSERGAMEPENALTYPOINT * len(ga_obj_res) > 0 else 0
            gao_obj.passed = True
            gao_obj.save()
            user1_point = g_obj.get_points_lte(g_obj.user1_id)
            user2_point = g_obj.get_points_lte(g_obj.user2_id)
            if user1_point <= 0 or user2_point <= 0:
                game_finished = True
                g_obj.status = GameStatusChoices[2][0]
                g_obj.user1.game_status = USERGAMESTATUS[0][0]
                g_obj.user2.game_status = USERGAMESTATUS[0][0]
                g_obj.user1.save()
                g_obj.user2.save()
                if user1_point > user2_point:
                    g_obj.win_user = g_obj.user1
                elif user1_point < user2_point:
                    g_obj.win_user = g_obj.user2
            else:
                g_obj.round = g_obj.round + 1
            g_obj.save()
            # if game_finished is not True:
            if g_obj.status == GameStatusChoices[4][0]:
                pass
            else:
                geModel.updated_game_message()
        return_val = [False,'continue', 'Go to next round']
        print("[False,'continue', 'Go to next round']")
    except Exception as e:
        return_val = [True,'continue', 'Please wait your opponent']
        print("[True,'continue', 'Please wait your opponent'] {}".format(str(e)))
    return return_val

def at_check_callback(g_id):
    from panel.models import GameAction, Game
    from bot_app.general.utils import GameStatusChoices
    print("at_check_callback... {}".format(g_id))
    g_obj = Game.objects.filter(id=g_id).get()
    # geModel = GameEditModel(g_id=g_id,u1_id=g_obj.user1.m_id,u2_id=g_obj.user2.m_id)
    # return_val = [True,'continue']
    game_finished = False
    try:
        ga_obj = GameAction.objects.filter(round=g_obj.round,ready=True, game=g_obj,user=g_obj.user1).get()
        gao_obj = GameAction.objects.filter(round=g_obj.round,ready=True, game=g_obj,user=g_obj.user2).get()
        # if ga_obj and gao_obj:
        ga_obj_action = atack_deffence(gao_obj.get_result_formated()[3], ga_obj.get_result_formated()[4])
        gao_obj_action = atack_deffence(ga_obj.get_result_formated()[3], gao_obj.get_result_formated()[4])
        ga_obj.point = ga_obj_action if ga_obj_action >= 0 else 0
        ga_obj.passed = True
        gao_obj.point = gao_obj_action if ga_obj_action >= 0 else 0
        gao_obj.passed = True
        ga_obj.save()
        gao_obj.save()
        return_val = [True,'continue', 'Go to next round','round={} ga_obj_action = {} - gao_obj_action={}'.format(g_obj.round,ga_obj_action,gao_obj_action)]
        print(return_val)
    except Exception as e:
        print("[False,'continue', 'Please wait your opponent'.format(str(e))] {}".format(str(e)))
        return_val = [False,'continue', 'Please wait your opponent {}'.format(str(e))]
    return return_val



@shared_task
def game_action_task(g_id):
    from panel.models import Game
    print("starting...")
    print("starting... {}".format(g_id))
    g_obj = Game.objects.filter(id=g_id).get()

    if g_obj.get_points(g_obj.user1_id) > 0 and g_obj.get_points(g_obj.user2_id) > 0:
        rt = RepeatedTimer(6, at_check_callback, g_id)  # it auto-starts, no need of rt.start()
        print(rt.result)
        try:
            sleep(60)  # your long-running job goes here...
        finally:
            rt.stop()  # better in a try/finally block to make sure the program ends!
        at_last(g_id)
    print('finally')


@shared_task
def game_action_main_task(g_id):
    from panel.models import Game
    print("starting...")
    print("starting... {}".format(g_id))
    g_obj = Game.objects.filter(id=g_id).get()
    import time

    print("Start : %s" % time.ctime())

    i = 1
    result = False
    if g_obj.get_points(g_obj.user1_id) > 0 and g_obj.get_points(g_obj.user2_id) > 0:
        while i < 4 and result is False:
            time.sleep(9.8)
            result = at_check_callback(g_id)[0]
            print('while-end i={} result='.format(i,str(result)))
            i+=1
    print('while-end i={}'.format(i))
    at_last(g_id)

    print('finally')
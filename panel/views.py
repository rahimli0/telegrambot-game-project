import hashlib
import random

from celery.utils.time import timezone
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import make_password
from django.db.models import Q
from datetime import datetime
from django.http import HttpResponse, Http404, JsonResponse
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.translation import ugettext as _

from base_user.tools.common import USERTYPES, AdminstrativUSERTYPES
from panel.decorators import check_telegram_user, is_admin, is_adminstrativ

GUser = get_user_model()

from django_telegram_login.widgets.constants import (
    SMALL,
    MEDIUM,
    LARGE,
    DISABLE_USER_PHOTO,
)
from django_telegram_login.widgets.generator import (
    create_callback_login_widget,
    create_redirect_login_widget,
)
from django_telegram_login.authentication import verify_telegram_authentication
from django_telegram_login.errors import (
    NotTelegramDataError,
    TelegramDataIsOutdatedError,
)

from general.views import base_auth
from panel.forms import *
from panel.models import *

bot_name = settings.TELEGRAM_BOT_NAME
bot_token = settings.TELEGRAM_BOT_TOKEN
redirect_url = settings.TELEGRAM_LOGIN_REDIRECT_URL

from django_telegram_login.authentication import verify_telegram_authentication
from django_telegram_login.errors import (
    NotTelegramDataError,
    TelegramDataIsOutdatedError,
)




@login_required(login_url='base-user:callback')
@check_telegram_user
# @is_adminstrativ(rule_item='dashboard')
def index(request):

    context = base_auth(req=request)
    user = request.user
    if request.method == 'POST' and request.is_ajax():
        message_code = 0
        result_data = ''
        try:
            if user.usertype != 2:
                result_data = '{}'.format(render_to_string(
                    "panel/include/home/main.html",
                    {
                        'section_count': 0,
                        # 'active_tasks_count': EmployeeTask.objects.filter(is_canceled=False,is_finished=False).count(),
                        'employees_count': GUser.objects.filter(is_deleted=False,is_active=True).count(),
                    }
                ))
            else:
                result_data = ''
            message_code = 1
        except:
            pass
        data = {
            'main_result': result_data,
            'message_code': message_code,
        }
        return JsonResponse(data=data)
    return render(request, 'panel/dashboard.html', context=context)


@login_required(login_url='base-user:callback')
@check_telegram_user
@is_admin
def test(request):

    context = base_auth(req=request)
    return render(request, 'base-main.html', context=context)






USERSearchCHOICE = (
    ('first_name',_('First name')),
    ('last_name',_('Last name')),
    ('username',_('Username')),
    ('date',_('Date')),
)


@login_required(login_url='base-user:callback')
@check_telegram_user
@is_admin
# @is_adminstrativ(rule_item='user-list')
def user_list(request):
    user= request.user
    context = base_auth(req=request)
    inital_data = {
        'order_type':'-',
        'field_list':'title',
    }
    search_form = GeneralSearchForm(USERSearchCHOICE,request.POST or None,initial=inital_data)
    if request.method=='POST' and request.is_ajax():
        if search_form.is_valid():
            cleaned_data = search_form.cleaned_data
            first_name = cleaned_data.get('first_name',None)
            last_name = cleaned_data.get('last_name',None)
            username = cleaned_data.get('username',None)
            field_list = cleaned_data.get('field_list',None)
            order_type = cleaned_data.get('order_type',None)

            if user.usertype == 1:
                data_list = GUser.objects.filter(is_deleted=False,is_active=True).order_by('{}{}'.format(order_type,field_list))
            else:
                data_list = GUser.objects.filter(is_deleted=False,is_active=True,usertype=2).order_by('{}{}'.format(order_type,field_list))
            if first_name:
                data_list = data_list.filter(first_name__icontains=first_name)

            if last_name:
                data_list = data_list.filter(last_name__icontains=last_name)

            if username:
                data_list = data_list.filter(username__icontains=username)

            try:
                page_num = int(request.POST.get('page-num', 2))
            except:
                page_num = 0
            list_html = ''
            for_i = (page_num-1)*50
            data_list = data_list[50 * (page_num - 1):50 * page_num]
            for data_list_item in data_list[50 * (page_num - 1):50 * page_num]:
                for_i += 1
                list_html = '{}{}'.format(list_html, render_to_string(
                    "panel/users/include/list-item.html",
                    {
                        'for_i': for_i,
                        'base_profile': request.user,
                        'data_item': data_list_item,
                    }
                ))
            if page_num < 2:
                main_html = "{}".format(
                    render_to_string(
                        'panel/users/include/main.html',
                        {
                            'list_html': list_html,
                        }
                    )
                )
            else:
                main_html = list_html
            message_code = 1
            data = {
                'main_result': main_html,
                'message_code': message_code,
            }
            return JsonResponse(data=data)
    context['search_form'] = search_form


    return render(request, 'panel/users/user-list.html', context=context)



@login_required(login_url='base-user:callback')
@check_telegram_user
@is_admin
def user_list_removed(request):
    user= request.user
    context = base_auth(req=request)
    inital_data = {
        'order_type':'-',
        'field_list':'title',
    }
    search_form = GeneralSearchForm(USERSearchCHOICE,request.POST or None,initial=inital_data)
    if request.method=='POST' and request.is_ajax():
        if search_form.is_valid():
            cleaned_data = search_form.cleaned_data
            first_name = cleaned_data.get('first_name',None)
            last_name = cleaned_data.get('last_name',None)
            username = cleaned_data.get('username',None)
            field_list = cleaned_data.get('field_list',None)
            order_type = cleaned_data.get('order_type',None)

            if user.usertype == 1:
                data_list = GUser.objects.filter(is_deleted=True).order_by('{}{}'.format(order_type,field_list))
            else:
                data_list = GUser.objects.filter(is_deleted=True,usertype=2).order_by('{}{}'.format(order_type,field_list))
            if first_name:
                data_list = data_list.filter(first_name__icontains=first_name)

            if last_name:
                data_list = data_list.filter(last_name__icontains=last_name)

            if username:
                data_list = data_list.filter(username__icontains=username)

            try:
                page_num = int(request.POST.get('page-num', 2))
            except:
                page_num = 0
            list_html = ''
            for_i = (page_num-1)*50
            data_list = data_list[50 * (page_num - 1):50 * page_num]
            for data_list_item in data_list[50 * (page_num - 1):50 * page_num]:
                for_i += 1
                list_html = '{}{}'.format(list_html, render_to_string(
                    "panel/users/include/list-item-removed.html",
                    {
                        'for_i': for_i,
                        'base_profile': request.user,
                        'data_item': data_list_item,
                    }
                ))
            if page_num < 2:
                main_html = "{}".format(
                    render_to_string(
                        'panel/users/include/main.html',
                        {
                            'list_html': list_html,
                        }
                    )
                )
            else:
                main_html = list_html
            message_code = 1
            data = {
                'main_result': main_html,
                'message_code': message_code,
            }
            return JsonResponse(data=data)
    context['search_form'] = search_form
    context['extra_title'] = 'The removed'


    return render(request, 'panel/users/user-list.html', context=context)





@login_required(login_url='base-user:callback')
@check_telegram_user
@is_admin
# @is_adminstrativ(rule_item='user-create')
def user_create(request):
    now = datetime.now()
    user = request.user
    context = base_auth(req=request)
    if user.usertype == 1 :
        usertype_choice = USERTYPES
    else:
        usertype_choice = AdminstrativUSERTYPES
    user_form = UserForm(usertype_choice,request.POST or None, request.FILES or None)
    if request.method == "POST" and request.is_ajax():
        message_code = 0
        if user_form.is_valid():
            clean_data = user_form.cleaned_data
            email = clean_data.get('email')
            username = clean_data.get('username')
            phone = clean_data.get('phone')
            usertype = clean_data.get('usertype')
            password = clean_data.get('password')
            random_string = str(random.random()).encode('utf8')
            salt = hashlib.sha1(random_string).hexdigest()[:5]
            password = make_password(str(password).strip(), salt=salt)

            user_obj = GUser(
                             # first_name=name,
                             # last_name=surname,
                             email=email,
                             username=username,
                             phone=phone,
                             password=password,
                             usertype=usertype,
                             is_active=True,

             )
            user_obj.save()
            message_code = 1
            data = {
                'message_code':message_code,
                'message_title': _('Added successfully '),
                'message_text': '',
                'redirect_url': reverse('panel:user-list', kwargs={}),
            }
        else:
            message_title = _('Düzgün məlumat daxil edin')
            message_text = "{}".format(str(user_form.errors))
            data = {
                'message_code': message_code,
                'message_title': message_title,
                'message_text': message_text,
            }
        return JsonResponse(data=data)

    context['form'] = user_form
    response = render(request, 'panel/users/user-form.html', context=context)
    return response



@login_required(login_url='base-user:callback')
@check_telegram_user
@is_admin
# @is_adminstrativ(rule_item='user-edit')
def user_edit(request,id):
    user = request.user
    context = base_auth(req=request)
    if user.usertype == 1:
        user_obj = get_object_or_404(GUser,id=id)
        usertype_choice = USERTYPES
    else:
        user_obj = get_object_or_404(GUser,id=id,usertype=2)
        usertype_choice = AdminstrativUSERTYPES
    inital_data = {
        'name':user_obj.first_name,
        'surname':user_obj.last_name,
        'email':user_obj.email,
        'username':user_obj.username,
        'usertype':user_obj.usertype,
        'phone':user_obj.phone,
    }
    user_form = UserEditForm(id,usertype_choice,request.POST or None, request.FILES or None,initial=inital_data)


    if request.method == 'POST' and request.is_ajax():
        message_code = 0
        if user_form.is_valid():
            clean_data = user_form.cleaned_data
            rules = clean_data.get('rules',None)
            workdays = clean_data.get('workdays',None)
            # name = clean_data.get('name',None)
            # surname = clean_data.get('surname',None)
            email = clean_data.get('email',None)
            username = clean_data.get('username',None)
            phone = clean_data.get('phone',None)
            usertype = clean_data.get('usertype',None)
            password = clean_data.get('password')

            random_string = str(random.random()).encode('utf8')
            salt = hashlib.sha1(random_string).hexdigest()[:5]

            # user_obj.first_name=name
            # user_obj.last_name=surname
            user_obj.email=email
            user_obj.username=username
            user_obj.usertype=usertype
            user_obj.phone=phone
            if password:
                password = make_password(password, salt=salt)
                user_obj.password=password
            user_obj.save()


            message_code = 1
            data = {
                'message_code':message_code,
                'message_title': _('Added successfully'),
                'message_text': '',
                'redirect_url': reverse('panel:user-list', kwargs={}),
            }
        else:
            message_title = _('Düzgün məlumat daxil edin')
            message_text = "{}".format(str(user_form.errors))
            data = {
                'message_code': message_code,
                'message_title': message_title,
                'message_text': message_text,
            }
        return JsonResponse(data=data)
            # user_form = UserEditForm(user_id=user_obj.id)
        # else:
        #     return HttpResponse(user_form.errors)

    context['form'] = user_form
    context['data_obj'] = user_obj
    response = render(request, 'panel/users/user-form.html', context=context)
    return response






@login_required(login_url='base-user:callback')
@check_telegram_user
@is_admin
# @is_adminstrativ(rule_item='section-remove')
def user_remove(request,id):
    user = request.user
    if request.method == 'POST' and request.is_ajax():
        try:
            if user.usertype == 1:
                data_obj = get_object_or_404(GUser,id=id)
            else:
                data_obj = get_object_or_404(GUser,id=id,usertype=2)
            if data_obj.is_deleted:
                data_obj.is_active = True
                data_obj.is_deleted = False
            else:
                data_obj.is_active = False
                data_obj.is_deleted = True
            data_obj.save()
            message_code = 1
        except:
            message_code = 0
        data = {'message_code':message_code}
        return JsonResponse(data=data)
    else:
        raise Http404



# ------------------------------------------------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------------------------------------------



AvatarSearchCHOICE = (
    ('title',_('Title')),
    ('gender',_('Gender')),
    ('status',_('status')),
    ('date',_('Date')),
)


@login_required(login_url='base-user:callback')
@check_telegram_user
@is_admin
# @is_adminstrativ(rule_item='user-list')
def avatar_list(request):
    user= request.user
    context = base_auth(req=request)
    inital_data = {
        'order_type':'-',
        'field_list':'title',
    }
    search_form = GeneralSearchForm(AvatarSearchCHOICE,request.POST or None,initial=inital_data)
    if request.method=='POST' and request.is_ajax():
        if search_form.is_valid():
            cleaned_data = search_form.cleaned_data
            title = cleaned_data.get('title',None)
            gender = cleaned_data.get('gender',None)
            status = cleaned_data.get('status',None)
            field_list = cleaned_data.get('field_list',None)
            order_type = cleaned_data.get('order_type',None)

            data_list = Avatar.objects.filter(is_active=True,is_deleted=False).get().order_by('{}{}'.format(order_type,field_list))
            if title:
                data_list = data_list.filter(title__icontains=title)
            if gender:
                data_list = data_list.filter(gender=gender)

            if status:
                data_list = data_list.filter(status=status)

            try:
                page_num = int(request.POST.get('page-num', 2))
            except:
                page_num = 0
            list_html = ''
            for_i = (page_num-1)*50
            data_list = data_list[50 * (page_num - 1):50 * page_num]
            for data_list_item in data_list[50 * (page_num - 1):50 * page_num]:
                for_i += 1
                list_html = '{}{}'.format(list_html, render_to_string(
                    "panel/avatars/include/list-item.html",
                    {
                        'for_i': for_i,
                        'base_profile': request.user,
                        'data_item': data_list_item,
                    }
                ))
            if page_num < 2:
                main_html = "{}".format(
                    render_to_string(
                        'panel/avatars/include/main.html',
                        {
                            'list_html': list_html,
                        }
                    )
                )
            else:
                main_html = list_html
            message_code = 1
            data = {
                'main_result': main_html,
                'message_code': message_code,
            }
            return JsonResponse(data=data)
    context['search_form'] = search_form


    return render(request, 'panel/avatars/avatar-list.html', context=context)





@login_required(login_url='base-user:callback')
@check_telegram_user
@is_admin
# @is_adminstrativ(rule_item='user-create')
def avatar_create(request):
    now = datetime.now()
    user = request.user
    context = base_auth(req=request)

    form = AvatarModelForm(request.POST or None, request.FILES or None)
    if request.method == "POST" and request.is_ajax():
        message_code = 0
        if form.is_valid():
            form_val = form.save(commit=False)
            form_val.save()
            messages.add_message(request, messages.SUCCESS, 'Succesfully added')
            message_code = 1
            data = {
                'message_code':message_code,
                'message_title': _('Added successfully '),
                'message_text': '',
                'redirect_url': reverse('panel:avatar-list', kwargs={}),
            }
        else:

            message_title = _('Düzgün məlumat daxil edin')
            message_text = "{}".format(str(form.errors))
            data = {
                'message_code': message_code,
                'message_title': message_title,
                'message_text': message_text,
            }
        return JsonResponse(data=data)

    context['form'] = form
    response = render(request, 'panel/avatars/form.html', context=context)
    return response



@login_required(login_url='base-user:callback')
@check_telegram_user
@is_admin
# @is_adminstrativ(rule_item='user-edit')
def avatar_edit(request,id):
    user = request.user
    context = base_auth(req=request)
    data_obj = get_object_or_404(Avatar,id=id,is_deleted=False)
    if request.method == "POST" and request.is_ajax():
        form = AvatarModelForm(request.POST, request.FILES or None, instance=data_obj)
        message_code = 0
        if form.is_valid():
            form_val = form.save(commit=False)
            form_val.save()
            messages.add_message(request, messages.SUCCESS, 'Succesfully added')
            message_code = 1
            data = {
                'message_code':message_code,
                'message_title': _('Added successfully '),
                'message_text': '',
                'redirect_url': reverse('panel:avatar-list', kwargs={}),
            }
        else:

            message_title = _('Düzgün məlumat daxil edin')
            message_text = "{}".format(str(form.errors))
            data = {
                'message_code': message_code,
                'message_title': message_title,
                'message_text': message_text,
            }
        return JsonResponse(data=data)

    form = AvatarModelForm(instance=data_obj)
    context['form'] = form
    context['data_obj'] = data_obj
    response = render(request, 'panel/avatars/form.html', context=context)
    return response






@login_required(login_url='base-user:callback')
@check_telegram_user
@is_admin
# @is_adminstrativ(rule_item='section-remove')
def avatar_remove(request,id):
    user = request.user
    if request.method == 'POST' and request.is_ajax():
        try:
            if user.usertype == 1:
                data_obj = get_object_or_404(Avatar,id=id)
            else:
                data_obj = get_object_or_404(Avatar,id=id,usertype=2)
            if data_obj.is_deleted:
                data_obj.is_active = True
                data_obj.is_deleted = False
            else:
                data_obj.is_active = False
                data_obj.is_deleted = True
            data_obj.save()
            message_code = 1
        except:
            message_code = 0
        data = {'message_code':message_code}
        return JsonResponse(data=data)
    else:
        raise Http404



# ------------------------------------------------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------------------------------------------




AvatarItemSearchCHOICE = (
    ('title',_('Title')),
    ('gender',_('Gender')),
    ('status',_('status')),
    ('date',_('Date')),
)


@login_required(login_url='base-user:callback')
@check_telegram_user
@is_admin
# @is_adminstrativ(rule_item='user-list')
def avatar_item_list(request):
    user= request.user
    context = base_auth(req=request)
    inital_data = {
        'order_type':'-',
        'field_list':'title',
    }
    search_form = GeneralSearchForm(AvatarItemSearchCHOICE,request.POST or None,initial=inital_data)
    if request.method=='POST' and request.is_ajax():
        if search_form.is_valid():
            cleaned_data = search_form.cleaned_data
            title = cleaned_data.get('title',None)
            gender = cleaned_data.get('gender',None)
            status = cleaned_data.get('status',None)
            field_list = cleaned_data.get('field_list',None)
            order_type = cleaned_data.get('order_type',None)

            data_list = AvatarItem.objects.filter(is_active=True,is_deleted=False).order_by('{}{}'.format(order_type,field_list))
            if title:
                data_list = data_list.filter(title__icontains=title)
            if gender:
                data_list = data_list.filter(gender=gender)

            if status:
                data_list = data_list.filter(status=status)

            try:
                page_num = int(request.POST.get('page-num', 2))
            except:
                page_num = 0
            list_html = ''
            for_i = (page_num-1)*50
            data_list = data_list[50 * (page_num - 1):50 * page_num]
            for data_list_item in data_list[50 * (page_num - 1):50 * page_num]:
                for_i += 1
                list_html = '{}{}'.format(list_html, render_to_string(
                    "panel/avatar-items/include/list-item.html",
                    {
                        'for_i': for_i,
                        'base_profile': request.user,
                        'data_item': data_list_item,
                    }
                ))
            if page_num < 2:
                main_html = "{}".format(
                    render_to_string(
                        'panel/avatar-items/include/main.html',
                        {
                            'list_html': list_html,
                        }
                    )
                )
            else:
                main_html = list_html
            message_code = 1
            data = {
                'main_result': main_html,
                'message_code': message_code,
            }
            return JsonResponse(data=data)
    context['search_form'] = search_form


    return render(request, 'panel/avatar-items/avatar-item-list.html', context=context)





@login_required(login_url='base-user:callback')
@check_telegram_user
@is_admin
# @is_adminstrativ(rule_item='user-create')
def avatar_item_create(request):
    now = datetime.now()
    user = request.user
    context = base_auth(req=request)

    form = AvatarItemModelForm(request.POST or None, request.FILES or None)
    if request.method == "POST" and request.is_ajax():
        message_code = 0
        if form.is_valid():
            form_val = form.save(commit=False)
            form_val.save()
            messages.add_message(request, messages.SUCCESS, 'Succesfully added')
            message_code = 1
            data = {
                'message_code':message_code,
                'message_title': _('Added successfully '),
                'message_text': '',
                'redirect_url': reverse('panel:avatar-item-list', kwargs={}),
            }
        else:

            message_title = _('Düzgün məlumat daxil edin')
            message_text = "{}".format(str(form.errors))
            data = {
                'message_code': message_code,
                'message_title': message_title,
                'message_text': message_text,
            }
        return JsonResponse(data=data)

    context['form'] = form
    response = render(request, 'panel/avatar-items/form.html', context=context)
    return response



@login_required(login_url='base-user:callback')
@check_telegram_user
@is_admin
# @is_adminstrativ(rule_item='user-edit')
def avatar_item_edit(request,id):
    user = request.user
    context = base_auth(req=request)
    data_obj = get_object_or_404(AvatarItem,id=id,is_deleted=False)
    if request.method == "POST" and request.is_ajax():
        form = AvatarItemModelForm(request.POST, request.FILES or None, instance=data_obj)
        message_code = 0
        if form.is_valid():
            form_val = form.save(commit=False)
            form_val.save()
            messages.add_message(request, messages.SUCCESS, 'Succesfully added')
            message_code = 1
            data = {
                'message_code':message_code,
                'message_title': _('Added successfully '),
                'message_text': '',
                'redirect_url': reverse('panel:avatar-item-list', kwargs={}),
            }
        else:

            message_title = _('Düzgün məlumat daxil edin')
            message_text = "{}".format(str(form.errors))
            data = {
                'message_code': message_code,
                'message_title': message_title,
                'message_text': message_text,
            }
        return JsonResponse(data=data)

    form = AvatarItemModelForm(instance=data_obj)
    context['form'] = form
    context['data_obj'] = data_obj
    response = render(request, 'panel/avatar-items/form.html', context=context)
    return response






@login_required(login_url='base-user:callback')
@check_telegram_user
@is_admin
# @is_adminstrativ(rule_item='section-remove')
def avatar_item_remove(request,id):
    user = request.user
    if request.method == 'POST' and request.is_ajax():
        try:
            data_obj = get_object_or_404(AvatarItem,id=id)
            if data_obj.is_deleted:
                data_obj.is_active = True
                data_obj.is_deleted = False
            else:
                data_obj.is_active = False
                data_obj.is_deleted = True
            data_obj.save()
            message_code = 1
        except:
            message_code = 0
        data = {'message_code':message_code}
        return JsonResponse(data=data)
    else:
        raise Http404



# ------------------------------------------------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------------------------------------------------


@login_required(login_url='base-user:callback')
@check_telegram_user
@is_admin
# @is_adminstrativ(rule_item='main-settings')
def main_settings(request):
    context = base_auth(req=request)
    try:
        data_obj = MainSettings.objects.filter().order_by('-date').get()
        context = base_auth(req=request)
        if request.method == "POST":
            form = MainSettingsForm(request.POST or None,request.FILES or None, instance=data_obj)
            if form.is_valid():
                post = form.save(commit=False)
                post.title = 'Main'
                post.save()
                messages.add_message(request, messages.SUCCESS, 'Succesfully changed')

        else:
            form = MainSettingsForm(instance=data_obj)
        context['form'] = form
        context['data_obj'] = data_obj
    except:
        form = MainSettingsForm(request.POST or None,request.FILES or None)
        if request.method == 'POST':
            if form.is_valid():
                form_val = form.save(commit=False)
                form_val.save()
                messages.add_message(request, messages.SUCCESS, 'Succesfully changed')
                form = MainSettingsForm()
            # else:
            #     return HttpResponse(employee_form.errors)
        context['form'] = form

    return render(request, 'panel/general/settings/edit.html', context=context)



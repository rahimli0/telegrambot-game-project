from django.contrib import admin

# Register your models here.
from panel.models import *

admin.site.register(MainSettings)
admin.site.register(AvatarGenderType)
admin.site.register(Avatar)
admin.site.register(UserAvatar)
admin.site.register(AvatarItem)
admin.site.register(UserAvatarItem)
admin.site.register(AllUserStatus)
admin.site.register(Game)
admin.site.register(GameAction)

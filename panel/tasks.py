import json

import requests
import telebot
# Create your views here.
import telegram
from celery import shared_task
from telebot.types import ReplyKeyboardMarkup, KeyboardButton, CallbackQuery
from telegram import ParseMode

from bot_app.general.utils import GenderChoices, AvatarPaidFreeChoices

from tbgm.settings import TELEGRAM_BOT_TOKEN

bot = telebot.TeleBot(TELEGRAM_BOT_TOKEN)

from telebot import types

import datetime

from celery import shared_task


# @app.task


@shared_task
def package_product_task_general(gender,paidFree):
    from panel.models import AvatarGenderType, Avatar
    from bot_app.functions import make_avatar_total_images_name
    obj, created = AvatarGenderType.objects.get_or_create(gender=gender,paid_status=paidFree)
    data_list = Avatar.objects.filter(is_active=True, is_deleted=False,gender=gender,paid_status=paidFree).order_by('status')
    list_data = []
    if data_list:
        for item in data_list:
            list_data.append((item.title, str(item.photo)))
        obj.photo.name = "{}".format(make_avatar_total_images_name(list_data))
        obj.save()
        code = 200
    return True


def confirm_request_dailycheck():
    pass

@shared_task
def calculated_point(user_id,id):
    # from django.contrib.auth import get_user_model
    # GUser = get_user_model()
    # from panel.models import UserPoint
    # try:
    #     user_item  = GUser.objects.get(id=user_id)
    #     points = UserPoint.objects.filter(user_id=user_id).exclude(id=id)
    #     user_item.point = points.extra({'get_my_point': 'sum(point)'}).values('get_my_point')[0]['get_my_point'] if points else 0.0
    #     user_item.save()
    # except:
    #     pass
    return True

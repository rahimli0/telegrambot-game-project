from django.conf.urls import url

from .views import *

app_name = 'userprofile'
urlpatterns = [
	url(r'^$', index, name='index'),


	url(r'^user/list/$', user_list, name='user-list'),
	url(r'^user/list/removed/$', user_list_removed, name='user-list-removed'),
	url(r'^user/create/', user_create, name='user-create'),
	url(r'^user/edit/(?P<id>[0-9]+)/', user_edit, name='user-edit'),
	url(r'^user/remove/(?P<id>[0-9]+)/', user_remove, name='user-remove'),

	url(r'^avatar/list/$', avatar_list, name='avatar-list'),
	url(r'^avatar/create/', avatar_create, name='avatar-create'),
	url(r'^avatar/edit/(?P<id>[0-9]+)/', avatar_edit, name='avatar-edit'),
	url(r'^avatar/remove/(?P<id>[0-9]+)/', avatar_remove, name='avatar-remove'),

	url(r'^avatar-item/list/$', avatar_item_list, name='avatar-item-list'),
	url(r'^avatar-item/create/', avatar_item_create, name='avatar-item-create'),
	url(r'^avatar-item/edit/(?P<id>[0-9]+)/', avatar_item_edit, name='avatar-item-edit'),
	url(r'^avatar-item/remove/(?P<id>[0-9]+)/', avatar_item_remove, name='avatar-item-remove'),


	url(r'^test/$', test, name='test'),
	url(r'^main/settings/$', main_settings, name='main-settings'),


]

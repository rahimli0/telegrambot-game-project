import _thread
import uuid

import telebot
from django.conf import settings
from django.db import models
from django.db.models import signals
from django.utils.translation import ugettext as _

# Create your models here.
from telebot import types

from bot_app.functions import *
from bot_app.general.functions import delete_message_one_message
from bot_app.general.utils import *
from general.functions import path_and_rename, UploadToPath
from panel.utils import utils
from panel.tasks import calculated_point
from panel.utils.managers import GameManager
from tbgm.settings import TELEGRAM_BOT_NAME, TELEGRAM_BOT_TOKEN
from bot_app.tasks import *


class AllUserStatus(models.Model):
    chat_id = models.CharField(verbose_name=_('Telegram id'), max_length=255,unique=True)
    referal_game = models.CharField(verbose_name="Referal game", default='-',max_length=255,)
    character_name = models.CharField(verbose_name="Character name",max_length=255, blank=True,null=True)
    phone_number = models.CharField(verbose_name="Phone",max_length=255, blank=True,null=True)
    gender = models.CharField(verbose_name="Gender", max_length=255, blank=True,null=True)
    avatar = models.ForeignKey('Avatar', blank=True,null=True)
    next_step_status = models.CharField(verbose_name="Next step status", default='-',max_length=255,)
    deleted_last_message_id = models.IntegerField(verbose_name="Deleted last message", default=0,)

    strength = models.IntegerField(verbose_name="Strength", default=0)
    quickness = models.IntegerField(verbose_name="Quickness", default=0)
    intuition = models.IntegerField(verbose_name="Iintuition", default=0)
    durability = models.IntegerField(verbose_name="Durability", default=0)

    date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    def __str__(self):
        return self.chat_id
    def get_total_point(self):
        mainSettings = MainSettings.objects.filter().first()
        my_points = self.strength + self.quickness + self.intuition + self.durability
        total_points = mainSettings.limit_point - my_points if mainSettings and mainSettings.limit_point > 0 else TOTALUSERGAMEPOINT
        return total_points

class AvatarGenderType(models.Model):
    gender = models.CharField(max_length=255,choices=GenderChoices)
    paid_status = models.CharField(max_length=255,choices=AvatarPaidFreeChoices)
    photo = models.ImageField(upload_to=UploadToPath('photos/avatar/all/%Y/%m/%d'), max_length=255,blank=True,null=True)
    date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)


class Avatar(models.Model):
    title = models.CharField(max_length=255,)
    gender = models.CharField(max_length=255,choices=GenderChoices)
    photo = models.ImageField(upload_to=UploadToPath('photos/avatar/all/%Y/%m/%d'), max_length=255)
    photo_url = models.CharField( max_length=500, blank=True,null=True)
    status = models.CharField(max_length=255,choices=AvatarStatusChoices)
    paid_status = models.CharField(max_length=255,choices=AvatarPaidFreeChoices,default=AvatarPaidFreeChoices[0][0])
    is_active = models.BooleanField(_('active'), default=True,help_text=_('Designates whether this user should be treated as ''active. Unselect this instead of deleting accounts.'))
    is_deleted = models.BooleanField(_('deleted'), default=False,help_text=_('Designates whether this user should be treated as ''active. Unselect this instead of deleting accounts.'))
    date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    __original_gender = None
    __original_paid_status = None
    def __str__(self):
        return self.title
    def save(self, force_insert=False, force_update=False, *args, **kwargs):
        super(Avatar, self).save(*args, **kwargs)
        if self.photo:
            self.photo_url = str(self.photo)
        if self.gender != self.__original_gender or self.paid_status != self.__original_paid_status:
            from panel.tasks import package_product_task_general
            package_product_task_general.delay(self.gender,self.paid_status)
            package_product_task_general.delay(self.__original_gender,self.__original_paid_status)
        super(Avatar, self).save(force_insert, force_update, *args, **kwargs)
        self.__original_gender = self.gender
        self.__original_paid_status = self.paid_status


class GameAction(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,related_name='user')
    game = models.ForeignKey('Game',related_name='game')
    round = models.PositiveIntegerField(verbose_name="Round",default=1)
    point = models.IntegerField(verbose_name="Point",default=0)
    attack = models.CharField(max_length=255,choices=AtackChoices,blank=True,null=True)
    attack2 = models.CharField(max_length=255,choices=AtackChoices,blank=True,null=True)
    deffence = models.CharField(max_length=255,choices=DeffenceChoices,blank=True,null=True)
    deffence2 = models.CharField(max_length=255,choices=DeffenceChoices,blank=True,null=True)
    ready = models.BooleanField(default=False)
    passed = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    def __str__(self):
        return "{} - {}".format(str(self.game.u_id),str(self.round))
    # class Meta:
    #     unique_together = ['user', 'game', 'round']
    def get_result(self):
        result_list = []
        atack_list = []
        deffence_list = []
        if self.attack:
            atack_list.append(self.attack)
            result_list.append(self.attack)
        if self.attack2:
            atack_list.append(self.attack2)
            result_list.append(self.attack2)
        if self.deffence:
            deffence_list.append(self.deffence)
            result_list.append(self.deffence)
        if self.deffence2:
            deffence_list.append(self.deffence2)
            result_list.append(self.deffence2)
        return [result_list,atack_list,deffence_list]
    def get_result_formated(self):
        result_list = []
        atack_list = []
        deffence_list = []
        atack_dict = {
            "Head":0,
            "Body":0,
            "Leg":0,
        }
        deffence_dict = {
            "Head":0,
            "Body":0,
            "Leg":0,
        }
        if self.attack:
            atack_dict["{}".format(str(self.attack).replace("hit:",""))] = self.user.strength
            atack_list.append(str(self.attack).replace("hit:",""))
            result_list.append(str(self.attack).replace("hit:",""))
        if self.attack2:
            atack_dict["{}".format(str(self.attack2).replace("hit:",""))] = self.user.strength
            atack_list.append(str(self.attack2).replace("hit:",""))
            result_list.append(str(self.attack2).replace("hit:",""))
        if self.deffence:
            deffence_dict["{}".format(str(self.deffence).replace("block:",""))] = self.user.durability
            deffence_list.append(str(self.deffence).replace("block:",""))
            result_list.append(str(self.deffence).replace("block:",""))
        if self.deffence2:
            deffence_dict["{}".format(str(self.deffence2).replace("block:",""))] = self.user.durability
            deffence_list.append(str(self.deffence2).replace("block:",""))
            result_list.append(str(self.deffence2).replace("block:",""))
        return [result_list,atack_list,deffence_list,atack_dict,deffence_dict]
    def save(self, force_insert=False, force_update=False, *args, **kwargs):
        super(GameAction, self).save(*args, **kwargs)
        act_list = []
        if self.attack:
            act_list.append(self.attack)
        if self.attack2:
            act_list.append(self.attack2)
        if self.deffence:
            act_list.append(self.deffence)
        if self.deffence2:
            act_list.append(self.deffence2)
        if len(act_list)>=3:
            self.ready = True
        # if self.active_avatar_photo != self.__original_active_avatar_photo:
        #     make_avatar(self.pk)
            # make_avatar.delay(self.pk)
        # super(GameAction, self).save(force_insert, force_update, *args, **kwargs)



class Game(models.Model):
    u_id = models.UUIDField(_('U id'), unique=True, default=uuid.uuid4)
    user1 = models.ForeignKey(settings.AUTH_USER_MODEL,related_name='user1',blank=True,null=True)
    user2 = models.ForeignKey(settings.AUTH_USER_MODEL,related_name='user2',blank=True,null=True)
    private_number = models.CharField(max_length=255,blank=True,null=True)
    user_message1 = models.IntegerField(blank=True,null=True)
    user_message2 = models.IntegerField(blank=True,null=True)
    win_user = models.ForeignKey(settings.AUTH_USER_MODEL,related_name='win_user',blank=True,null=True)
    round = models.PositiveIntegerField(verbose_name="Round",default=0)
    result = models.PositiveIntegerField(verbose_name="Result",default=0)
    status = models.CharField(max_length=255,choices=GameStatusChoices,default='Waiting')
    type = models.CharField(max_length=255,choices=GameTypeChoices,default='Public')
    game_result_photo = models.ImageField(verbose_name=_('Avatar result photo'), max_length=500, blank=True,null=True)
    error_messages = models.TextField(blank=True,null=True)
    date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    objects = GameManager()
    __original_user1 = None
    __original_user2 = None
    __original_round = None
    __original_status = None
    def __str__(self):
        return str(self.u_id)
    def get_absolute_url(self):
        return 'https://t.me/{}?startgame={}'.format(TELEGRAM_BOT_NAME,self.u_id)

    def get_points(self,u_id):
        gas_obj = GameAction.objects.filter(game=self,round__lt=self.round,user_id=u_id)
        return_val = TOTALUSERGAMESTARTPOINT
        try:
            if gas_obj:
                extra = gas_obj.extra(select={'total_point': 'sum(point)', }).values('total_point')[0]
                total_point = int(extra['total_point'])
                return_val -= total_point
        except:
            pass
        return return_val
    def get_points_lte(self,u_id):
        gas_obj = GameAction.objects.filter(game=self,round__lte=self.round,user_id=u_id)
        return_val = TOTALUSERGAMESTARTPOINT
        try:
            if gas_obj:
                extra = gas_obj.extra(select={'total_point': 'sum(point)', }).values('total_point')[0]
                total_point = int(extra['total_point'])
                return_val -= total_point
        except:
            pass
        return return_val

    def __init__(self, *args, **kwargs):
        super(Game, self).__init__(*args, **kwargs)
        self.__original_user1 = self.user1
        self.__original_user2 = self.user2
        self.__original_round = self.round
        self.__original_status = self.status
    def save(self, force_insert=False, force_update=False, *args, **kwargs):
        super(Game, self).save(*args, **kwargs)
        # if self.user1 != self.__original_user1 or self.user2 != self.__original_user2:
        #     photo_url  = make_game_image(str(self.user1.avatar_result_photo) if self.user1 and self.user1.avatar_result_photo else '',str(self.user2.avatar_result_photo) if self.user2 and self.user2.avatar_result_photo else '','static/versus-back.png')
        #     if photo_url:
        #         self.game_result_photo.name = photo_url
        # if self.id and self.status != self.__original_status and self.status == GameStatusChoices[1][0]:
        #     bot = telebot.TeleBot(TELEGRAM_BOT_TOKEN)
        #     if self.user1 and self.user2:
        #         domain_name = 'https://www.book-plays.com'
        #         user1_message = bot.send_photo(chat_id=self.user1.m_id,caption="self.game_result_photo.name",photo="{}{}".format(domain_name,self.game_result_photo.url))
        #         user2_message = bot.send_photo(chat_id=self.user2.m_id,caption="self.game_result_photo.name",photo="{}{}".format(domain_name,self.game_result_photo.url))
        #         self.user_message1 = user1_message.message_id
        #         self.user_message2 = user2_message.message_id
        if self.id and self.round != self.__original_round and self.status == GameStatusChoices[1][0] and (self.get_points_lte(self.user1_id) > 0 and self.get_points_lte(self.user2_id) > 0):
            from bot_app.tasks import game_action_main_task
            game_action_main_task.delay(self.id)
        super(Game, self).save(force_insert, force_update, *args, **kwargs)
        self.__original_user1 = self.user1
        self.__original_user2 = self.user2
        self.__original_round = self.round
        self.__original_status = self.status
def game_save(sender, instance, created, *args, **kwargs):
    # pass
    # make_avatar.delay(instance.id)

    # if created:
    #     photo_url  = make_game_image(str(instance.user1.avatar_result_photo) if instance.user1 and instance.user1.avatar_result_photo else '',str(instance.user2.avatar_result_photo) if instance.user2 and instance.user2.avatar_result_photo else '','static/versus-back.png')
    #     if photo_url:
    #         instance.game_result_photo.name = photo_url
    #         instance.save()
    if created:
        if instance.type == GameTypeChoices[1][0]:
            if instance.user2:
                text = "Hi {}, {} wants to play a game with you".format(instance.user1.get_full_name(),
                                                                             instance.user2.get_full_name())
                # text = "{} -> <a href='{}'>{}</a>".format(link_text, 'https://t.me/{}?start={}'.format(TELEGRAM_BOT_NAME,
                #                                                                                    instance.u_id),
                #                                       'Click here')
                markup = types.InlineKeyboardMarkup()
                markup.row_width = 1
                markup.add(
                    types.InlineKeyboardButton("{}".format(PrivateGameAcceptRejectControllerHandler[0][1]), callback_data="{}".format(PrivateGameAcceptRejectControllerHandler[0][0])),
                    types.InlineKeyboardButton("{}".format(PrivateGameAcceptRejectControllerHandler[1][1]), callback_data="{}".format(PrivateGameAcceptRejectControllerHandler[1][0])),
                )
                bot = telebot.TeleBot(TELEGRAM_BOT_TOKEN)
                try:
                    if instance.user2 and instance.user2.avatar_result_photo_url:
                        bot.send_photo(chat_id=instance.user2.m_id,caption=text,photo="http://game.minbashi.com/media/{}".format(instance.user2.avatar_result_photo_url),reply_markup=markup)
                    else:
                        bot.send_message(chat_id=instance.user2.m_id,text=text,reply_markup=markup)
                except:
                    bot.send_message(chat_id=instance.user2.m_id, text=text,reply_markup=markup)

                if instance.user2 and instance.user2.main_message_id:
                    _thread.start_new_thread(delete_message_one_message, (instance.user2.m_id, instance.user2.main_message_id))
    if created is False and instance.status == GameStatusChoices[2][0]:
        from bot_app.general.widgets import GameEditModel
        geModel = GameEditModel(g_id=instance.pk, u1_id=instance.user1.m_id, u2_id=instance.user2.m_id)
        # geModel.get_game_finish_game()
        geModel.finished_game_message()
    if created and instance.type == GameTypeChoices[0][0]:
        from bot_app.tasks import channel_message
        channel_message.delay('https://t.me/{}?start={}'.format(TELEGRAM_BOT_NAME,instance.u_id))
    #     # print(signal)
    #     # Send verification email
    #     confirm_obj = UserConfrimationKeys(key=uuid.uuid4(),user=instance,expired_date=datetime.now()+timedelta(days=7),expired=False)
    #     confirm_obj.save()
    #     try:
    #         send_html_mail(instance.email,_("Confirm email"),'<a href="{}">{}</a>'.format(reverse('base-user:confirm-account',kwargs={'username':instance.username,'key':confirm_obj.key}),_('Confirm your account,click here')))
    #     except:
    #         pass

signals.post_save.connect(game_save, sender=Game)

#
# class StartGame(models.Model):
#     game = models.ForeignKey('Game',related_name='game_start',unique=True)
#     status1 = models.CharField(max_length=255,choices=GameStatusChoices,default='Waiting')
#     status2 = models.CharField(max_length=255,choices=GameStartStatusChoices,default='Waiting')
#     date = models.DateTimeField(auto_now_add=True)
#     updated_date = models.DateTimeField(auto_now=True)
#     __original_status1 = None
#     __original_status2 = None
#     def __init__(self, *args, **kwargs):
#         super(StartGame, self).__init__(*args, **kwargs)
#         self.__original_status1 = self.status1
#         self.__original_status2 = self.status2
#     def save(self, force_insert=False, force_update=False, *args, **kwargs):
#         super(StartGame, self).save(*args, **kwargs)
#         super(StartGame, self).save(force_insert, force_update, *args, **kwargs)
#         self.__original_status1 = self.status1
#         self.__original_status2 = self.status2


#
# class GameLog(models.Model):
#     game = models.ForeignKey('Game',related_name='game')
#     user = models.ForeignKey(settings.AUTH_USER_MODEL,related_name='user')
#     date = models.DateTimeField(auto_now_add=True)


class AvatarItem(models.Model):
    title = models.CharField(max_length=255,)
    gender = models.CharField(max_length=255,choices=GenderChoices)
    photo = models.ImageField(upload_to=UploadToPath('photos/avatar/all/%Y/%m/%d'), max_length=255)
    status = models.CharField(max_length=255,choices=AvatarStatusChoices)
    photo_url = models.CharField( max_length=500, blank=True,null=True)
    x_coordinate = models.IntegerField(default=0)
    y_coordinate = models.IntegerField(default=0)
    type = models.CharField(max_length=255,choices=AvatarItemTypeChoices)
    is_active = models.BooleanField(_('active'), default=True,help_text=_('Designates whether this user should be treated as ''active. Unselect this instead of deleting accounts.'))
    is_deleted = models.BooleanField(_('deleted'), default=False,help_text=_('Designates whether this user should be treated as ''active. Unselect this instead of deleting accounts.'))
    date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    def __str__(self):
        return self.title
    def save(self, force_insert=False, force_update=False, *args, **kwargs):
        super(AvatarItem, self).save(*args, **kwargs)
        if self.photo:
            self.photo_url = str(self.photo)
        super(AvatarItem, self).save(force_insert, force_update, *args, **kwargs)



class UserAvatar(models.Model):
    user = models.ForeignKey( settings.AUTH_USER_MODEL)
    avatar = models.ForeignKey('Avatar')
    status = models.CharField(max_length=255,choices=UserAvatarStatusChoices)
    date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    def __str__(self):
        return "{} - {}".format(self.user.m_id,self.avatar.title)



class UserAvatarItem(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    avatar_item = models.ForeignKey('AvatarItem')
    status = models.CharField(max_length=255,choices=UserAvatarStatusChoices)
    date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    def __str__(self):
        return "{} - {}".format(self.user.m_id,self.avatar_item.title)



class MainSettings(models.Model):
    title = models.CharField(max_length=255)
    delay_days = models.IntegerField(default=0)
    limit_point = models.IntegerField(verbose_name=_('Limit point'),default=0)
    big_logo = models.ImageField(upload_to=path_and_rename, null=True, blank=True)
    small_logo = models.ImageField(upload_to=path_and_rename, null=True, blank=True)
    date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    def __str__(self):
        return self.title
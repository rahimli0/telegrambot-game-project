from django import forms
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext as _

from base_user.tools.common import USERTYPES
from panel.models import *

GUser = get_user_model()

class EmployeeModelForm(forms.ModelForm):
    class Meta:
        model = GUser
        fields = ('first_name', 'last_name', 'email', 'phone',)
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control', }),
            'last_name': forms.TextInput(attrs={'class': 'form-control', }),
            'phone': forms.TextInput(attrs={'class': 'form-control', }),
            'email': forms.EmailInput(attrs={'class': 'form-control', }),
        }






ORDERTYPECHOICE  = (
    ('-', _('Desc')),
    ('',_('Asc')),
)
FIELDLISTCHOICE  = (
    ('date',_('Date')),
    ('title',_('Title')),
    ('short_text',_('Short text')),
)

class MainSearchForm(forms.Form):
    search = forms.CharField(max_length=255,label=_('Search'), required=False, widget=forms.TextInput(attrs={'placeholder': _('Search'), 'autocomplete': 'off', 'class': 'form-control', }))
    field_list = forms.ChoiceField(choices=FIELDLISTCHOICE,label=_('Field'),  required=True, widget=forms.Select(attrs={'placeholder': _('Field'), 'autocomplete': 'off','class': 'form-control', }))
    order_type = forms.ChoiceField(choices=ORDERTYPECHOICE ,label=_('Order'),  required=False, widget=forms.Select(attrs={'placeholder': _('Order'), 'autocomplete': 'off','class': 'form-control', }))





class TaskSearchForm(forms.Form):
    search = forms.CharField(max_length=255,label=_('Search'), required=False, widget=forms.TextInput(
        attrs={'placeholder': _('Search'), 'autocomplete': 'off',
               'class': 'form-control', }))
    field_list = forms.ChoiceField(choices=[],label=_('Field'),  required=True, widget=forms.Select(attrs={'placeholder': _('Field'), 'autocomplete': 'off','class': 'form-control', }))
    order_type = forms.ChoiceField(choices=ORDERTYPECHOICE ,label=_('Order'),  required=False, widget=forms.Select(attrs={'placeholder': _('Order'), 'autocomplete': 'off','class': 'form-control', }))

    first_date = forms.DateField(input_formats=('%d.%m.%Y',),label=_('First date'), required=False,widget=forms.DateInput(format='%m/%d/%Y',attrs={'placeholder': _('First date'), 'autocomplete': 'off','class': 'form-control', }))
    end_date = forms.DateField(input_formats=('%d.%m.%Y',),label=_('End date'), required=False,widget=forms.DateInput(format='%m/%d/%Y',attrs={'placeholder': _('End date'), 'autocomplete': 'off','class': 'form-control', }))
    def __init__(self, field_list_choice, *args, **kwargs):
        super(TaskSearchForm, self).__init__(*args, **kwargs)

        self.fields['field_list'].choices = field_list_choice







class DailyCheckSearchForm(forms.Form):
    search = forms.CharField(max_length=255,label=_('Search'), required=False, widget=forms.TextInput(
        attrs={'placeholder': _('Search'), 'autocomplete': 'off',
               'class': 'form-control', }))
    field_list = forms.ChoiceField(choices=[],label=_('Field'),  required=True, widget=forms.Select(attrs={'placeholder': _('Field'), 'autocomplete': 'off','class': 'form-control', }))
    order_type = forms.ChoiceField(choices=ORDERTYPECHOICE ,label=_('Order'),  required=False, widget=forms.Select(attrs={'placeholder': _('Order'), 'autocomplete': 'off','class': 'form-control', }))

    first_date = forms.DateField(input_formats=('%d.%m.%Y',),label=_('First date'), required=False,widget=forms.DateInput(format='%m/%d/%Y',attrs={'placeholder': _('First date'), 'autocomplete': 'off','class': 'form-control', }))
    end_date = forms.DateField(input_formats=('%d.%m.%Y',),label=_('End date'), required=False,widget=forms.DateInput(format='%m/%d/%Y',attrs={'placeholder': _('End date'), 'autocomplete': 'off','class': 'form-control', }))
    def __init__(self, field_list_choice, *args, **kwargs):
        super(DailyCheckSearchForm, self).__init__(*args, **kwargs)

        self.fields['field_list'].choices = field_list_choice






class GeneralSearchForm(forms.Form):
    search = forms.CharField(max_length=255,label=_('Search'), required=False, widget=forms.TextInput(
        attrs={'placeholder': _('Search'), 'autocomplete': 'off',
               'class': 'form-control', }))
    field_list = forms.ChoiceField(choices=[],label=_('Field'),  required=True, widget=forms.Select(attrs={'placeholder': _('Field'), 'autocomplete': 'off','class': 'form-control', }))
    order_type = forms.ChoiceField(choices=ORDERTYPECHOICE ,label=_('Order'),  required=False, widget=forms.Select(attrs={'placeholder': _('Order'), 'autocomplete': 'off','class': 'form-control', }))
    def __init__(self, field_list_choice, *args, **kwargs):
        super(GeneralSearchForm, self).__init__(*args, **kwargs)

        self.fields['field_list'].choices = field_list_choice




class UserForm(forms.Form):
    name = forms.CharField(required=False,max_length=254, label="Name", error_messages={})
    surname = forms.CharField(required=False,max_length=254, label="Surname", error_messages={},widget=forms.TextInput(attrs={'placeholder': _("Soyad"),'class':'form-control','readonly':True}))
    email = forms.EmailField(required=True,error_messages={},label=_('Email'))
    username = forms.CharField(required=True,error_messages={},label=_('Username'),widget=forms.TextInput(attrs={'placeholder': _("Username"),'class':'form-control'}))
    usertype = forms.ChoiceField(choices=[],required=True,error_messages={},label=_('Type'),widget=forms.Select(attrs={'placeholder': _("Type"),'class':'form-control js-example-basic-single w-100'}))
    phone = forms.CharField(required=False,error_messages={},label=_('Phone'))
    password = forms.CharField(required=True, min_length=6,label=_("Password"))
    retype_password = forms.CharField(required=True,min_length=6,label=_("Password type"))


    def __init__(self,usertype_choice, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)

        self.fields['usertype'].choices = usertype_choice
        self.fields['name'].widget = forms.TextInput(attrs={'placeholder': _("Name"),'class':'form-control','readonly':True})
        self.fields['username'].widget = forms.TextInput(attrs={'placeholder': _("Surname"),'class':'form-control'})
        self.fields['email'].widget = forms.EmailInput(attrs={'placeholder': _("Email"),'class':'form-control'})
        self.fields['phone'].widget = forms.TextInput(attrs={'placeholder': _("Phone"),'class':'form-control'})
        self.fields['password'].widget = forms.PasswordInput(attrs={'placeholder': _("Password"),'class':'form-control'})
        self.fields['retype_password'].widget = forms.PasswordInput(attrs={'placeholder': _("Password confirm"),'class':'form-control'})

    def clean(self):
        cleaned_data = super(UserForm, self).clean()

        email = cleaned_data.get('email')
        username = cleaned_data.get('username')
        password = cleaned_data.get('password')
        password_confirm = cleaned_data.get('retype_password')
        user_email_obj = GUser.objects.filter(email=email)
        user_username_obj = GUser.objects.filter(username=username)
        if user_email_obj:
            self._errors['email'] = _('Email is allready use')
        if user_username_obj:
            self._errors['username'] = _('Username is allready use')
        if password and password_confirm:
            if password != password_confirm:
                raise forms.ValidationError(_("Passwords not same"))
        return cleaned_data





class UserEditForm(forms.Form):
    name = forms.CharField(required=False,max_length=254, label="Name", error_messages={})
    surname = forms.CharField(required=False,max_length=254, label="Surname", error_messages={})
    email = forms.EmailField(required=True,error_messages={},label=_('Email'))
    username = forms.CharField(required=True,error_messages={},label=_('Username'),widget= forms.TextInput(attrs={'placeholder': _("Username"),'class':'form-control'}))
    usertype = forms.ChoiceField(choices=USERTYPES,required=True,error_messages={},label=_('Type'),widget=forms.Select(attrs={'placeholder': _("Type"),'class':'form-control js-example-basic-single w-100'}))
    phone = forms.CharField(required=False,error_messages={},label=_('Number'))
    password = forms.CharField(required=False,label=_("Password"))
    retype_password = forms.CharField(required=False,label=_("Password confirm"))

    user_id = 0

    def __init__(self,user_id,usertype_choice, *args, **kwargs):
        super(UserEditForm, self).__init__(*args, **kwargs)
        self.user_id = user_id
        self.fields['usertype'].choices = usertype_choice
        self.fields['name'].widget = forms.TextInput(attrs={'placeholder': _("Name"),'class':'form-control','readonly':True})
        self.fields['surname'].widget = forms.TextInput(attrs={'placeholder': _("Surname"),'class':'form-control','readonly':True})
        self.fields['email'].widget = forms.EmailInput(attrs={'placeholder': _("Email"),'class':'form-control'})
        self.fields['phone'].widget = forms.TextInput(attrs={'placeholder': _("Phone"),'class':'form-control'})
        self.fields['password'].widget = forms.PasswordInput(attrs={'placeholder': _("Password"),'class':'form-control'})
        self.fields['retype_password'].widget = forms.PasswordInput(attrs={'placeholder': _("Password confirm"),'class':'form-control'})


    def clean(self):
        cleaned_data = super(UserEditForm, self).clean()

        email = cleaned_data.get('email')
        username = cleaned_data.get('username')
        password = cleaned_data.get('password')
        password_confirm = cleaned_data.get('retype_password')

        user_email_obj = GUser.objects.filter(email=email).exclude(id=self.user_id)
        user_username_obj = GUser.objects.filter(username=username).exclude(id=self.user_id)
        if user_email_obj:
            self._errors['email'] = _('Email is allready use')
        if user_username_obj:
            self._errors['username'] = _('Username is allready use')
        if password:
            if password and password_confirm:
                if password != password_confirm:
                    raise forms.ValidationError(_("Passwords not same"))
        return cleaned_data


class MainSettingsForm(forms.ModelForm):

    class Meta:
        model = MainSettings
        fields = ('limit_point','big_logo','small_logo',)
        widgets = {
            # 'title': forms.TextInput(attrs={'class': 'form-control', }),
            'limit_point': forms.NumberInput(attrs={'class': 'form-control', }),
        }


class AvatarModelForm(forms.ModelForm):
    class Meta:
        model = Avatar
        fields = ('title', 'gender', 'status', 'photo', )
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control', }),
            'gender': forms.Select(attrs={'class': 'form-control', }),
            'status': forms.Select(attrs={'class': 'form-control', }),
        }


class AvatarItemModelForm(forms.ModelForm):
    class Meta:
        model = AvatarItem
        fields = ('title', 'gender', 'status', 'photo', 'x_coordinate', 'y_coordinate', )
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control', }),
            'gender': forms.Select(attrs={'class': 'form-control', }),
            'status': forms.Select(attrs={'class': 'form-control', }),
            'x_coordinate': forms.TextInput(attrs={'class': 'form-control', }),
            'y_coordinate': forms.TextInput(attrs={'class': 'form-control', }),
        }

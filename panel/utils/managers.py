
from django.db import models
from django.db.models import Q


class GameManager(models.Manager):
    def by_user(self, user):
        qlookup = Q(user1=user) | Q(user2=user)
        qlookup2 = Q(user1=user) & Q(user2=user)
        qs = self.get_queryset().filter(qlookup).exclude(qlookup2).distinct()
        return qs

    def get_or_new(self, user, other_uuid_id):  # get_or_create
        uuid_id = user.uuid_id
        if uuid_id == other_uuid_id:
            return None
        qlookup1 = Q(user1__uuid_id=uuid_id) & Q(user2__uuid_id=other_uuid_id)
        qlookup2 = Q(user1__uuid_id=other_uuid_id) & Q(user2__uuid_id=uuid_id)
        qs = self.get_queryset().filter(qlookup1 | qlookup2).distinct()
        if qs.count() == 1:
            return qs.user1(), False
        elif qs.count() > 1:
            return qs.order_by('timestamp').user1(), False
        else:
            Klass = user.__class__
            user2 = Klass.objects.get(uuid_id=other_uuid_id)
            if user != user2:
                obj = self.model(
                    user1=user,
                    user2=user2
                )
                obj.save()
                return obj, True
            return None, False

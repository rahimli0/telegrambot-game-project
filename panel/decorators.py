from django.conf import settings
from django.contrib.auth import get_user_model, logout
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.urls import reverse

# from panel.models import RuleUserModel

GUser = get_user_model()
bot_name = settings.TELEGRAM_BOT_NAME
bot_token = settings.TELEGRAM_BOT_TOKEN
redirect_url = settings.TELEGRAM_LOGIN_REDIRECT_URL

from django_telegram_login.authentication import verify_telegram_authentication
from django_telegram_login.errors import (
    NotTelegramDataError,
    TelegramDataIsOutdatedError,
)
from functools import wraps

def check_telegram_user(function):
    def wrap(request, *args, **kwargs):
        user = request.user
        if not user.hash:
            logout(request)
            return HttpResponseRedirect(redirect_to=reverse('base-user:callback'))

        # try:
        request_data = {}
        # request.GET.pop(user.hash)
        # request.GET.pop(user.auth_date)
        # request.GET.pop(user.m_id)
        # remember old state
        # _mutable = request.GET._mutable

        # set to mutable
        # request.GET._mutable = True

        # сhange the values you want
        request_data['hash'] = user.hash
        request_data['auth_date'] = user.auth_date
        request_data['id'] = user.m_id
        if user.first_name:
            request_data['first_name'] = user.first_name
        if user.photo_url:
            request_data['photo_url'] = user.photo_url
        if user.last_name:
            request_data['last_name'] = user.last_name
        request_data['username'] = user.username

        # set mutable flag back
        # request.GET._mutable = _mutable
        # return HttpResponse("{} {} {}".format(user.m_id,user.auth_date,user.hash))
        try:
            result = verify_telegram_authentication(bot_token=bot_token, request_data=request_data)

        except TelegramDataIsOutdatedError:
            logout(request)
            return HttpResponseRedirect(redirect_to=reverse('base-user:callback'))

        except NotTelegramDataError:
            logout(request)
            return HttpResponseRedirect(redirect_to=reverse('base-user:callback'))
        return function(request, *args, **kwargs)
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


def is_admin(function):
    def wrap(request, *args, **kwargs):
        user = request.user
        if user.usertype != 1:
            raise Http404

        # logout(request)
        # return HttpResponseRedirect(redirect_to=reverse('base-user:callback'))
        return function(request, *args, **kwargs)
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap

decorator_with_args = lambda d: lambda *args, **kwargs: lambda func: wraps(func)(d(func, *args, **kwargs))
@decorator_with_args
def is_adminstrativ(function=None,rule_item=None):
    def wrap(request, *args, **kwargs):
        user = request.user
        if user.usertype  not in [1,4]:
            raise Http404
        else:
            if user.usertype == 1 or user.usertype == 4 and rule_item in user.get_user_rules():
                pass
            else:
                raise Http404
        return function(request, *args, **kwargs)
    # wrap.__doc__ = function.__doc__
    # wrap.__name__ = function.__name__
    return wrap


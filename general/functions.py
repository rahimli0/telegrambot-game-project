from django.conf import settings
from celery import shared_task
from django.contrib.admin.widgets import AdminFileWidget
from django.core.files.storage import default_storage
from django.utils import timezone
import os
from uuid import uuid4

from django.utils.datetime_safe import datetime as datetime_util
from django.utils.safestring import mark_safe
import datetime

import datetime
import os
import unicodedata

from django.core.files.storage import default_storage
from django.utils.deconstruct import deconstructible
from django.utils.encoding import force_text, force_str




class ImageWidget(AdminFileWidget):
    def render(self, name, value, attrs=None):
        output = []
        if value and hasattr(value, "url"):
            output.append(('<a target="_blank" href="%s">'
                           '<img src="%s" height="200" title="%s"/></a> '
                           % (value.url, value.url, value.name)))
        output.append(super(AdminFileWidget, self).render(name, value, attrs))
        return mark_safe(u''.join(output))




def path_and_rename(instance, filename):
    now = datetime_util.now()
    upload_to = 'photos/{}/{}/{}'.format(now.year,now.month,now.day)
    ext = filename.split('.')[-1]
    # get filename
    if instance.pk:
        filename = '{}-{}.{}'.format(uuid4().hex,instance.pk, ext)
    else:
        # set filename as random string
        filename = '{}.{}'.format(uuid4().hex, ext)
    # return the whole path to the file
    return os.path.join(upload_to, filename)

@deconstructible
class UploadToPath(object):
    def __init__(self, upload_to):
        self.upload_to = upload_to

    def __call__(self, instance, filename):
        return self.generate_filename(filename)

    def get_directory_name(self):
        return os.path.normpath(force_text(datetime.datetime.now().strftime(force_str(self.upload_to))))

    def get_filename(self, filename):
        filename = default_storage.get_valid_name(os.path.basename(filename))
        filename = force_text(filename)
        filename = unicodedata.normalize('NFKD', filename).encode('ascii', 'ignore').decode('ascii')
        return os.path.normpath(filename)

    def generate_filename(self, filename):
        return os.path.join(self.get_directory_name(), self.get_filename(filename))

def path_and_rename_general(instance, filename,folder_name):
    now = datetime_util.now()
    upload_to = '{}/{}/{}/{}'.format(folder_name,now.year,now.month,now.day)
    ext = filename.split('.')[-1]
    # get filename
    if instance.pk:
        filename = '{}-{}.{}'.format(uuid4().hex,instance.pk, ext)
    else:
        # set filename as random string
        filename = '{}.{}'.format(uuid4().hex, ext)
    # return the whole path to the file
    return os.path.join(upload_to, filename)

#
# def path_and_rename_spec(path):
#     def wrapper(instance, filename):
#         try :
#             ext = filename.split('.')[-1]
#             ext = str(ext).lower()
#         except:
#             ext = filename.split('.')[-1]
#         # get filename
#         if instance.pk:
#             filename = '{}-{}.{}'.format(uuid4().hex, instance.pk, ext)
#         else:
#             # set filename as random string
#             filename = '{}.{}'.format(uuid4().hex, ext)
#         # return the whole path to the file
#         return os.path.join(path, filename)
#     return wrapper
#


from PIL import Image, ImageDraw, ImageFilter
def avatar_make(user_id):
    im1 = Image.open('avatar.png')
    im2 = Image.open('glasses.png')
    back_im = im1.copy()
    back_im.paste(im2.s, (100, 50))
    back_im.save('rocket_pillow_paste_pos.jpg', quality=95)

@shared_task
def send_html_mail(email,subject,html):
    from django.core.mail import send_mail
    from django.conf import settings
    send_mail(
        subject=subject,
        html_message=html,
        message='',
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=[email],
        fail_silently=False
    )
    return True
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin, UserManager
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.conf import settings
from django.core import validators
from django.utils.translation import ugettext_lazy as _
from base_user.tools.common import GENDER, USERTYPES, slugify, USERTYPESDICT, USERGAMESTATUS
from bot_app.general.utils import LanguageList, TOTALUSERGAMEPOINT
from general.functions import path_and_rename, send_html_mail
from datetime import datetime as d
import calendar
from django.db.models import Q, signals
from bot_app.tasks import *
# from work.common import *
# My custom tools import
from datetime import datetime, timedelta
# Create your models here.
from panel.models import MainSettings

USER_MODEL = settings.AUTH_USER_MODEL
import random
import uuid




# Customize User model
class MyUser(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    Username, password and email are required. Other fields are optional.
    """

    username = models.CharField(verbose_name=_('Username'), max_length=100, unique=True,
                                help_text=_('Required. max 75 symbol. The letters, numbers and '
                                            '@/./+/-/_ symbols.'),
                                validators=[
                                    validators.RegexValidator(r'^[\w.@+-]+$', _('Düzgün User adı daxil edin.'),
                                                              'yanlışdır')
                                ]
                                )
    m_id = models.CharField(verbose_name=_('Telegram id'), max_length=255, blank=True)
    main_message_id = models.CharField(verbose_name=_('Main message'), max_length=255, null=True, blank=True)
    hash = models.CharField(verbose_name=_('Telegram hash'), max_length=255, blank=True)
    auth_date = models.CharField(verbose_name=_('Telegram auth_date'), max_length=255, blank=True)
    photo_url = models.CharField(verbose_name=_('Telegram photo'), max_length=500, blank=True)
    active_avatar_photo = models.ForeignKey('panel.Avatar',verbose_name=_('Active avatar  photo'), blank=True,null=True)
    avatar_result_photo = models.ImageField(verbose_name=_('Avatar result photo'), max_length=500, blank=True,null=True)
    avatar_result_photo_url = models.CharField(verbose_name=_('Avatar result photo url'), max_length=500, blank=True,null=True)
    # username = models.CharField(_('first name'), max_length=255, blank=True)
    first_name = models.CharField(verbose_name=_('First name'), max_length=255, blank=True,null=True)
    last_name = models.CharField(verbose_name=_('Last name'), max_length=255, blank=True,null=True)
    gender = models.CharField(verbose_name=_('Gender'), max_length=255, blank=True,null=True)
    language = models.CharField(verbose_name=_('Language'), max_length=255, default=LanguageList[0])
    email = models.EmailField(verbose_name=_('Email address'), max_length=255,null=True)
    # section = models.ForeignKey('panel.Section',help_text=_("Section"), verbose_name=_("Section"), max_length=255,blank=True,null=True,on_delete=models.CASCADE)
    profile_picture = models.ImageField(upload_to=path_and_rename, null=True, blank=True)
    verified = models.BooleanField(default=False, verbose_name="Verified")
    phone = models.CharField(max_length=100, verbose_name=_("Phone"), null=True, blank=True)
    slug = models.SlugField(null=True, blank=True)

    strength = models.IntegerField(verbose_name="Strength", default=0)
    quickness = models.IntegerField(verbose_name="Quickness", default=0)
    intuition = models.IntegerField(verbose_name="Iintuition", default=0)
    durability = models.IntegerField(verbose_name="Durability", default=0)


    usertype = models.IntegerField(choices=USERTYPES, verbose_name="User Type", default=3)
    game_status = models.IntegerField(choices=USERGAMESTATUS, verbose_name="Game Type", default=0)
    is_staff = models.BooleanField(_('staff status'), default=False, help_text=_('Designates whether the user can log into this admin site.'))
    is_active = models.BooleanField(_('active'), default=True,help_text=_('Designates whether this user should be treated as ''active. Unselect this instead of deleting accounts.'))
    is_deleted = models.BooleanField(_('deleted'), default=False,help_text=_('Designates whether this user should be treated as ''active. Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    is_mini_admin = models.BooleanField(_('Mini Admin'), default=False,help_text=_('Mini Admin'))

    __original_active_avatar_photo = None
    """
        Important non-field stuff
    """
    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'
    def get_total_point(self):
        mainSettings = MainSettings.objects.filter().first()
        my_points = self.strength + self.quickness + self.intuition + self.durability
        total_points = mainSettings.limit_point - my_points if mainSettings and mainSettings.limit_point > 0 else TOTALUSERGAMEPOINT
        return total_points

    def __init__(self, *args, **kwargs):
        super(MyUser, self).__init__(*args, **kwargs)
        self.__original_active_avatar_photo = self.active_avatar_photo

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s' % (self.first_name)
        return full_name.strip() if full_name.strip() else self.username

    def get_usertype(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        return_val = ''
        try:
            return_val = USERTYPESDICT['{}'.format(self.usertype)]
        except:
            pass
        return return_val

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name
    def __str__(self):
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip() if full_name.strip() else self.username
    def save(self, force_insert=False, force_update=False, *args, **kwargs):
        super(MyUser, self).save(*args, **kwargs)
        self.slug = slugify(self.username+str(timezone.now().timestamp()).replace('.','-'))
        # if self.active_avatar_photo != self.__original_active_avatar_photo:
        #     make_avatar(self.pk)
            # make_avatar.delay(self.pk)
        super(MyUser, self).save(force_insert, force_update, *args, **kwargs)
        self.__original_active_avatar_photo = self.active_avatar_photo
        # super(MyUser, self).save(*args, **kwargs)
    def permissions(self):
        return_list = []
        for item in UserPermission.objects.filter(user=self):
            return_list.append(item.permission)
        return return_list

def user_delete_signal(sender, instance, *args, **kwargs):
    from panel.models import AllUserStatus
    AllUserStatus.objects.filter(chat_id=instance.m_id).delete()
def user_save(sender, instance, created, *args, **kwargs):
    pass
    # make_avatar.delay(instance.id)
    # if created:
    #     make_avatar(instance.pk)
        # make_avatar.delay(instance.pk)
    #     # print(signal)
    #     # Send verification email
    #     confirm_obj = UserConfrimationKeys(key=uuid.uuid4(),user=instance,expired_date=datetime.now()+timedelta(days=7),expired=False)
    #     confirm_obj.save()
    #     try:
    #         send_html_mail(instance.email,_("Confirm email"),'<a href="{}">{}</a>'.format(reverse('base-user:confirm-account',kwargs={'username':instance.username,'key':confirm_obj.key}),_('Confirm your account,click here')))
    #     except:
    #         pass

signals.post_save.connect(user_save, sender=MyUser)
signals.pre_delete.connect(user_delete_signal, sender=MyUser)


class UserConfrimationKeys(models.Model):
    key = models.CharField(max_length=255,null=True, blank=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True,blank=True,on_delete=models.CASCADE)
    expired_date = models.DateTimeField()
    expired = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-date',)
        verbose_name = "Təstiqlənmiş user"
        verbose_name_plural = "Təstiqlənmiş userlər"

    def __str__(self):
        return "%s" % self.key


rule_models_choices  = (

)


class UserPermission(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    permission = models.CharField(choices=rule_models_choices,max_length=255)
    class Meta:
        unique_together = ("user", "permission")


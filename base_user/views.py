from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.contrib import auth
from django.contrib.auth import get_user_model, logout
from django.urls import reverse

from django_telegram_login.widgets.constants import (
    SMALL,
    MEDIUM,
    LARGE,
    DISABLE_USER_PHOTO,
)
from django_telegram_login.widgets.generator import (
    create_callback_login_widget,
    # create_redirect_login_widget,
)
from django_telegram_login.authentication import verify_telegram_authentication
from django_telegram_login.errors import (
    NotTelegramDataError,
    TelegramDataIsOutdatedError,
)

bot_name = settings.TELEGRAM_BOT_NAME
bot_token = settings.TELEGRAM_BOT_TOKEN
redirect_url = settings.TELEGRAM_LOGIN_REDIRECT_URL

from django_telegram_login.authentication import verify_telegram_authentication
from django_telegram_login.errors import (
    NotTelegramDataError,
    TelegramDataIsOutdatedError,
)
GUser = get_user_model()


def log_out(request):
    if request.user.is_authenticated():
        logout(request)
    next_url = reverse('panel:index')
    return HttpResponseRedirect(next_url)





def create_redirect_login_widget(
        redirect_url, bot_name, size=SMALL, user_photo=True, access_write=True
):
    """
    Create a redirect widget, that allows to handle an user data as get request params.
    """
    script_initital = \
        '<script async src="https://telegram.org/js/telegram-widget.js?2" '
    bot = 'data-telegram-login="{}" '.format(bot_name)
    size = 'data-size="{}" '.format(size)
    userpic = \
        'data-userpic="{}" '.format(str(user_photo).lower()) if not user_photo else ''
    redirect = 'data-auth-url="{}" '.format(redirect_url)
    access = 'data-request-access="write"' if access_write else ''
    script_end = '></script>'

    widget_script = \
        script_initital + bot + size + userpic + redirect + access + script_end
    return widget_script


def index(request):

    # Initially, the index page may have no get params in URL
    # For example, if it is a home page, a user should be redirected from the widget
    if not request.GET.get('hash'):
        return HttpResponse('Handle the missing Telegram data in the response.')

    try:
        result = verify_telegram_authentication(bot_token=bot_token, request_data=request.GET)

    except TelegramDataIsOutdatedError:
        return HttpResponse('Authentication was received more than a day ago.')

    except NotTelegramDataError:
        return HttpResponse('The data is not related to Telegram!')
    print(result)
    # Or handle it as you wish. For instance, save to the database.
    return HttpResponse('Hello, ' + result['first_name'] + '!')


def callback(request):
    telegram_login_widget = create_callback_login_widget(bot_name, size=SMALL)

    context = {'telegram_login_widget': telegram_login_widget}
    return render(request, 'telegram_auth/callback.html', context)

from django.contrib.auth import load_backend, login, get_user_model


def redirect(request):

    # Initially, the index page may have no get params in URL
    # For example, if it is a home page, a user should be redirected from the widget
    if not request.GET.get('hash'):
        return HttpResponse('Handle the missing Telegram data in the response.')

    try:
        result = verify_telegram_authentication(bot_token=bot_token, request_data=request.GET)

    except TelegramDataIsOutdatedError:
        return HttpResponse('Authentication was received more than a day ago.')

    except NotTelegramDataError:
        return HttpResponse('The data is not related to Telegram!')
    print(result)
    # Or handle it as you wish. For instance, save to the database.
    # return HttpResponse('Hello, ' + result['first_name'] + '!')
    try:
        user = GUser.objects.filter(username=request.GET.get('username')).get()
        auth.login(request, user)
        user.m_id = request.GET.get('id')
        user.auth_date = request.GET.get('auth_date')
        user.hash = request.GET.get('hash')
        if request.GET.get('first_name'):
            user.first_name = request.GET.get('first_name')
        else:
            user.first_name = ''
        if request.GET.get('photo_url'):
            user.photo_url = request.GET.get('photo_url')
        else:
            user.photo_url = ''
        if request.GET.get('last_name'):
            user.last_name = request.GET.get('last_name')
        else:
            user.last_name = ''
        user.save()
        return HttpResponseRedirect(reverse('panel:index'))
    except:
        logout(request)
        return HttpResponseRedirect(redirect_to=reverse('base-user:callback'))
